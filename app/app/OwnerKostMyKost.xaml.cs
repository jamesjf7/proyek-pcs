﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostMyKost.xaml
    /// </summary>
    public partial class OwnerKostMyKost : Page
    {
        public OwnerKostMyKost()
        {
            InitializeComponent();
        }

        List<ActKost> listKost = new List<ActKost>();
        private void Page_Loaded(object sender, RoutedEventArgs e){
            string sql = @"
                select * from kost 
                where ownerkost_id=:id
                order by kost_id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", (App.User as OwnerKost).OwnerId);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            listKost = new List<ActKost>();
            while (reader.Read()){
                Kost newKost = new Kost(
                    App.User as OwnerKost,
                    reader.GetString(2),
                    reader.GetString(3),
                    BitmapConverter.GetBitmapFromBase64(reader.GetString(4)),
                    reader.GetString(5),
                    Convert.ToInt32(reader.GetValue(7).ToString()),
                    Convert.ToInt32(reader.GetValue(8).ToString())
                );
                ActKost actKost = new ActKost(newKost,
                    reader.GetString(0),
                    reader.GetDateTime(6),
                    Convert.ToInt32(reader.GetValue(9).ToString())
                );
                actKost.Rating = loadStarRate(actKost);
                listKost.Add(actKost);
            }
            App.connection.Close();

            // Generate kost
            displayKost();
        }

        float loadStarRate(ActKost kost) {
            // Ga perlu Conn.Open soalnya masih open(?)
            string sql = @"
                select to_char(nvl(avg(review_rating),-1),'999.9')
                from review rv
                inner join rent r1 on rv.rent_id = r1.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                where r2.kost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            float StarRate = float.Parse(cmd.ExecuteScalar().ToString());
            return StarRate;
        }

        void displayKost(){
            // Generate Kost...
            foreach (var kost in listKost){
                generateKost(kost);
            }
        }

        void generateKost(ActKost kost)
        {
            // Main Root
            TransitioningContent root = new TransitioningContent
            {
                OpeningEffect = new TransitionEffect(
                    TransitionEffectKind.ExpandIn
                )
            };

            // Root 1
            Grid root1 = new Grid
            {
                Width = 500, Height = 300,
                Margin = new Thickness(10),
                Background = BrushGen.FromDrawingColor(
                    System.Drawing.Color.RoyalBlue    
                )
            };
            root1.RowDefinitions.Add(new RowDefinition());
            root1.RowDefinitions.Add(new RowDefinition { Height=GridLength.Auto});
            root1.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(300)});
            root1.ColumnDefinitions.Add(new ColumnDefinition());
            root.Content = root1;

            // Child 1
            Border child1 = new Border
            {
                Background = BrushGen.FromString("#000")
            };
            child1.SetValue(Grid.RowSpanProperty,2);
            root1.Children.Add(child1);

            // Child 1.1
            Image image = new Image{
                Source = kost.Image,
                Stretch = Stretch.UniformToFill,
                HorizontalAlignment = HorizontalAlignment.Center
            };
            child1.Child = image;

            // Child 2
            StackPanel child2 = new StackPanel
            {
                Margin = new Thickness(10)
            };
            child2.SetValue(Grid.ColumnProperty, 1);
            root1.Children.Add(child2);

            // Child 2.n
            var listText = new[]{
                "Name :", kost.Name,
                "Created At :", kost.Created.ToString("dd MMMM yyyy"),
                "Rating :", "5.0",
                "Status :", new[]{"InActive","Active"}[kost.Status],
            };
            for (int i = 0; i < listText.Length; i++){
                TextBlock tb = new TextBlock{
                    Text = listText[i],
                    FontSize = 16
                };
                if (i % 2 == 0){
                    tb.Foreground = BrushGen.FromString("#eee");
                }else{
                    tb.Background = BrushGen.FromString("#99F");
                    tb.Padding = new Thickness(5);
                }
                if(listText[i] == "5.0"){
                    // Star Bar
                    float StarRate = kost.Rating;
                    bool rated = StarRate >= 0;
                    // textRating.Text = $"({(rated ? StarRate.ToString("0.0") : "New")})";
                    RatingBar rb = new RatingBar {
                        Value = rated ? (int)Math.Round(StarRate) : 5,
                        Foreground = BrushGen.FromString(rated ? "#DAA520" : "#7CFC00"),
                        IsReadOnly = true
                    };
                    // Add to stackpanel
                    child2.Children.Add(rb);
                }else{
                    child2.Children.Add(tb);
                }
            }

            // Child 3
            Button child3 = new Button{
                Content = "Detail",
                Margin = new Thickness(10)
            };
            child3.SetValue(Grid.RowProperty,1);
            child3.SetValue(Grid.ColumnProperty,1);
            // Button OnClick
            child3.Click += (object sender, RoutedEventArgs e) =>{
                App.Actmenu.navigate(new OwnerKostDetailKost(kost));
            };
            root1.Children.Add(child3);

            // Push to Wrap Panel
            wrapPane.Children.Add(root);
        }
    }
}
