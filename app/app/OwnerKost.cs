﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class OwnerKost : Account
    {
        private string ownerId;
        private string telp;
        private string email;

        public string OwnerId { get => ownerId; set => ownerId = value; }
        public string Telp { get => telp; set => telp = value; }
        public string Email { get => email; set => email = value; }

        public OwnerKost(string id, string name, int saldo, BitmapImage bimp, string ownerId, string telp, string email) : base(id, name, saldo, bimp)
        {
            OwnerId = ownerId;
            Telp = telp;
            Email = email;
        }
    }
}
