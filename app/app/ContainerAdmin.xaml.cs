﻿using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for ContainerAdmin.xaml
    /// </summary>
    public partial class ContainerAdmin : Page, INavigate
    {
       
        public ContainerAdmin()
        {
            InitializeComponent();
        }

        public void navigate(Page page, object param)
        {
            mainFrame.Content = null;
            mainFrame.NavigationService.Navigate(page, param);
        }

        public void navigate(Page page)
        {
            navigate(page, null);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            navigate(new AdminHome());
            tbname.Text = App.User.Name;
            profile.ImageSource = App.User.Image;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lbadmin.SelectedIndex;
            if (index == 0)
            {
                navigate(new AdminHome());
            }
             else if (index == 1)// Lihat all mhs
            {
                navigate(new AdminViewMhs());
            }
            else if (index == 2)//lihat all owner kost
            {
                navigate(new AdminViewOwnerkost());
            }
            else if (index == 3)//lihat all kost
            {
                navigate(new AdminViewKost());
            }
            else if (index == 4)//lihat list admin
            {
                navigate(new AdminViewAdmin());
            }
            else if (index == 5)//report mhs
            {
                navigate(new AdminConfirmationReportMhs());
            }
            else if (index == 6)//lihat jurusan
            {
                navigate(new AdminViewListJurusan());
            }
            else if (index == 7)//konfirmasi owner kost
            {
                navigate(new AdminConfirmOwnerKost());
            }
            else if (index == 8)//Generate report
            {
                navigate(new AdminGenerateReport());
            }else if(index == 9) { // Change Password
                navigate(new ChangePassword());
            }
            else if (index == 10)//logout
            {
                var result = new SweetAlert()
                {
                    Title = "Confirmation",
                    Message = "Are you sure you want to leave?",
                    MsgImage = SweetAlertImage.QUESTION,
                    MsgButton = SweetAlertButton.YesNo,
                    OkText = "Yes",
                    CancelText = "No"
                }.ShowDialog();
                if (result == SweetAlertResult.YES)
                    App.MainWindow.navigateContainer(Container.StartMenu);
                return;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to Exit?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.YES)
                Application.Current.Shutdown();
        }
    }
}
