﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostUpdateFacility.xaml
    /// </summary>
    public partial class OwnerKostUpdateFacility : Page
    {
        ActKost kost;
        List<ActFasilitas> listFasilitas = new List<ActFasilitas>();
        public OwnerKostUpdateFacility(ActKost kost)
        {
            InitializeComponent();
            this.kost = kost;

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string idkost = kost.Id.ToString();
            OracleDataAdapter da;
            OracleCommandBuilder builder;
            string sql = "select * from fasilitas where KOST_ID ='" + idkost + "'";
            da = new OracleDataAdapter(sql, App.connection);
            builder = new OracleCommandBuilder(da);
            var dt = new DataTable();
            da.Fill(dt);

           
            foreach (DataRow item in dt.Rows)
            {
                Fasilitas fas = new Fasilitas(item["FASILITAS_NAMA"].ToString(), BitmapConverter.GetBitmapFromBase64(item["FASILITAS_IMAGE"].ToString()));
                listFasilitas.Add(new ActFasilitas(fas, item["FASILITAS_id"].ToString()));
            }
            
            foreach (var fasilitas in listFasilitas)
                addStackPanelChild(fasilitas,fasilitas.Id);
        }

        void addStackPanelChild(Fasilitas fasilitas = null, string id=null)
        {
            // Main Root
            TransitioningContent root = new TransitioningContent
            {
                OpeningEffect = new TransitionEffect(
                    TransitionEffectKind.FadeIn
                )
            };

            // Root1
            Border root1 = new Border
            {
                Margin = new Thickness(0, 0, 0, 20),
                BorderThickness = new Thickness(2),
                BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0)),
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#CCE5FF"))
            };
            root.Content = root1;

            // Root2
            Grid root2 = new Grid
            {
                Margin = new Thickness(20)
            };
            root1.Child = root2;
            // Column
            root2.ColumnDefinitions.Add(new ColumnDefinition
            {
                Width = new GridLength(100)
            });
            root2.ColumnDefinitions.Add(new ColumnDefinition());

            // Child1
            Button child1 = new Button
            {
                Width = 100,
                Height = 100,
                Background = new SolidColorBrush(Color.FromRgb(
                    (System.Drawing.Color.Navy).R,
                    (System.Drawing.Color.Navy).G,
                    (System.Drawing.Color.Navy).B
                )),
                Padding = new Thickness(0),
            };
            child1.Click += btnImageClick;
            root2.Children.Add(child1);
            child1.SetValue(Grid.ColumnProperty, 0);

            // Child2
            StackPanel child2 = new StackPanel
            {
                Margin = new Thickness(10, 0, 0, 0),
                VerticalAlignment = VerticalAlignment.Center,
            };
            root2.Children.Add(child2);
            child2.SetValue(Grid.ColumnProperty, 1);

            // GrandChild
            // TextBlock
            var control = fasilitas != null && fasilitas.Image != null ?
            (UIElement)new Image
            {
                Source = fasilitas.Image,
                Stretch = Stretch.UniformToFill
            } :
            (UIElement)new TextBlock
            {
                Background = new SolidColorBrush(Color.FromRgb(
                    (System.Drawing.Color.Navy).R,
                    (System.Drawing.Color.Navy).G,
                    (System.Drawing.Color.Navy).B
                )),
                Text = "Add Image"
            };

            // Textbox
            TextBox tb2 = new TextBox
            {
                Style = tempTb.Style,
                VerticalAlignment = VerticalAlignment.Top,
                TextWrapping = TextWrapping.Wrap,
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FAFAFA")),
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                FontSize = 16,
                MaxHeight = 90,
                MaxLength = 20,
                Margin = new Thickness(0, 0, 0, 10)
            };
            tb2.Text = fasilitas == null ? "" : fasilitas.Name;
            tb2.SetValue(HintAssist.HintProperty, "Facility name");


            // button Remove
            Button btn = new Button
            {
                //Name = $"btnRemove{stackFacility.Children.Count-1}",
                Background = new SolidColorBrush(Color.FromRgb(
                    (System.Drawing.Color.Red).R,
                    (System.Drawing.Color.Red).G,
                    (System.Drawing.Color.Red).B
                )),
                Content = "Remove",
                Foreground = new SolidColorBrush(Color.FromRgb(
                    (System.Drawing.Color.White).R,
                    (System.Drawing.Color.White).G,
                    (System.Drawing.Color.White).B
                )),
                BorderThickness = new Thickness(0),
                FontWeight = FontWeights.ExtraBold,
                Height = 45
            };
            btn.Click += btnRemoveClick;

            // Assign GrandChild to Child
            child1.Content = control;
            child2.Children.Add(tb2);
            child2.Children.Add(btn);

            // Add to StackPanel
            var children = stackFacility.Children;
            children.Add(root);
        }
        void btnImageClick(object sender, RoutedEventArgs e)
        {
            var btnImage = sender as Button;
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                btnImage.Content = null;
                Image newImg = new Image
                {
                    Stretch = Stretch.UniformToFill,
                    Source = new BitmapImage(new Uri(op.FileName)),
                    HorizontalAlignment = HorizontalAlignment.Center
                };
                btnImage.Content = newImg;
            }
        }

        void btnRemoveClick(object sender, RoutedEventArgs e)
        {
            Button control = sender as Button;
            DependencyObject parent;
            parent = VisualTreeHelper.GetParent(control);// stackpanel
            parent = VisualTreeHelper.GetParent(parent); // grid
            parent = VisualTreeHelper.GetParent(parent); // border
            parent = VisualTreeHelper.GetParent(parent); // ContentPresenter
            parent = VisualTreeHelper.GetParent(parent); // Border
            parent = VisualTreeHelper.GetParent(parent); // TransitioningContent
            stackFacility.Children.Remove((UIElement)parent);
        }
        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            addStackPanelChild();
        }

       
        List<ActFasilitas> getFasilitas()
        {
            var listFas = new List<ActFasilitas>();
            foreach (var comp in stackFacility.Children)
            {
                Grid grid = ((comp as TransitioningContent)
                    .Content as Border)
                    .Child as Grid;
                // Child 1
                Button child1 = grid.Children[0] as Button;
                UIElement grandChild = child1.Content as UIElement;
                BitmapImage bimp = grandChild is Image ?
                    (grandChild as Image).Source as BitmapImage : null;
                // Child 2
                StackPanel child2 = grid.Children[1] as StackPanel;
                grandChild = child2.Children[0] as UIElement;
                string name = (grandChild as TextBox).Text;
               
                // Push Fasilitas
                Fasilitas fas = new Fasilitas(name, bimp);
                listFas.Add(new ActFasilitas(fas, ""));
            }
            return listFas;
        }

        private void BtnSimpan_Click(object sender, RoutedEventArgs e)
        {
            
            listFasilitas = getFasilitas();
            string sql = "select * from fasilitas  where KOST_ID ='" + kost.Id + "'";
            var da = new OracleDataAdapter(sql, App.connection);
            var builder = new OracleCommandBuilder(da);
            var ds = new DataTable();
            da.Fill(ds);
            //ds.Rows.Clear();
            // Remove Old Rows
            foreach (DataRow row in ds.Rows)
                row.Delete();
            // End of Remove

            foreach (var item in listFasilitas)
            {
                var row = ds.NewRow();
                row[0] = "";
                row[1] = kost.Id;
                row[2] = item.Name;
                row[3] = BitmapConverter.GetBase64FromBitmap(item.Image);
                ds.Rows.Add(row);
            }
            da.Update(ds);
            // Navigate back to Home
            SweetAlert.Show("Success", "Facility successfully changed!", msgImage: SweetAlertImage.SUCCESS);
            App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e){
            App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }
    }
}
