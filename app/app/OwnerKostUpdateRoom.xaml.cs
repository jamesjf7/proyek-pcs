﻿using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostUpdateRoom.xaml
    /// </summary>
    public partial class OwnerKostUpdateRoom : Page
    {
        ActKost kost;
        List<ActRoomKost> actRooms;
        int tempFloor;
        public OwnerKostUpdateRoom(ActKost kost,int idx=0)
        {
            InitializeComponent();
            this.kost = kost;
            tempFloor = idx;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            loadRoom();
            Floor = tempFloor;
        }

        #region Watch Property
        // Floor
        private int _floor;
        public int Floor {
            get => _floor;
            set {
                // Cek Lantai -1
                if (value < 0) return;
                // Cek Lantai Kosong
                bool change = false;
                int maxFloor = kost.Floors;
                int currFloor = Floor;
                if (value > maxFloor && kost.isFloorEmpty(currFloor)) {
                    SweetAlert.Show("Whoops", "This floor is lonely", msgImage: SweetAlertImage.WARNING);
                } else {
                    change = true;
                }
                // Jika Floor Berubah...
                if (change) {
                    _floor = value;
                    txtFloor.Text = value + "";
                    btnMinus.IsEnabled = value > 0;
                    displayRoom();
                }
            }
        }
        // RoomKost
        private ActRoomKost _actRoom;
        public ActRoomKost ActRoom {
            get => _actRoom;
            set {
                _actRoom = value;
                cardDetail.Visibility = Visibility.Collapsed;
                if(ActRoom != null){
                    imgRoom.ImageSource = ActRoom.Image;
                    textRoom.Text = $"{ActRoom.Name} (Floor {ActRoom.Lantai})";
                    textPrice.Text = CurrencyConverter.ToCurrency(ActRoom.Harga);
                    textNextPrice.Text = CurrencyConverter.ToCurrency(ActRoom.HargaNext);
                    textGender.Text = ActRoom.StrGender;
                    textGender.Foreground = BrushGen.FromString(new[] { "#00F","#F00","#FFA500" }[ActRoom.Gender]);
                    textStatus.Text = ActRoom.StrStatus;
                    textDesc.Text = ActRoom.Description;
                    cardDetail.Visibility = Visibility.Visible;
                }
            }
        }
        #endregion

        // Load Room From DB
        void loadRoom() {
            string sql = $@"
                select r1.room_id, room_nama, room_lantai,
	                room_image, room_harga, room_description,
	                room_gender, room_harga_next, room_status, 
                    nvl(a.account_nama, ' ')
                from room r1
                left outer join (
	                select * from rent
	                where rent_status = 1
                )r2 on r1.room_id = r2.room_id
                left outer join mahasiswa m on r2.mahasiswa_id = m.mahasiswa_id
                left outer join account a on m.account_id = a.account_id
                where kost_id = :id and room_status <> -2
                order by 1
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            actRooms = new List<ActRoomKost>();
            while (reader.Read()) {
                ActRoomKost newRoom = new ActRoomKost(
                    new RoomKost(
                        reader.GetString(1),
                        reader.GetInt32(2),
                        BitmapConverter.GetBitmapFromBase64(reader.GetString(3)),
                        (int)reader.GetInt64(4),
                        reader.GetString(5),
                        reader.GetInt32(6)
                    ),
                    reader.GetString(0),
                    Convert.ToInt32(reader.GetValue(7).ToString()),
                    reader.GetInt32(8),
                    reader.GetString(9)
                );
                actRooms.Add(newRoom);
            }
            kost.ListRoom = actRooms.ToList<RoomKost>();
            App.connection.Close();
        }
        // Generate Room
        void displayRoom() {
            // Clear Wrap Pane
            wrapPanel.Children.Clear();
            // Update ListRoom
            var listRoom = actRooms
            .Where(room => room.Lantai == Floor)
            .OrderBy(room => room.Name)
            .ToList();
            // Dynamic Component
            foreach (var room in listRoom) {
                // Main Root
                TransitioningContent root = new TransitioningContent {
                    OpeningEffect = new TransitionEffect(
                        TransitionEffectKind.ExpandIn
                    )
                };

                // Root1
                Grid root1 = new Grid {
                    Width = 200,
                    Height = 250,
                    Background = BrushGen.FromString("#fff"),
                    Margin = new Thickness(10)
                };
                root1.RowDefinitions.Add(new RowDefinition());
                root1.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                root.Content = root1;

                // Child1
                Border child1 = new Border {
                    Background = BrushGen.FromString("#333")
                };
                child1.SetValue(Grid.RowProperty, 0);
                root1.Children.Add(child1);

                // Child2
                Border child2 = new Border {
                    Background = BrushGen.FromString(new[] { "#3D619B", "#DE63B3", "#5F9EA0" }[room.Gender])
                };
                child2.SetValue(Grid.RowProperty, 1);
                root1.Children.Add(child2);

                // Child 1.1
                Image image = new Image {
                    Source = room.Image,
                    Stretch = Stretch.UniformToFill,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                };
                child1.Child = image;

                // Child 2.1
                StackPanel stackPane = new StackPanel {
                    VerticalAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(10)
                };
                child2.Child = stackPane;

                // Child 2.1.n
                stackPane.Children.Add(new TextBlock {
                    Text = room.Name, FontSize = 18,
                    FontWeight = FontWeights.Medium,
                    Foreground = BrushGen.FromString("#fff")
                });
                string[][] arrText = {
                    new[]{$"Price: "      ,$"{CurrencyConverter.ToCurrency(room.Harga)}"  },
                    new[]{$"Next Price: " ,$"{CurrencyConverter.ToCurrency(room.HargaNext)}"  },
                    new[]{$"Gender: "     ,$"{new[]{"Man","Woman","Both"}[room.Gender]}"  },
                    new[]{$"Status: "     ,room.DispStatus}
                };
                foreach (var arr in arrText) {
                    StackPanel newStack = new StackPanel {
                        Orientation = Orientation.Horizontal
                    };
                    newStack.Children.Add(new TextBlock {
                        Text = arr[0],
                        Foreground = BrushGen.FromString("#fff")
                    });
                    newStack.Children.Add(new TextBlock {
                        Text = arr[1], FontWeight=FontWeights.Thin,
                        Foreground = BrushGen.FromString("#fff")
                    });
                    stackPane.Children.Add(newStack);
                }

                // Button Detail
                Button btnDetail = new Button {
                    Content = "Detail",
                    Margin = new Thickness(0, 10, 0, 0)
                };
                stackPane.Children.Add(btnDetail);
                // Background
                LinearGradientBrush linear = new LinearGradientBrush {
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1),
                };
                linear.GradientStops.Add(new GradientStop {
                    Offset = 0.0f, Color = ColorGen.FromString("#0081E1")
                });
                linear.GradientStops.Add(new GradientStop {
                    Offset = 1.0f, Color = ColorGen.FromString("#00D7DD")
                });
                btnDetail.Background = linear;
                // OnClick
                btnDetail.Click += (object sender, RoutedEventArgs e) => {
                    ActRoom = room;
                };

                // Add to Parent
                wrapPanel.Children.Add(root);
            }
        }

        #region Component Event
        private void BtnBack_Click(object sender, RoutedEventArgs e) {
            App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }

        private void BtnPlus_Click(object sender, RoutedEventArgs e) => Floor++;
        private void BtnMinus_Click(object sender, RoutedEventArgs e) => Floor--;

        private void BtnAddRoom_Click(object sender, RoutedEventArgs e) {
            App.Actmenu.navigate(new OwnerKostCreateRoom(kost, Floor));
        }
        private void BtnEdit_Click(object sender, RoutedEventArgs e) {
            if (ActRoom == null) {
                SweetAlert.Show("Whoops","No Room Selected!",msgImage: SweetAlertImage.WARNING);
            } else {
                App.Actmenu.navigate(new OwnerKostModifyRoom(kost,ActRoom));
            }
        }

        private void BtnDel_Click(object sender, RoutedEventArgs e) {
            if (ActRoom == null) {
                SweetAlert.Show("Whoops","No Room Selected!",msgImage: SweetAlertImage.WARNING);
            } else {
                var result = new SweetAlert(){
                    Title = "Confirmation",
                    Message = $"Are you sure you want to delete Room {ActRoom.Name}?",
                    MsgImage = SweetAlertImage.QUESTION,
                    MsgButton = SweetAlertButton.YesNo,
                    OkText = "Yes",
                    CancelText = "No"
                }.ShowDialog();
                if (result == SweetAlertResult.YES) {
                    if(ActRoom.Occupant != " ") {
                        result = new SweetAlert() {
                            Title = "Confirmation",
                            Message = $"Are you really sure? [{ActRoom.Occupant} will be kicked]",
                            MsgImage = SweetAlertImage.QUESTION,
                            MsgButton = SweetAlertButton.YesNo,
                            OkText = "Yes",
                            CancelText = "No"
                        }.ShowDialog();
                        if(result == SweetAlertResult.NO) {
                            SweetAlert.Show("Cancelled",
                                $"{ActRoom.Occupant} is safe",
                            msgImage: SweetAlertImage.ERROR);
                            return;
                        }
                    }
                    // Soft Delete
                    OracleCommand cmd = new OracleCommand {
                        Connection = App.connection,
                        CommandText = "DeleteRoom",
                        CommandType = CommandType.StoredProcedure,
                    };
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "id",
                        OracleDbType = OracleDbType.Varchar2,
                        Value = ActRoom.Id
                    });
                    App.openConnection();
                    cmd.ExecuteNonQuery();
                    App.closeConnection();
                    // Success Delete [Kasih Pesan + Remove Card]
                    SweetAlert.Show("Success", "Room Successfully deleted!", msgImage: SweetAlertImage.SUCCESS);
                    actRooms.Remove(ActRoom);
                    ActRoom = null;
                    displayRoom();
                }
            }
        }
        #endregion

    }
}
