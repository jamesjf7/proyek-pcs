﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    class Mahasiswa : Account
    {
        private string mhsId, jurusanNama, username;
        private int gender, tahun;

        public string MhsId { get => mhsId; set => mhsId = value; }
        public string JurusanNama { get => jurusanNama; set => jurusanNama = value; }
        public string Username { get => username; set => username = value; }
        public int Gender{ get => gender; set => gender = value; }
        public int Tahun { get => tahun; set => tahun = value; }

        public Mahasiswa(string id, string name, int saldo, BitmapImage bimp, string mhsId, int gender, string jurusanNama, int tahun, string username) : base(id, name, saldo, bimp)
        {
            MhsId = mhsId;
            JurusanNama = jurusanNama;
            Gender = gender;
            Tahun = tahun;
            Username = username;
        }
    }
}
