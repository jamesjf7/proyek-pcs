﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace app
{
    public interface INavigate
    {
        void navigate(Page page, Object param);
        void navigate(Page page);
    }
}
