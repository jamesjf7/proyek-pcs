﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for MhsHistory.xaml
    /// </summary>
    public partial class MhsHistory : Page
    {
        public MhsHistory()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Query masih salah (rencana dijadiin function rencana(?))
            string query = $"select * from rent rn " +
                           $"inner join room r on r.room_id = rn.room_id " +
                           $"inner join kost ko on ko.kost_id = r.kost_id " +
                           $"where rn.mahasiswa_id = '{((Mahasiswa)App.User).MhsId}' and rn.rent_status = 2 or rn.rent_status = -2";
            listHistory.Content = (App.Actmenu as ContainerMhs).Generate_List(query,"History");
        }
    }
}
