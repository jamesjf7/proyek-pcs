﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using System.Media;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System.Data;
namespace app
{
    public partial class MhsDialog : Window
    {
        string type;
        object data;
        List<UIElement> listElem = new List<UIElement>();
        public MhsDialog(string type, string buttonType, object data = null)
        {
            InitializeComponent();
            // Data = data
            this.type = type;
            this.data = data;
            // JENIS BUTTON
            if (buttonType == "OkCancel") {
                grBtn.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                grBtn.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                Button btnOk = new Button() { Content = "Ok", Style = (Style)FindResource("MaterialDesignToolButton"), Foreground = new SolidColorBrush(Colors.Indigo), FontWeight = FontWeights.Bold, Height = 50 };
                Button btnCancel = new Button() { Content = "Cancel", Style = (Style)FindResource("MaterialDesignToolButton"), Foreground = new SolidColorBrush(Colors.Indigo), FontWeight = FontWeights.SemiBold, Height = 50 };
                btnOk.Click += BtnOK_Click;
                btnCancel.Click += BtnCancel_Click;
                Grid.SetColumn(btnOk, 0);
                Grid.SetColumn(btnCancel, 1);
                grBtn.Children.Add(btnOk);
                grBtn.Children.Add(btnCancel);
            }
            else if (buttonType == "Ok") {
                Button btnOk = new Button() { Content = "Ok", Style = (Style)FindResource("MaterialDesignToolButton"), Foreground = new SolidColorBrush(Colors.Indigo), FontWeight = FontWeights.Bold, Height = 50 };
                btnOk.Click += BtnOK_Click;
                grBtn.Children.Add(btnOk);
            }

            // JENIS MODE
            if (type == "TopUp")
            {
                // textbox 1    
                tbTitle.Text = "Top Up";
                DockPanel dp = new DockPanel() { Margin = new Thickness(10) };
                listElem.Add(new TextBlock() { Text = "Nominal : ", FontSize = 20, FontWeight = FontWeights.SemiBold });
                listElem.Add(new TextBox() { Name = "tb_nominal" });
                foreach (UIElement i in listElem) dp.Children.Add(i);
                grContent.Children.Add(dp);
            }
            else if (type == "PayUtang")
            {
                tbTitle.Text = "Pay Utang";
                OracleCommand cmd = new OracleCommand($"SELECT utang_nominal+utang_denda FROM UTANG WHERE RENT_ID = '{data}'", App.connection);
                App.openConnection();
                string utangTotal = $"{CurrencyConverter.ToCurrency(int.Parse(cmd.ExecuteScalar().ToString()))}";
                App.closeConnection();
                DockPanel dp = new DockPanel();
                listElem.Add(new TextBlock() { Text = $"Pay Utang", FontSize = 20, FontWeight = FontWeights.SemiBold });
                listElem.Add(new TextBlock() { Text = $"{utangTotal}", FontSize = 20, FontWeight = FontWeights.SemiBold });
                foreach (UIElement i in listElem) dp.Children.Add(i);
                grContent.Children.Add(dp);
            }
            else if (type == "DetailNews")
            {
                //News nw = ((News)data);
                //tbTitle.Text = "Detail News";
                //DockPanel dp = new DockPanel() { Margin = new Thickness(10) };
                //listElem.Add(new TextBlock() { Text = $"Title News : {nw}", FontSize = 20, FontWeight = FontWeights.SemiBold });
                //listElem.Add(new TextBlock() { Text = $"Description : {nw}", FontSize = 20, FontWeight = FontWeights.SemiBold });
                //foreach (UIElement i in listElem) dp.Children.Add(i);
                //grContent.Children.Add(dp);
            }
            else if (type == "ReviewRoom")
            {
                tbTitle.Text = "Review Kost";
                OracleCommand cmd = new OracleCommand(
                    $"select * from rent rn " +
                    $"inner join room r on r.room_id = rn.room_id " +
                    $"where rn.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' and rn.rent_id = '{data}'",
                    App.connection
                );
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    // textbox 5 , rating bar 4
                    StackPanel sp = new StackPanel() { Margin = new Thickness(5) };
                    listElem.Add(new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) }, HorizontalAlignment = HorizontalAlignment.Center, Height = 200, Width = 300 });
                    listElem.Add(new TextBlock() { Text = reader["room_nama"].ToString() });
                    listElem.Add(new TextBlock() { Text = $"{CurrencyConverter.ToCurrency(int.Parse(reader["room_harga"].ToString()))}" });
                    listElem.Add(new TextBlock() { Text = reader["rent_tanggal"].ToString() });
                    listElem.Add(new RatingBar() { Value = 3 });
                    listElem.Add(new TextBox() { });
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    grContent.Children.Add(sp);
                }
            }
            else if (type == "DetailRoom")
            {
                tbTitle.Text = "Detail Room";
                App.connection.Open();
                OracleCommand cmd = new OracleCommand($"SELECT * FROM ROOM WHERE ROOM_ID = '{data}'", App.connection);
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StackPanel sp = new StackPanel() { Margin = new Thickness(10) };
                    listElem.Add(new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) }, HorizontalAlignment = HorizontalAlignment.Center, Height = 200, Width = 300 });
                    listElem.Add(new TextBlock() { Text = reader["room_nama"].ToString() });
                    listElem.Add(new TextBlock() { Text = $"Lantai {reader["room_lantai"]}" });
                    listElem.Add(new TextBlock() { Text = $"{CurrencyConverter.ToCurrency(int.Parse(reader["room_harga"].ToString()))}" });
                    listElem.Add(new TextBlock() { Text = reader["room_description"].ToString() });
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    grContent.Children.Add(sp);
                }
                App.connection.Close();
            }
            else if (type == "ListReview") {
                App.openConnection();
                string query = $"select * from review rv " +
                               $"inner join rent rn on rn.rent_id = rv.rent_id " +
                               $"inner join room r on r.room_id = rn.room_id " +
                               $"inner join kost ko on ko.kost_id = r.kost_id " +
                               $"inner join mahasiswa m on m.mahasiswa_id = rn.mahasiswa_id " +
                               $"inner join account a on a.account_id = m.account_id " +
                               $"where ko.kost_id = '{data}'";
                OracleCommand cmd = new OracleCommand(query,App.connection);
                OracleDataReader reader = cmd.ExecuteReader();
                ScrollViewer sv = new ScrollViewer() { Height = 400 };
                StackPanel spContainer = new StackPanel() { Margin = new Thickness(5)};
                if (!reader.HasRows) {
                    grContent.Children.Add(new TextBlock() { Text = "No Review"});
                }
                while (reader.Read()) {
                    List<UIElement> listElem = new List<UIElement>();
                    Card c = new Card() { Margin = new Thickness(5) };
                    StackPanel sp = new StackPanel() { Margin = new Thickness(5) };
                    DockPanel dp = new DockPanel() { Margin = new Thickness(5) };
                    dp.Children.Add(new TextBlock() { Text = $"{reader["account_nama"].ToString()}", FontSize = 18, FontWeight = FontWeights.SemiBold});
                    dp.Children.Add(new RatingBar() { Value = int.Parse(reader["review_rating"].ToString()), IsReadOnly = true });
                    dp.Children.Add(new TextBlock() { Text = $"({reader["review_rating"].ToString()}.0)", FontSize = 18 });
                    listElem.Add(dp);
                    listElem.Add(new TextBlock() { Text = $"{reader["review_description"].ToString()}", FontSize = 16, FontWeight = FontWeights.Thin , Margin = new Thickness(5,0,5,0)});
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    c.Content = sp;
                    spContainer.Children.Add(c);
                }
                sv.Content = spContainer;
                grContent.Children.Add(sv);
                App.closeConnection();
            }
            else if (type == "LeaveRoom")
            {
                tbTitle.Text = "Leave Room";
                App.connection.Open();
                OracleCommand cmd = new OracleCommand($"SELECT * FROM RENT RN INNER JOIN ROOM R ON R.ROOM_ID = RN.ROOM_ID WHERE RN.RENT_ID = '{data}'", App.connection);
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StackPanel sp = new StackPanel() { Margin = new Thickness(10) };
                    listElem.Add(new TextBlock() { Text = $"Do you really want to leave {reader["room_nama"].ToString()} ?" , FontSize = 18});
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    grContent.Children.Add(sp);
                }
                App.connection.Close();
            }
            else if (type == "Report")
            {
                // textbox
                App.connection.Open();
                tbTitle.Text = "Report";
                OracleCommand cmd = new OracleCommand(
                    $"select * from kost where kost_id = '{data}'",
                    App.connection
                );
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    // textbox 4
                    StackPanel sp = new StackPanel() { Margin = new Thickness(5) };
                    listElem.Add(new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["kost_image"].ToString()) }, HorizontalAlignment = HorizontalAlignment.Center, Height = 200, Width = 300 });
                    listElem.Add(new TextBlock() { Text = reader["kost_id"].ToString(), FontSize = 16, FontWeight = FontWeights.Bold });
                    listElem.Add(new TextBlock() { Text = reader["kost_name"].ToString(), FontSize = 16, FontWeight = FontWeights.SemiBold });
                    listElem.Add(new TextBlock() { Text = reader["kost_alamat"].ToString() });
                    listElem.Add(new TextBox() { });
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    grContent.Children.Add(sp);
                }
                App.connection.Close();
            }
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (type == "TopUp")
            {
                try
                {
                    string id = App.User.Id;
                    int nominal = int.Parse(((TextBox)listElem[1]).Text);
                    string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    if (nominal > 0)
                    {
                        App.connection.Open();
                        OracleCommand cmd = new OracleCommand("INSERT INTO TOPUP VALUES('',:id,to_date(:tanggal,'dd/mm/yyyy hh24:mi:ss'),:nominal)", App.connection);
                        cmd.Parameters.Add("id", id);
                        cmd.Parameters.Add("tanggal", date);
                        cmd.Parameters.Add("nominal", nominal);
                        cmd.ExecuteNonQuery();
                        cmd = new OracleCommand($"SELECT ACCOUNT_SALDO FROM ACCOUNT WHERE ACCOUNT_ID = '{id}'", App.connection);
                        App.User.Saldo = int.Parse(cmd.ExecuteScalar().ToString());
                        SweetAlert.Show("Top Up", $"Succesfully Top Up {CurrencyConverter.ToCurrency(int.Parse(((TextBox)listElem[1]).Text))}", msgButton: SweetAlertButton.OK);
                        App.connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    App.connection.Close();
                    SweetAlert.Show("Notice", $"Saldo tidak cukup", msgButton: SweetAlertButton.OK, msgImage: SweetAlertImage.ERROR);
                }
            }
            else if (type == "PayUtang") {
                try{
                    App.openConnection();
                    OracleCommand cmd = new OracleCommand($"UPDATE UTANG SET UTANG_STATUS = 1 WHERE RENT_ID = '{data}'",App.connection);
                    cmd.ExecuteNonQuery();
                    cmd = new OracleCommand($"SELECT ACCOUNT_SALDO FROM ACCOUNT WHERE ACCOUNT_ID = '{App.User.Id}'", App.connection);
                    App.User.Saldo = int.Parse(cmd.ExecuteScalar().ToString());
                    SweetAlert.Show("Pay Utang", $"Succesfully Pay Utang {((TextBlock)listElem[1]).Text}",     msgButton: SweetAlertButton.OK, msgImage: SweetAlertImage.SUCCESS);
                    App.closeConnection();
                    App.Actmenu.navigate(new MhsTransactionRecord());
                }
                catch (Exception ex){
                    App.closeConnection();
                    SweetAlert.Show("Invalid",$"Saldo tidak cukup" ,msgButton: SweetAlertButton.OK,msgImage: SweetAlertImage.ERROR);
                }
            }
            else if (type == "ReviewRoom"){ 
                if ((listElem[5] as TextBox).Text == "")
                {
                    SweetAlert.Show("Review", "All field must be filled", msgButton: SweetAlertButton.OK);
                }
                else
                {
                    OracleCommand cmd = new OracleCommand("INSERT INTO REVIEW VALUES('',:rentID,:reviewRating,:reviewDescription)", App.connection);
                    cmd.Parameters.Add("rentID", data);
                    cmd.Parameters.Add("reviewRating", (listElem[4] as RatingBar).Value);
                    cmd.Parameters.Add("reviewDescription", (listElem[5] as TextBox).Text);
                    cmd.ExecuteNonQuery();
                    SweetAlert.Show("Review", "Review sent", msgButton: SweetAlertButton.OK);
                }
            }
            else if (type == "DetailRoom")
            {
                try
                {
                    string mhsID = ((Mahasiswa)App.User).MhsId;
                    string roomID = data + "";
                    App.connection.Open();
                    OracleCommand cmd = new OracleCommand("INSERT INTO RENT VALUES('',:mhsID,:roomID,'',0,0,0,'',SYSDATE)", App.connection);
                    cmd.Parameters.Add("mhsID", mhsID);
                    cmd.Parameters.Add("roomID", roomID);
                    cmd.ExecuteNonQuery();
                    cmd = new OracleCommand($"SELECT ACCOUNT_SALDO FROM ACCOUNT WHERE ACCOUNT_ID = '{App.User.Id}'", App.connection);
                    App.User.Saldo = int.Parse(cmd.ExecuteScalar().ToString());
                    SweetAlert.Show("Rent", $"Rent receipt send", msgButton: SweetAlertButton.OK);
                    App.connection.Close();
                }
                catch (Exception ex)
                {
                    App.connection.Close();
                    SweetAlert.Show("Notice", $"Saldo tidak cukup", msgButton: SweetAlertButton.OK, msgImage: SweetAlertImage.ERROR);
                }
            }
            else if (type == "LeaveRoom")
            {
                // update rent status jadi 2
                try
                {
                    string mhsID = ((Mahasiswa)App.User).MhsId;
                    string rentID = data + "";
                    App.connection.Open();
                    OracleCommand cmd = new OracleCommand($"UPDATE RENT SET RENT_STATUS = 2 WHERE RENT_ID = '{rentID}'", App.connection);
                    cmd.ExecuteNonQuery();
                    SweetAlert.Show("Leave", $"Successfully Left", msgButton: SweetAlertButton.OK);
                    App.connection.Close();
                }
                catch (Exception ex)
                {
                    App.connection.Close();
                    SweetAlert.Show("Notice", $"Invalid {ex.Message}", msgButton: SweetAlertButton.OK, msgImage: SweetAlertImage.ERROR);
                }
            }
            else if (type == "Report")
            {
                string mhsID = ((Mahasiswa)App.User).MhsId;
                string kostID = data + "";
                string reportDescription = ((TextBox)listElem[4]).Text;
                OracleCommand cmd = new OracleCommand("INSERT INTO REPORT VALUES('',:mhsID,:kostID,:reportDescription,0,SYSDATE,SYSDATE)", App.connection);
                cmd.Parameters.Add("mhsID", mhsID);
                cmd.Parameters.Add("kostID", kostID);
                cmd.Parameters.Add("reportDescription", reportDescription);
                App.connection.Open();
                cmd.ExecuteNonQuery();
                SweetAlert.Show("Rent", $"Report Succesfully", msgButton: SweetAlertButton.OK);
                App.connection.Close();
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {   
            this.Close();
        }
    }
}
