﻿using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostUpdateKost.xaml
    /// </summary>
    public partial class OwnerKostUpdateKost : Page
    {
        ActKost kost;
        public OwnerKostUpdateKost(ActKost kost)
        {
            InitializeComponent();
            this.kost = kost;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            addImageBtnImage(kost.Image);
            textName.Text = kost.Name;
            textAddress.Text = kost.Alamat;
            textDesc.Text = kost.Description;
            textDenda.Text = kost.Denda.ToString();
            textTelat.Text = kost.DueDay.ToString();
        }
        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                addImageBtnImage(bmp);
            }
        }

        BitmapImage bimp;
        void addImageBtnImage(BitmapImage bmp)
        {
            btnImage.Content = null;
            Image newImg = new Image();
            newImg.Name = "imgPhoto";
            newImg.Stretch = Stretch.UniformToFill;
            newImg.Source = bimp = bmp;
            newImg.HorizontalAlignment = HorizontalAlignment.Center;
            btnImage.Content = newImg;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to Save ?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.NO)
                return;

            string name = textName.Text;
            string addr = textAddress.Text;
            string desc = textDesc.Text;
            string denda = textDenda.Text;
            string telat = textTelat.Text;
            
            string title = "Uh Oh";
            string message = "";
            bool valid = false;
            if (bimp == null)
            {
                message = "Please Add Image first";
            }
            else if (new string[] { name, addr, desc, denda, telat }.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else if (denda.ToCharArray().Any(c => !Char.IsDigit(c)))
            {
                message = "Invalid number (denda)";
            }
            else if (telat.ToCharArray().Any(c => !Char.IsDigit(c)))
            {
                message = "Invalid number (batas)";
            }
            else valid = true;

            if (!valid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
            }
            else
            {
                string query = "UPDATE KOST set " +
               "KOST_NAME = :NAME,KOST_ALAMAT = :alamat,KOST_IMAGE = :image,KOST_DESCRIPTION = :dc,KOST_DENDA_TELAT  = :dt,KOST_BATAS_HARI_TELAT=:ht WHERE kost_id = :KODE";
                OracleCommand cmd = new OracleCommand(query, App.connection);
                cmd.Parameters.Add(":NAME", name);
                cmd.Parameters.Add(":alamat", addr);
                cmd.Parameters.Add(":image", BitmapConverter.GetBase64FromBitmap(bimp));
                cmd.Parameters.Add(":dc", desc);
                cmd.Parameters.Add(":dt", int.Parse(denda));
                cmd.Parameters.Add(":ht", int.Parse(telat));
                cmd.Parameters.Add(":kode", kost.Id);
                App.connection.Open();
                cmd.ExecuteNonQuery();
                App.connection.Close();

                kost.Name = name;
                kost.Image = bimp;
                kost.Alamat = addr;
                kost.Description = desc;
                kost.Denda = int.Parse(denda);
                kost.DueDay = int.Parse(telat);

                // Kasih Message
                SweetAlert.Show("Success", "Kost successfully updated!", msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new OwnerKostDetailKost(kost));
            }
          

        }
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }
    }
}
