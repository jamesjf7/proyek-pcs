﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class Kost
    {
        private OwnerKost owner;
        private string name;
        private string alamat;
        private BitmapImage image;
        private string description;
        private int denda;
        private int dueDay;
        private List<Fasilitas> listFasilitas;
        private List<RoomKost> listRoom;

        public OwnerKost Owner { get => owner; set => owner = value; }
        public string Name { get => name; set => name = value; }
        public string Alamat { get => alamat; set => alamat = value; }
        public BitmapImage Image { get => image; set => image = value; }
        public string Description { get => description; set => description = value; }
        public int Denda { get => denda; set => denda = value; }
        public int DueDay { get => dueDay; set => dueDay = value; }
        public List<Fasilitas> ListFasilitas { get => listFasilitas; set => listFasilitas = value; }
        public List<RoomKost> ListRoom { get => listRoom; set => listRoom = value; }

        public Kost(OwnerKost owner, string name, string alamat, BitmapImage image, string description, int denda, int dueDay)
        {
            this.owner = owner;
            this.name = name;
            this.alamat = alamat;
            this.image = image;
            this.description = description;
            this.denda = denda;
            this.dueDay = dueDay;
            this.listFasilitas = new List<Fasilitas>();
            this.listRoom = new List<RoomKost>();
        }

        public Kost(Kost kost){
            this.assignFromKost(kost);
        }

        public void assignFromKost(Kost kost, bool copyObject=true) {
            Name = kost.name;
            Alamat = kost.alamat;
            Image = kost.image;
            Description = kost.description;
            Denda = kost.denda;
            DueDay = kost.dueDay;
            // Object
            if (copyObject) {
                Owner = kost.Owner;
                ListFasilitas = kost.ListFasilitas;
                ListRoom = kost.ListRoom;
            }
        }

        public bool isFasilitasValid()
        {
            foreach (var fasilitas in listFasilitas) {
                if (fasilitas.Name == "" || fasilitas.Image == null)
                    return false;
            }
            return true;
        }

        public int Floors { get => this.ListRoom.Aggregate(0, (mx, cr) => Math.Max(mx, cr.Lantai));}

        public bool isFloorEmpty(int floor) => this.ListRoom.Where(room => room.Lantai == floor).Count() == 0;
        public bool isValidRoomName(string name,RoomKost room=null) => 
            this.ListRoom.All(ro => ro==room || ro.Name.ToLower() != name.ToLower());
    }

    public class ActKost : Kost
    {
        private string id;
        private DateTime created;
        private int status;
        private float rating;

        public string Id { get => id; set => id = value; }
        public DateTime Created { get => created; set => created = value; }
        public int Status { get => status; set => status = value; }
        public float Rating { get => rating; set => rating = value; }

        public ActKost(Kost kost, string id, DateTime created, int status, float rating=-1) : base(kost)
        {
            Id = id;
            Created = created;
            Status = status;
            Rating = rating;
        }
        
        void assignFromActKost(ActKost actKost){
            Id = actKost.Id;
            Created = actKost.Created;
            Status = actKost.status;
            assignFromKost(actKost);
        }

    }

    public class Fasilitas
    {
        private string name;
        private BitmapImage image;

        public string Name { get => name; set => name = value; }
        public BitmapImage Image { get => image; set => image = value; }

        public Fasilitas(string name, BitmapImage image)
        {
            this.Name = name;
            this.Image = image;
        }

        public Fasilitas(Fasilitas fas)
        {
            assignFromFasilitas(fas);
        }

        public void assignFromFasilitas(Fasilitas fasilitas)
        {
            Name = fasilitas.Name;
            Image = fasilitas.Image;
        }
    }

    public class ActFasilitas : Fasilitas
    {
        public string Id { get; set; }
        public ActFasilitas(Fasilitas fasilitas, string id) : base(fasilitas)
        {
            this.Id = id;
        }
    }

    public class RoomKost
    {
        private string name;
        private int lantai;
        private BitmapImage image;
        private int harga;
        private string description;
        private int gender;

        public string Name { get => name; set => name = value; }
        public int Lantai { get => lantai; set => lantai = value; }
        public BitmapImage Image { get => image; set => image = value; }
        public int Harga { get => harga; set => harga = value; }
        public string Description { get => description; set => description = value; }
        public int Gender { get => gender; set => gender = value; }
        public string StrGender => new[] { "Man", "Woman", "Both" }[Gender];

        public RoomKost(string name, int lantai, BitmapImage image, int harga, string description, int gender)
        {
            this.name = name;
            this.lantai = lantai;
            this.image = image;
            this.harga = harga;
            this.description = description;
            this.gender = gender;
        }
        public RoomKost(RoomKost room)
        {
            assignFromRoomKost(room);
        }

        public void assignFromRoomKost(RoomKost room)
        {
            name = room.name;
            lantai = room.lantai;
            image = room.image;
            harga = room.harga;
            description = room.description;
            gender = room.gender;
        }
    }

    public class ActRoomKost : RoomKost
    {
        public string Id { get; set; }
        public int HargaNext { get; set; }
        public int Status { get; set; }
        public string Occupant { get; set;}
        public string StrStatus => new[] {"Deleted","UnAvailable","Available", $"Occupied\n({Occupant})" }[Status+2];
        public string DispStatus => new[] { "Deleted", "UnAvailable", "Available", $"Occupied" }[Status + 2];

        public ActRoomKost(RoomKost room, string id, int hargaNext, int status, string occupant="") : base(room)
        {
            Id = id;
            HargaNext = hargaNext;
            Status = status;
            Occupant = occupant;
        }
        
        public static int UsedRooms(List<ActRoomKost> listRoom) => listRoom.Aggregate(0, (acc, room) => room.Status == 1 ? acc + 1 : acc);

        public static int CheapestRoom(List<ActRoomKost> listRoom) => listRoom.Aggregate(int.MaxValue, (mn, cr) => Math.Min(mn, cr.Harga));
    }
}
