﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostHome.xaml
    /// </summary>
    public partial class OwnerKostHome : Page
    {
        public OwnerKostHome()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            textOwner.Text = $"{App.User.Name}!";
            // Hid Child
            foreach (UIElement child in gridMenu.Children) {
                child.Visibility = Visibility.Collapsed;
            }
            // Intro
            DispatcherTimer t1 = new DispatcherTimer {
                Interval = new TimeSpan(0, 0, 0, 0, 500)
            };
            t1.Tick += (object t, EventArgs te) => {
                t1.Stop();
                cardMenu.Visibility = Visibility.Visible;
                // Buat Timer
                DispatcherTimer timer = new DispatcherTimer {
                    Interval = new TimeSpan(0, 0, 0, 0, 150)
                };
                timer.Tick += tick;
                timer.Start();
            };
            t1.Start();
            // Event Grid
            generateCardClick();
        }

        Random rand = new Random();
        int ctr = 0;
        void tick(object sender, EventArgs e) {
            var timer = sender as DispatcherTimer;
            bool change = false;
            while (!change) {
                int rIdx = rand.Next(4);
                UIElement child = gridMenu.Children[rIdx];
                if (child.Visibility != Visibility.Visible) {
                    child.Visibility = Visibility.Visible;
                    change = true;
                }
            }
            if (++ctr == 4) {
                timer.Stop();
            }
        }

        void generateCardClick() {
            int index = -1;
            foreach (UIElement child in gridMenu.Children) {
                // Navigate
                child.MouseDown += (object sender, MouseButtonEventArgs e)=>{
                    index = gridMenu.Children.IndexOf(child);
                };
                child.MouseUp += (object sender, MouseButtonEventArgs e) => {
                    int temp = gridMenu.Children.IndexOf(child);
                    if(index == temp && index != -1) {
                        Page page = new Page[]{
                            new OwnerKostCreateKost1(), new OwnerKostMyKost(),
                            new OwnerKostRoomRequest(), new OwnerKostNews()
                        }[index];
                        App.Actmenu.navigate(page);
                    }
                    index = -1;
                };
                // Hover
                Border border = ((child as TransitioningContent).Content as Card).Content as Border;
                SolidColorBrush color1 = border.Background as SolidColorBrush;
                SolidColorBrush color2 = new SolidColorBrush {
                    Color = new Color {
                        A = 6*16 + 6, // #66
                        R = color1.Color.R,
                        G = color1.Color.G,
                        B = color1.Color.B,
                    }
                };
                child.MouseEnter += (object sender, MouseEventArgs m)=>{
                    border.Background = color2;
                };
                child.MouseLeave += (object sender, MouseEventArgs m) => {
                    border.Background = color1;  
                };
            }
        }
    }
}
