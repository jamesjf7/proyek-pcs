﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewListJurusan.xaml
    /// </summary>
    public partial class AdminViewListJurusan : Page
    {
        DataTable dt;
        public AdminViewListJurusan()
        {
            InitializeComponent();
            load();
        }
        public void load()
        {
            OracleDataAdapter da = new OracleDataAdapter("select JURUSAN_ID as ID,JURUSAN_KODE as Kode , JURUSAN_NAMA as Nama from jurusan", App.connection);
            dt = new DataTable();
            da.Fill(dt);
            dgjurusan.ItemsSource = dt.DefaultView;
        }
        private void btcreate_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new AdminAddJurusan());
        }

        private void dgjurusan_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int idx = dgjurusan.SelectedIndex;
            if (idx == -1) return;
            string id=dt.Rows[idx]["ID"].ToString();
            int kode=int.Parse(dt.Rows[idx]["Kode"].ToString());
            string nama=dt.Rows[idx]["Nama"].ToString();
            App.Actmenu.navigate(new UpdateDeleteJurusan(kode,nama,id));
        }
    }
}
