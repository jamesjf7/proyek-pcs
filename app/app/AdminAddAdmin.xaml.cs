﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System.Data;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminAddAdmin.xaml
    /// </summary>
    public partial class AdminAddAdmin : Page
    {
        bool redirAdmin;
        public AdminAddAdmin(bool adminmenu = true)
        {
            InitializeComponent();
            redirAdmin = adminmenu;
        }
        BitmapImage img = null;
        OpenFileDialog op;
        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            string name = textName.Text;
            string user = textUsername.Text.ToLower();
            string pass1 = textPass1.Password;
            string pass2 = textPass2.Password;
            var tempImage = img;
            string title = "Uh Oh";
            string message = "";
            bool invalid = true;
            if (tempImage == null)
            {
                message = "Please Add Image first";
            }
            else if (new String[] { name, user, pass1, pass2 }.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else if (!user.ToCharArray().All(c => Char.IsLetterOrDigit(c)))
            {
                message = "Username can only contain letter and number";
            }
            else if (pass1 != pass2)
            {
                message = "Confirm Password must be same with Password";
            }
            else invalid = false;

            // Give Message
            if (invalid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
                return;
            }

            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand()
            {
                Connection = App.connection,
                CommandText = "generateAdmin",
                CommandType = CommandType.StoredProcedure
            };
            // Buat Return
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            // Parameter Varchar2
            List<KeyValuePair<string, string>> listParam = new List<KeyValuePair<string, string>>();
            listParam.Add(new KeyValuePair<string, string>("name", name));
            listParam.Add(new KeyValuePair<string, string>("username", user));
            listParam.Add(new KeyValuePair<string, string>("password", pass1));
            foreach (var pair in listParam)
            {
                cmd.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = pair.Key,
                    OracleDbType = OracleDbType.Varchar2,
                    Value = pair.Value
                });
            }
            // Parameter Clob
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "image",
                OracleDbType = OracleDbType.Clob,
                Value = BitmapConverter.GetBase64FromFile(op.FileName)
            });
            App.connection.Open();
            cmd.ExecuteNonQuery();
            string result = cmd.Parameters["ret"].Value.ToString();
            App.connection.Close();
            if (result == "0")
            {
                SweetAlert.Show("Whoops", "Username already exists", msgImage: SweetAlertImage.ERROR);
            }
            else
            {
                SweetAlert.Show("Congratulation", "You have successfully register", msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(redirAdmin? new AdminViewAdmin() as Page : new Login());
            }
        }

        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }
    }
}
