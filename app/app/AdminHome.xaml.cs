﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminHome.xaml
    /// </summary>
    public partial class AdminHome : Page
    {
        public AdminHome()
        {
            InitializeComponent();
        }
        private void generate_Click(object sender, RoutedEventArgs e)
        {
            
            DateTime date1 = start.SelectedDate.Value;
            DateTime date2 = end.SelectedDate.Value;
            if (date2.CompareTo(date1) <= 0)
            {
                SweetAlert.Show("Whoops", "[Start Date > End Date] Error", msgImage: SweetAlertImage.ERROR);
                return;
            }
            // Activate Viewer
            viewer.IsEnabled = true;

            // Show Report
            ReportPendapatan rp = new ReportPendapatan();
            rp.SetDatabaseLogon("proyekpcs", "proyekpcs");
                // Param
                rp.SetParameterValue("date", date1);
                rp.SetParameterValue("enddate", date2);
                viewer.ViewerCore.ReportSource = rp;

        }

        private void BtnTagih_Click(object sender, RoutedEventArgs e) {
            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand() {
                Connection = App.connection,
                CommandText = "tagih",
                CommandType = CommandType.StoredProcedure
            };
            App.openConnection();
            cmd.ExecuteNonQuery();
            App.closeConnection();
            SweetAlert.Show("Info", "Trigger Tagih Success!", msgImage: SweetAlertImage.INFORMATION);
        }

        private void BtnKick_Click(object sender, RoutedEventArgs e) {
            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand() {
                Connection = App.connection,
                CommandText = "kick",
                CommandType = CommandType.StoredProcedure
            };
            App.openConnection();
            cmd.ExecuteNonQuery();
            App.closeConnection();
            SweetAlert.Show("Info", "Trigger Kick Success!", msgImage: SweetAlertImage.INFORMATION);
        }
    }
}
