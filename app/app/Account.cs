﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class Account
    {
        private string id;
        private string name;
        private int saldo;
        private BitmapImage image;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Saldo { get => saldo; set => saldo = value; }
        public BitmapImage Image { get => image; set => image = value; }

        public Account(string id, string name, int saldo, BitmapImage bimp)
        {
            this.id = id;
            this.name = name;
            this.saldo = saldo;
            this.image = bimp;
        }
    }
}
