﻿using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostCreateKost1.xaml
    /// </summary>
    public partial class OwnerKostCreateKost1 : Page
    {
        Kost kost = null;
        public OwnerKostCreateKost1()
        {
            InitializeComponent();
        }

        public OwnerKostCreateKost1(Kost kost)
        {
            InitializeComponent();
            this.kost = kost;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (kost != null)
            {
                addImageBtnImage(kost.Image);
                textName.Text = kost.Name;
                textAddress.Text = kost.Alamat;
                textDesc.Text = kost.Description;
                textDenda.Text = kost.Denda.ToString();
                textTelat.Text = kost.DueDay.ToString();
            }
        }

        BitmapImage bimp;
        void addImageBtnImage(BitmapImage bmp)
        {
            btnImage.Content = null;
            Image newImg = new Image();
            newImg.Name = "imgPhoto";
            newImg.Stretch = Stretch.UniformToFill;
            newImg.Source = bimp = bmp;
            newImg.HorizontalAlignment = HorizontalAlignment.Center;
            btnImage.Content = newImg;
        }

        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                addImageBtnImage(bmp);
            }
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            string name = textName.Text;
            string addr = textAddress.Text;
            string desc = textDesc.Text;
            string denda = textDenda.Text;
            string telat = textTelat.Text;

            string title = "Uh Oh";
            string message = "";
            bool valid = false;
            if (bimp == null)
            {
                message = "Please Add Image first";
            }
            else if (new string[] { name, addr, desc, denda, telat }.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else if (denda.ToCharArray().Any(c => !Char.IsDigit(c)))
            {
                message = "Invalid number (denda)";
            }
            else if (telat.ToCharArray().Any(c => !Char.IsDigit(c)))
            {
                message = "Invalid number (batas)";
            }
            else valid = true;

            if (!valid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
            }
            else
            {
                var tempKost = new Kost(
                    App.User as OwnerKost,
                    name, addr, bimp, desc,
                    Convert.ToInt32(denda),
                    Convert.ToInt32(telat)
                );

                switch (kost)
                {
                    case null:
                        kost = tempKost;
                        break;
                    default:
                        kost.assignFromKost(tempKost,false);
                        break;
                }
                App.Actmenu.navigate(new OwnerKostCreateKost2(kost));
            }
        }
    }
}
