﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for UpdateDeleteJurusan.xaml
    /// </summary>
    public partial class UpdateDeleteJurusan : Page
    {
        string query,nama,id;
        int kode;
        public UpdateDeleteJurusan(int kodejur,string namajur,string idjur)
        {
            InitializeComponent();
            tbkode.Text = kodejur+"";
            kode = kodejur;
            tbnama.Text=nama = namajur;
            id = idjur;
        }
        private void btnupdate_Click(object sender, RoutedEventArgs e)
        {
            nama = tbnama.Text;
            kode = int.Parse(tbkode.Text);
            query = $"UPDATE JURUSAN SET JURUSAN_NAMA='{nama}',JURUSAN_KODE={kode} WHERE JURUSAN_ID='{id}'";
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                App.connection.Close();
                SweetAlert.Show("Message","Berhasil Update",SweetAlertButton.OK,SweetAlertImage.SUCCESS);
            }
            catch (Exception)
            {
                App.connection.Close();
                SweetAlert.Show("Message", "Gagal Update", SweetAlertButton.OK, SweetAlertImage.WARNING);

            }
            App.Actmenu.navigate(new AdminViewListJurusan());

        }

        private void btndelete_Click(object sender, RoutedEventArgs e)
        {
            query = $"DELETE FROM JURUSAN WHERE JURUSAN_ID='{id}'";
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                App.connection.Close();
                SweetAlert.Show("Message", "Berhasil Delete", SweetAlertButton.OK, SweetAlertImage.SUCCESS);
            }
            catch (Exception)
            {
                App.connection.Close();
                SweetAlert.Show("Message", "Gagal Delete", SweetAlertButton.OK, SweetAlertImage.WARNING);

            }
            App.Actmenu.navigate(new AdminViewListJurusan());
        }
    }
}
