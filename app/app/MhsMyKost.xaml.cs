﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
namespace app
{
    public partial class MhsMyKost : Page
    {
        public MhsMyKost()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string query =  $"select * from kost where kost_id in ( " +
                            $"select k.kost_id from kost k " +
                            $"inner join room r on r.kost_id = k.kost_id " +
                            $"inner join rent ren on ren.room_id = r.room_id " +
                            $"where ren.rent_status = 1 and ren.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' " +
                            $"group by k.kost_id )";
            listKost.Content = (App.Actmenu as ContainerMhs).Generate_List(query,"MyKost");
        }
    }
}
