﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminConfirmOwnerKost.xaml
    /// </summary>
    public partial class AdminConfirmOwnerKost : Page
    {
        public AdminConfirmOwnerKost()
        {
            InitializeComponent();
            string query = "select acc.account_id,acc.account_nama,acc.account_username,acc.account_image,ok.ownerkost_email,ok.ownerkost_telp from account acc join ownerkost ok on acc.account_id=ok.account_id where account_status = 0 and account_type=2";
            svregister.Content = load(query);
        }
        public StackPanel load(string query)
        {
            int idx = 0;
            StackPanel spContainer = new StackPanel();
            ItemsControl ic = new ItemsControl();
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                tc.OpeningEffects.Add(new TransitionEffect(new Random().Next(2) == 0 ? TransitionEffectKind.SlideInFromRight : TransitionEffectKind.SlideInFromLeft));
                StackPanel sp = new StackPanel() { Margin = new Thickness(10) };
                Card card = new Card() { Margin = new Thickness(0, 5, 0, 5) };
                Grid g = new Grid() { Height = 200 };
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                string accountid = reader.GetString(0),
                            accountnama = reader.GetString(1),
                            accountusername = reader.GetString(2),
                            ownerkostemail= reader.GetString(4),
                            ownerkosttelp=reader.GetString(5);
                ImageSource imaccount = BitmapConverter.GetBitmapFromBase64(reader.GetString(3));
                TextBlock textAccountID = new TextBlock() { Text = accountid, FontSize = 16, FontWeight = FontWeights.DemiBold }; // kostID
                TextBlock textAccountNama = new TextBlock() { Text = accountnama, FontSize = 16, FontWeight = FontWeights.Bold }; // kostNama 
                TextBlock textAccountUsername = new TextBlock() { Text = accountusername, FontSize = 16, FontWeight = FontWeights.SemiBold }; // kostAlamat
                TextBlock textAccounttelp = new TextBlock() { Text = ownerkosttelp, FontSize = 16, FontWeight = FontWeights.SemiBold }; // kostAlamat
                TextBlock textEmailDescription = new TextBlock() { Text = ownerkostemail, FontSize = 16 }; // kostDescription
                Grid graccountImage = new Grid() { Background = new ImageBrush() { ImageSource = imaccount } }; // kostImage
                Button btn = new Button() { Name = accountid, Content = "Agree", HorizontalAlignment = HorizontalAlignment.Center };
                btn.Click += (source, ev) => {
                    string query2 = $"update account set account_status=1 where account_id='{accountid}'";
                    OracleCommand cmd2 = new OracleCommand(query2, App.connection);
                    App.connection.Open();
                    cmd2.ExecuteNonQuery();
                    App.connection.Close();
                    App.Actmenu.navigate(new AdminConfirmOwnerKost());
                };
                Button btn2 = new Button() { Name = accountid, Content = "Disagree", HorizontalAlignment = HorizontalAlignment.Center };
                btn2.Click += (source, ev) => {
                    string query2 = $"update account set account_status=-1 where account_id='{accountid}'";
                    OracleCommand cmd2 = new OracleCommand(query2, App.connection);
                    App.connection.Open();
                    cmd2.ExecuteNonQuery();
                    App.connection.Close();
                    App.Actmenu.navigate(new AdminConfirmOwnerKost());
                };
                sp.Children.Add(textAccountID);
                sp.Children.Add(textAccountNama);
                sp.Children.Add(textAccountUsername);
                sp.Children.Add(textAccounttelp);
                sp.Children.Add(textEmailDescription);
                Grid.SetColumn(graccountImage, 0);
                Grid.SetColumn(sp, 1);
                Grid.SetColumn(btn, 2);
                Grid.SetColumn(btn2, 3);
                g.Children.Add(graccountImage);
                g.Children.Add(sp);
                g.Children.Add(btn);
                g.Children.Add(btn2);
                card.Content = g;
                tc.Content = card;
                ic.Items.Add(tc);
                idx++;
            }
            App.connection.Close();
            spContainer.Children.Add(ic);
            return spContainer;
        }
    }
}
