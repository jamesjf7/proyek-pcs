﻿using Microsoft.Win32;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostCreateKost4.xaml
    /// </summary>
    public partial class OwnerKostCreateKost4 : Page
    {
        Kost kost;
        RoomKost room=null;
        bool change = false;
      
        public OwnerKostCreateKost4(Kost kost,int lantai)
        {
            InitializeComponent();
            this.kost = kost;
            TFl.Text = lantai.ToString();
        }
        public OwnerKostCreateKost4(Kost kost,RoomKost data)
        {
            InitializeComponent();
            this.kost = kost;
            room = data;

            // Update Display
            textTitle.Text = "Update Room";
            TRn.Text = room.Name;
            TFl.Text = room.Lantai.ToString();
            TRh.Text = room.Harga.ToString();
            TRd.Text = room.Description.ToString();

            btnImage.Content = null;
            Image newImg = new Image();
            newImg.Name = "imgPhoto";
            newImg.Stretch = Stretch.UniformToFill;
            newImg.HorizontalAlignment = HorizontalAlignment.Center;
            newImg.Source = img = room.Image;
            btnImage.Content = newImg;

            new RadioButton[] { Rm, Rw,both }[room.Gender].IsChecked = true;
            btnAddRoom.Content = "Save Changes";
            change = !change;
        }

        BitmapImage img = null;
        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                        "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                        "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }

        private void BtnAddRoom_Click(object sender, RoutedEventArgs e)
        {

            if (TRn.Text=="" ||TRh.Text=="0" || TRh.Text=="" || TRd.Text=="" || (Rm.IsChecked==false && Rw.IsChecked==false && both.IsChecked==false) || img==null)
            {
                SweetAlert.Show("Opppsss", "Your field is not ready", msgImage: SweetAlertImage.WARNING);
                return;
            }

            bool kembar = kost.ListRoom.Any(temp => temp != room && temp.Name == TRn.Text);
            if (kembar)
            {
                SweetAlert.Show("Opppsss", "Room name is already used", msgImage: SweetAlertImage.WARNING);
                return;
            }

            if (!TRh.Text.ToCharArray().All(c => Char.IsDigit(c)))
            {
                SweetAlert.Show("Uh oh", "Room price must be number", msgImage: SweetAlertImage.WARNING);
                return;
            }

            kost.ListRoom.Remove(room);

            string nama = TRn.Text;
            int lantai = int.Parse(TFl.Text);
            var tempImage = img;
            int harga = int.Parse(TRh.Text);
            string desc = TRd.Text;
            int p = Rm.IsChecked == true ? 0 : Rw.IsChecked == true ? 1 : 2;

            kost.ListRoom.Add(new RoomKost(nama, lantai, tempImage, harga, desc, p));

            SweetAlert.Show("Congratulations",change==true ? "Update Success" : "Create room success ", msgImage: SweetAlertImage.SUCCESS);
            App.Actmenu.navigate(new OwnerKostCreateKost3(kost,lantai));
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to Back?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            int flr = Convert.ToInt32(TFl.Text);
            if (result == SweetAlertResult.YES)
                App.Actmenu.navigate(new OwnerKostCreateKost3(kost,flr));
               
        }
    }
}
