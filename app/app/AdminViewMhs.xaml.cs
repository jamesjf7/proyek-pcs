﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using System.Data;
namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewMhs.xaml
    /// </summary>
    public partial class AdminViewMhs : Page
    {
        DataTable dt;
        public AdminViewMhs()
        {
            InitializeComponent();
            load();
        }
        public void load()
        {
            OracleDataAdapter da = new OracleDataAdapter("select m.Account_id as Kode , acc.account_nama as Nama , acc.account_username as Username , case when m.mahasiswa_gender=0 then 'Male' else 'Female' end as Gender from mahasiswa m join account acc on m.account_id=acc.account_id", App.connection);
            dt = new DataTable();
            da.Fill(dt);
            dgmhs.ItemsSource = dt.DefaultView;
        }
        private void dgmhs_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgmhs.SelectedIndex < 0)
            {
                return;
            }
            int idx = dgmhs.SelectedIndex;
            string kode = dt.Rows[idx][0].ToString();
            App.Actmenu.navigate(new UpdateDeleteMhs(kode));
        }

        private void btcreate_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new AdminAddMhs());
        }
    }
}
