﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class Occupant
    {
        private BitmapImage image;
        private string name;
        private string room;
        private DateTime date;
        private float rating;
        private string rentId;

        public BitmapImage Image { get => image; set => image = value; }
        public string Name { get => name; set => name = value; }
        public string Room { get => room; set => room = value; }
        public DateTime Date { get => date; set => date = value; }
        public float Rating { get => rating; set => rating = value; }
        public string TextRating => rating<0?"New":rating.ToString("0.0");
        public string RentId { get => rentId; set => rentId = value; }

        public Occupant(BitmapImage image, string name, string room, DateTime date, float rating, string rentId) {
            Image = image;
            Name = name;
            Room = room;
            Date = date;
            Rating = rating;
            RentId = rentId;
        }
    }
}
