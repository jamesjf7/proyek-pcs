﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Oracle.DataAccess.Client;
namespace app{
    public partial class App : Application{
        public static OracleConnection connection = new OracleConnection(
            "Data Source=orcl;User Id=proyekpcs;Password=proyekpcs"    
        );
        public static void openConnection() {
            if (connection.State == ConnectionState.Closed){
                connection.Open();
            }
        }
        public static void closeConnection(){
            if (connection.State == ConnectionState.Open){
                connection.Close();
            }
        }
        public static MainWindow MainWindow;
        public static INavigate Actmenu;
        public static Account User;
    }
}
