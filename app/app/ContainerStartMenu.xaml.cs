﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for ContainerStartMenu.xaml
    /// </summary>
    public partial class ContainerStartMenu : Page, INavigate
    {
        public ContainerStartMenu()
        {
            InitializeComponent();
        }

        public void navigate(Page page, object param)
        {
            mainFrame.Content = null;
            mainFrame.NavigationService.Navigate(page,param);
        }

        public void navigate(Page page)
        {
            navigate(page, null);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            navigate(new Login());
        }
    }
}
