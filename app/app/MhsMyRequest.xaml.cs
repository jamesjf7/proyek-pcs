﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
namespace app{
    public partial class MhsMyRequest : Page
    {
        public MhsMyRequest()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // TABEL : RENT DAN ROOM 
            string query = $@"SELECT * FROM RENT RN 
                              INNER JOIN ROOM R ON R.ROOM_ID = RN.ROOM_ID 
                              WHERE RN.MAHASISWA_ID = '{((Mahasiswa)App.User).MhsId}' AND (RN.RENT_STATUS = -1 OR RN.RENT_STATUS = 0)";
           listRequest.Content = (App.Actmenu as ContainerMhs).Generate_List(query, "MyRequest");
        }
    }
}
