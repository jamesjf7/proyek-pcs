﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using System.Data;
namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewDetailKost.xaml
    /// </summary>
    public partial class AdminViewDetailKost : Page
    {
        string kostid;
        int lantai=0;
        public AdminViewDetailKost(string idkost)
        {
            InitializeComponent();
            kostid = idkost;
            floor.Text = lantai.ToString();
            load();
        }
        public void load()
        {
            listroom.Children.Clear();
            using (OracleCommand cmd = new OracleCommand($"SELECT * FROM ROOM WHERE KOST_ID = '{kostid}' and room_lantai={lantai} and not room_status=-2", App.connection))
            {
                App.connection.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                List<StackPanel> listSpContainer = new List<StackPanel>();
                for (int i = 0; i < 3; i++) listSpContainer.Add(new StackPanel() { Margin = new Thickness(5) });
                // buat 3 grid 
                List<UIElement> listElem = new List<UIElement>();
                int idx = 0;
                while (reader.Read())
                {
                    TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                    tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                    tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromRight));
                    StackPanel sp = new StackPanel();
                    Card card = new Card() { Margin = new Thickness(5), Padding = new Thickness(5) };
                     int   roomHarga = int.Parse(reader["room_harga"].ToString()),
                           roomGender = int.Parse(reader["room_gender"].ToString()),
                           roomStatus = int.Parse(reader["room_status"].ToString());
                    string roomID = reader["room_id"].ToString(),
                           roomNama = reader["room_nama"].ToString(),
                           roomDescription = reader["room_description"].ToString();
                    Grid grRoomImage = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) }, Height = 200 };
                    //listElem.Add(grRoomImage);
                    TextBlock textRoomNama = new TextBlock() { Text = roomNama, FontWeight = FontWeights.Bold };
                    TextBlock textRoomHarga = new TextBlock() { Text = $"Rp. {roomHarga},00", FontWeight = FontWeights.SemiBold, Foreground = Brushes.Indigo };
                    TextBlock textRoomDescription = new TextBlock() { Text = roomDescription };
                    string[] stat = {"Man","Woman","Both"};
                    TextBlock textRoomGender = new TextBlock() { Text = stat[roomGender]};
                    sp.Children.Add(grRoomImage);
                    sp.Children.Add(textRoomNama);
                    sp.Children.Add(textRoomHarga);
                    sp.Children.Add(textRoomDescription);
                    sp.Children.Add(textRoomGender);
                    card.Content = sp;
                    tc.Content = card;
                    listSpContainer[idx % 3].Children.Add(tc);
                    idx++;
                }
                Grid.SetColumn(listSpContainer[0], 0);
                Grid.SetColumn(listSpContainer[1], 1);
                Grid.SetColumn(listSpContainer[2], 2);
                listroom.Children.Add(listSpContainer[0]);
                listroom.Children.Add(listSpContainer[1]);
                listroom.Children.Add(listSpContainer[2]);
                App.connection.Close();
            }
        }

        private void btnup_Click(object sender, RoutedEventArgs e)
        {
            lantai++;
            floor.Text = lantai.ToString();
            load();
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            if (lantai - 1 < 0)
            {
                lantai = 0;
            }
            else
            {
                lantai--;
            }
            floor.Text = lantai.ToString();
            load();
        }
    }
}
