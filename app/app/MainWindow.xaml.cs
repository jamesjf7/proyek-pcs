﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;

namespace app{
    public partial class MainWindow : Window{
        public MainWindow(){
            InitializeComponent();
            App.MainWindow = this;
        }
        public void navigateContainer(Container cont, Account user=null)
        {
            INavigate container = null;
            App.User = user;
            if(cont == Container.StartMenu){
                container = new ContainerStartMenu();
            }else if(cont == Container.Admin){
                container = new ContainerAdmin();
            }else if (cont == Container.Mahasiswa){
                container = new ContainerMhs();
            }else if (cont == Container.OwnerKost){
                container = new ContainerOwnerKost();
            }
            btnExit.Visibility = App.User == null ? Visibility.Visible : Visibility.Collapsed;
            App.Actmenu = container;
            MainFrame.Content = null;
            MainFrame.NavigationService.Navigate(container);
            MainFrame.NavigationService.RemoveBackEntry();
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            navigateContainer(0);
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to leave?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes", CancelText = "No"
            }.ShowDialog();

            if(result == SweetAlertResult.YES)
                Application.Current.Shutdown();
        }
    }
}
