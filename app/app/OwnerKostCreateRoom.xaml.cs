﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostCreateRoom.xaml
    /// </summary>
    public partial class OwnerKostCreateRoom : Page
    {
        public OwnerKostCreateRoom(ActKost kost, int floor)
        {
            InitializeComponent();
            this.kost = kost;
            this.floor = floor;
        }

        ActKost kost;
        int floor;
        BitmapImage img = null;
        #region Event
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            textFloor.Text = floor.ToString();
        }
        
        private void BtnImage_Click(object sender, RoutedEventArgs e) {
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                        "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                        "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == DialogResult.OK) {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e) {
            string name   = textName.Text;
            string price  = textPrice.Text;
            string desc   = textDescription.Text;
            int gender    = getGender();

            // Check Error
            bool err = true;
            string msg = "Room Successfully Created!";
            if(new[]{name,price,desc}.Any(f => f == "") || img == null || gender == -1) {
                msg = "Your field are empty!";
            } else if (!kost.isValidRoomName(name)) {
                msg = "Room name already exist in your kost!";
            } else if (!price.ToCharArray().All(c => char.IsDigit(c))) {
                msg = "Invalid price!";
            } else if (Convert.ToInt32(price) < 100000) {
                msg = "Minimum price is IDR 100.000!";
            } else {
                err = false;
            }

            if (err) { // Invalid
                SweetAlert.Show("Whoops",msg,msgImage: SweetAlertImage.WARNING);
            } else { // Valid
                string sql = @"
                    insert into room 
                    values('', :kost, :name, :flr, :img, :hrg, 0, :desrciption, :gender, 0)
                ";
                OracleCommand cmd = new OracleCommand(sql, App.connection);
                cmd.Parameters.Add(":kost", kost.Id);
                cmd.Parameters.Add(":name", name);
                cmd.Parameters.Add(":flr", floor);
                cmd.Parameters.Add(new OracleParameter {
                    ParameterName = ":img",
                    OracleDbType = OracleDbType.Clob,
                    Value = BitmapConverter.GetBase64FromBitmap(img)
                });
                cmd.Parameters.Add(":hrg", Convert.ToInt32(price));
                cmd.Parameters.Add(":description", desc);
                cmd.Parameters.Add(":gender", gender);
                App.connection.Open();
                cmd.ExecuteNonQuery();
                App.connection.Close();
                // Give Message and Navigate to Update Room
                SweetAlert.Show("Success",msg,msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new OwnerKostUpdateRoom(kost,floor));
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e) {
            App.Actmenu.navigate(new OwnerKostUpdateRoom(kost,floor));
        }
        #endregion

        #region Function
        int getGender() {
            return (bool)radio1.IsChecked ? 0 : (bool)radio2.IsChecked ? 1 : (bool)radio3.IsChecked ? 2 : -1;
        }
        #endregion
    }
}
