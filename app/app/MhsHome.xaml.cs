﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;    
using Oracle.DataAccess.Client;
using System.Data;
namespace app{
    public partial class MhsHome : Page{
        public MhsHome(){
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            im_mhs_image.ImageSource = App.User.Image;
            tb_mhs_nama.Text = $"Nama : {App.User.Name}";
            tb_mhs_id.Text = $"Id : {(App.User as Mahasiswa).Username}";
            tb_mhs_gender.Text = $"Gender : " + (((Mahasiswa)App.User).Gender == 0 ? "Man" : "Woman");
            tb_mhs_saldo.Text = $"Saldo : {CurrencyConverter.ToCurrency(App.User.Saldo)}";

            // pengecekan apakah mahasiswa tidak ada kost
            using (OracleCommand cmd = new OracleCommand(
                $"select * from rent ren " +
                $"inner join room r on r.room_id = ren.room_id " +
                $"inner join kost k on k.kost_id = r.kost_id " +
                $"where ren.rent_status = 1 and ren.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}'"
                , App.connection
            )) {
                App.connection.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows) {
                    Button btn = new Button() { Content = "See Your Kost", HorizontalAlignment = HorizontalAlignment.Center, Margin = new Thickness(10, 10, 10, 10) };
                    btn.Click += (source, ev) => App.Actmenu.navigate(new MhsMyKost());
                    kost.Children.Add(new TextBlock() { Text = "Organize your kost here", FontSize = 18, HorizontalAlignment = HorizontalAlignment.Center, Margin = new Thickness(10, 10, 10, 10) });
                    kost.Children.Add(btn);
                }
                else {
                    Button btn = new Button() { Name = "search", Content = "Search Kost Right Now", HorizontalAlignment = HorizontalAlignment.Center, Margin = new Thickness(10, 10, 10, 10) };
                    btn.Click += search_click;
                    kost.Children.Add(new TextBlock() { Text = "You don't have any kost right now", FontSize = 18, HorizontalAlignment = HorizontalAlignment.Center, Margin = new Thickness(10, 10, 10, 10) });
                    kost.Children.Add(btn);
                }
                App.connection.Close();
            }

            // pengecekan ketika mahasiswa harus review
            //using (OracleCommand cmd = new OracleCommand(){
            //    CommandText = "hasMhsReview",
            //    CommandType = CommandType.StoredProcedure,
            //    Connection = App.connection
            //}){
            //    cmd.Parameters.Add(new OracleParameter()
            //    {
            //        ParameterName = "ret",
            //        OracleDbType = OracleDbType.Int32,
            //        Direction = ParameterDirection.ReturnValue
            //    });
            //    cmd.Parameters.Add(new OracleParameter()
            //    {
            //        ParameterName = "mhsID",
            //        Value = ((Mahasiswa)App.User).MhsId,
            //        OracleDbType = OracleDbType.Varchar2,
            //        Direction = ParameterDirection.Input
            //    });
            //    App.connection.Open();
            //    cmd.ExecuteNonQuery();
            //    App.connection.Close();
            //    int ret = int.Parse(cmd.Parameters["ret"].Value.ToString());
            //    if (ret == 0)
            //    {
            //        new MhsDialog("ReviewKost", "Ok").ShowDialog();
            //    }
            //}
            using (OracleCommand cmd = new OracleCommand($@"
                    SELECT r.rent_id, a.mustReview 
                    FROM RENT r
                    inner join(
                        select rent_id, nvl(count(*),0) as mustReview
                        from dtagih
                        group by rent_id
                        union
                        select rent_id, nvl(count(*),0)
                        from utang
                        group by rent_id
                    )a on r.rent_id = a.rent_id
                    WHERE MAHASISWA_ID = '{(App.User as Mahasiswa).MhsId}' AND RENT_STATUS = 1",App.connection)){
                App.openConnection();
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read()){
                    string rentID = reader["rent_id"].ToString();
                    int mustReview = int.Parse(reader["mustReview"].ToString());
                    OracleCommand cmd2 = new OracleCommand($"SELECT COUNT(*) FROM REVIEW WHERE RENT_ID = '{rentID}'", App.connection);
                    int countReview = int.Parse(cmd2.ExecuteScalar().ToString());
                    if (countReview < mustReview)
                    {
                        new MhsDialog("ReviewRoom", "Ok", rentID).ShowDialog();
                    }
                }
                App.closeConnection();
            }
        }
        private void search_click(object sender, EventArgs e) {
            App.Actmenu.navigate(new MhsSearchKost());
        }
        private void detail_click(object sender, EventArgs e){   }
    }
}
