﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class BitmapConverter
    {
        private BitmapConverter(){}

        public static Byte[] GetArrayFromFile(string path)
        {
            Byte[] data = null;

            FileInfo fileInf = new FileInfo(path);
            long numBytes = fileInf.Length;

            FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read);

            BinaryReader bReader = new BinaryReader(fStream);

            data = bReader.ReadBytes((int)numBytes);
            return data;
        }

        public static string GetBase64FromFile(string path) => Convert.ToBase64String(GetArrayFromFile(path));

        public static BitmapImage GetBitmapFromBase64(string base64)
        {
            var nottoxic = Convert.FromBase64String(base64);

            MemoryStream stream = new MemoryStream();
            stream.Write(nottoxic, 0, nottoxic.Length);
            stream.Position = 0;

            Image img = Image.FromStream(stream);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();

            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;
            bi.EndInit();
            return bi;
        }

        public static string GetBase64FromBitmap(BitmapImage bitmapImage){
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return Convert.ToBase64String(data);
        }
    }
}
