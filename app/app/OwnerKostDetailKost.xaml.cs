﻿using MaterialDesignThemes.Wpf;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostDetailKost.xaml
    /// </summary>
    public partial class OwnerKostDetailKost : Page
    {
        ActKost kost;
        List<ActRoomKost> rooms;
        List<Occupant> occupants;
        List<Mutation> mutations;
        List<Review> reviews; 
        List<Utang> utangs;
        DataTable dtBlacklist1;
        DataTable dtBlacklist2;
        public OwnerKostDetailKost(ActKost kost)
        {
            InitializeComponent();
            this.kost = kost;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Load Dulu dari DB
            loadGenerateFasilitas();
            loadGenerateRoom();
            loadGenerateOccupant();
            loadGenerateNews();
            loadGenerateRequest();
            loadGenerateReview();
            loadGenerateMutation();
            loadGenerateUtang();
            loadGenerateBlacklist1();
            loadGenerateBlacklist2();

            // Load Status
            loadProfit();
            loadStarRate();

            // Assign Ke tampilan
            textName.Text = kost.Name;
            textAddress.Text = kost.Alamat;
            imageKost1.Source = imageKost2.Source = kost.Image;
            textDesc.Text = kost.Description;

            // Kost Detail
            // Note: yang ada di func
            // Profit, StarRate
            textOccup.Text = $"{ActRoomKost.UsedRooms(rooms)} / {rooms.Count}";
            textSince.Text = kost.Created.ToString("dd MMMM yyyy");
            textStatus.Text = new[] { "Inactive", "Active" }[kost.Status];
            textPrice.Text = $"{CurrencyConverter.ToCurrency(ActRoomKost.CheapestRoom(rooms))} / Month";
            textLateFee.Text = CurrencyConverter.ToCurrency(kost.Denda);
            textDayLimit.Text = $"{kost.DueDay} Day(s)";

            // Nav
            IdxNav = 0;

            // Report Height
            cViewer.Height = this.ActualHeight - 80;
        }

        #region Watch Property
        // Watch Property
        private int _idxNav;
        public int IdxNav
        {
            get => _idxNav;
            set{
                _idxNav = value;
                for (int i = 0; i < gridNav.Children.Count; i++) {
                    Button btn = gridNav.Children[i] as Button;
                    btn.Background = BrushGen.FromString(i == IdxNav ? "#673AB7" : "#fef");
                    btn.Foreground = BrushGen.FromString(i != IdxNav ? "#673AB7" : "#fef");
                }
                for (int j = 1; j < stackContent.Children.Count; j++) {
                    bool vis = new[] { j, 0 }.Any(n => n == IdxNav);
                    stackContent.Children[j].Visibility = vis ?
                        Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private int _idxReview;
        public int IdxReview {
            get => _idxReview;
            set {
                _idxReview = value;
                // Update nav
                for (int i = 0; i < wpRating.Children.Count; i++) {
                    Button btn = wpRating.Children[i] as Button;
                    bool curr = i == IdxReview;
                    btn.Foreground  = 
                    btn.BorderBrush = BrushGen.FromString(curr ? "#FF1493" : "#666");
                }

                // Update Content
                foreach (Card card in stackReview.Children) {
                    // Card > Grid > Stackpanel(Child2) > Rating(Child2)
                    RatingBar rating = ((
                        card.Content as Grid)
                        .Children[1] as StackPanel)
                        .Children[1] as RatingBar;

                    int star = IdxReview == 0 ? -1 : 6 - IdxReview;
                    card.Visibility = star == -1 || star == rating.Value ?
                        Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        private int _profit;
        public int Profit {
            get => _profit;
            set {
                _profit = value;
                textProfit.Text = CurrencyConverter.ToCurrency(value);
            }
        }

        private float _starRate;
        public float StarRate {
            get => _starRate;
            set {
                _starRate = value;
                bool rated = StarRate >= 0;
                rateBar.Value = rated ? (int)Math.Round(StarRate) : 5;
                rateBar.Foreground = BrushGen.FromString(rated? "#DAA520" : "#7CFC00");
                string strRating = (rated ? StarRate.ToString("0.0") : "New");
                textRating.Text = $"({strRating})";
                textRating2.Text = strRating;
            }
        }

        private bool _flipBlack1;
        public bool FlipBlack1 {
            get => _flipBlack1;
            set {
                if (FlipBlack1 == value) return;
                _flipBlack1 = value;
                // Flip card
                // blk1 => No Data, blk2 => Data Mhs
                var card1 = FlipBlack1 ? cardBlk2 : cardBlk1;
                var card2 = FlipBlack1 ? cardBlk1 : cardBlk2;
                // Visibility
                card1.Visibility = Visibility.Collapsed;
                card1.Visibility = Visibility.Visible;
                // Index
                card1.SetValue(Panel.ZIndexProperty, 10);
                card2.SetValue(Panel.ZIndexProperty, 9);
            }
        }

        private bool _flipBlack2;
        public bool FlipBlack2 {
            get => _flipBlack2;
            set {
                if (FlipBlack2 == value) return;
                _flipBlack2 = value;
                // Flip card
                // blk1 => No Data, blk2 => Data Mhs
                var card1 = FlipBlack2 ? cardBlk4 : cardBlk3;
                var card2 = FlipBlack2 ? cardBlk3 : cardBlk4;
                // Visibility
                card1.Visibility = Visibility.Collapsed;
                card1.Visibility = Visibility.Visible;
                // Index
                card1.SetValue(Panel.ZIndexProperty, 10);
                card2.SetValue(Panel.ZIndexProperty, 9);
            }
        }

        private string _keyBlk1;
        public string KeyBlk1 {
            get => _keyBlk1;
            set {
                _keyBlk1 = value;
                DataView dv = dtBlacklist1.DefaultView;
                if(KeyBlk1 != "") {
                    dv.RowFilter = $"Name LIKE '%{KeyBlk1}%' OR NRP LIKE '%{KeyBlk1}%'";
                } else {
                    dv.RowFilter = "";
                }
                dgRent.ItemsSource = dv;
            }
        }

        private string _keyBlk2;
        public string KeyBlk2 {
            get => _keyBlk2;
            set {
                _keyBlk2 = value;
                DataView dv = dtBlacklist2.DefaultView;
                if (KeyBlk2 != "") {
                    dv.RowFilter = $"Name LIKE '%{KeyBlk2}%' OR NRP LIKE '%{KeyBlk2}%'";
                } else {
                    dv.RowFilter = "";
                }
                dgBlacklist.ItemsSource = dv;
            }
        }

        #endregion
    
        private void BtnNav_Click(object sender, RoutedEventArgs e) => IdxNav = gridNav.Children.IndexOf(sender as UIElement);
        
        private void BtnReview_Click(object sender, RoutedEventArgs e) => IdxReview =  wpRating.Children.IndexOf(sender as UIElement);

        #region Load Generate Data

        void loadGenerateFasilitas()
        {
            // Clear Child
            foreach (StackPanel sp in gridFaci.Children)
                sp.Children.Clear();

            string sql = $@"
                select fasilitas_nama, fasilitas_image 
                from fasilitas f where f.kost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add("id", kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            int idx = 0; // idx Stackpanel
            while (reader.Read()){
                Fasilitas fasilitas = new Fasilitas(
                    reader.GetString(0),
                    BitmapConverter.GetBitmapFromBase64(reader.GetString(1))
                );
                generateFasilitas(fasilitas, idx++);
            }
            App.connection.Close();
        }
        void generateFasilitas(Fasilitas fasilitas, int idx){
            // Dynamic Component
            Grid root = new Grid
            {
                Margin = new Thickness(0, 0, 0, 10)
            };
            root.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            root.RowDefinitions.Add(new RowDefinition());
            Border child1 = new Border
            {
                CornerRadius = new CornerRadius(10),
                Height = 100,
                Width = 100,
                BorderThickness = new Thickness(3),
                BorderBrush = BrushGen.FromString("#000"),
                Background = new ImageBrush
                {
                    ImageSource = fasilitas.Image,
                    Stretch = Stretch.UniformToFill,
                    AlignmentX = AlignmentX.Center,
                    AlignmentY = AlignmentY.Center
                }
            };
            TextBlock child2 = new TextBlock
            {
                Text = fasilitas.Name,
                FontSize = 18,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            };
            child2.SetValue(Grid.RowProperty, 1);
            root.Children.Add(child1);
            root.Children.Add(child2);
            // Add to StackPanel
            (gridFaci.Children[idx%5] as StackPanel).Children.Add(root);
        }

        void loadGenerateRoom(){
            // Clear Child
            foreach (StackPanel sp in gridRoom.Children)
                sp.Children.Clear();

            string sql = $@"
                select r1.room_id, room_nama, room_lantai,
	                room_image, room_harga, room_description,
	                room_gender, room_harga_next, room_status, 
                    nvl(a.account_nama, ' ')
                from room r1
                left outer join (
	                select * from rent
	                where rent_status = 1
                )r2 on r1.room_id = r2.room_id
                left outer join mahasiswa m on r2.mahasiswa_id = m.mahasiswa_id
                left outer join account a on m.account_id = a.account_id
                where kost_id = :id and room_status <> -2
                order by 1
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            rooms = new List<ActRoomKost>();
            int idx = 0;
            while (reader.Read()){
                ActRoomKost newRoom = new ActRoomKost(
                    new RoomKost(
                        reader.GetString(1),
                        reader.GetInt32(2),
                        BitmapConverter.GetBitmapFromBase64(reader.GetString(3)),
                        (int)reader.GetInt64(4),
                        reader.GetString(5),
                        reader.GetInt32(6)
                    ),
                    reader.GetString(0),
                    Convert.ToInt32(reader.GetValue(7).ToString()),
                    reader.GetInt32(8),
                    reader.GetString(9)
                );
                generateRoom(newRoom, idx++);
                rooms.Add(newRoom);
            }
            App.connection.Close();
        }
        void generateRoom(ActRoomKost room, int idx){
            // Dynamic Component
            Grid root = new Grid{
                Height = 200,
                Margin = new Thickness(10)
            };
            root.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            root.ColumnDefinitions.Add(new ColumnDefinition());
            Border child1 = new Border
            {
                Width = 120,
                BorderThickness = new Thickness(2, 2, 0, 2),
                BorderBrush = BrushGen.FromString("#000"),
                Background = new ImageBrush
                {
                    ImageSource = room.Image,
                    Stretch = Stretch.UniformToFill,
                    AlignmentX = AlignmentX.Center,
                    AlignmentY = AlignmentY.Center
                }
            };
            child1.SetValue(Grid.ColumnProperty, 0);
            root.Children.Add(child1);
            Border child2 = new Border
            {
                Background = BrushGen.FromString("#eef"),
                BorderThickness = new Thickness(2),
                BorderBrush = BrushGen.FromString("#000")
            };
            child2.SetValue(Grid.ColumnProperty, 1);
            root.Children.Add(child2);
            StackPanel stack = new StackPanel
            {
                Margin = new Thickness(10)
            };
            child2.Child = stack;
            TextBlock tbRoom = new TextBlock
            {
                Text = $"Room {room.Name} (Floor {room.Lantai})",
                FontSize = 18
            };
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            StackPanel sp1 = new StackPanel();
            StackPanel sp2 = new StackPanel();
            sp1.SetValue(Grid.ColumnProperty, 0);
            sp2.SetValue(Grid.ColumnProperty, 1);
            grid.Children.Add(sp1);
            grid.Children.Add(sp2);
            string[] listText = {
                "Current Price :", CurrencyConverter.ToCurrency(room.Harga),
                "Gender :", new[]{"Man","Woman","Both"}[room.Gender],
                "Next Month Price :", CurrencyConverter.ToCurrency(room.Harga),
                "Status :", room.DispStatus
            };
            for(int i = 0; i < listText.Length; i++){
                new[] { sp1, sp2 }[i / 4].Children.Add(new TextBlock{
                    Text = listText[i],
                    FontSize = new[]{14,12}[i%2]
                });
            }
            TextBlock tbDesc1 = new TextBlock{
                Text = "Description :",
                FontSize = 14
            };
            TextBlock tbDesc2 = new TextBlock{
                Text = room.Description,
                FontSize = 10,
                TextWrapping = TextWrapping.Wrap
            };
            stack.Children.Add(tbRoom);
            stack.Children.Add(grid);
            stack.Children.Add(tbDesc1);
            stack.Children.Add(tbDesc2);

            // Add to Grid.StackPanel
            (gridRoom.Children[idx % 2] as StackPanel).Children.Add(root);
        }

        void loadGenerateOccupant() {
            string sql = @"
                select
                    a.account_image,
	                a.account_nama,
	                r2.room_nama||' (Floor '||r2.room_lantai||')',
	                r1.rent_verified_at,
	                rv.rating,
                    r1.rent_id
                from rent r1
                inner join (
	                select rent_id, nvl(a.col2,-1) as rating
	                from rent
	                left outer join (
		                select rent_id as col1, substr(avg(review_rating),'0.0') as col2
		                from review group by rent_id	
	                )a on rent_id = a.col1
                )rv on r1.rent_id = rv.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                where r2.kost_id = :id and r1.rent_status = 1
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            occupants = new List<Occupant>();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                Occupant occupant = new Occupant(
                    BitmapConverter.GetBitmapFromBase64(reader.GetString(0)),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetDateTime(3),
                    float.Parse(reader.GetValue(4).ToString()),
                    reader.GetString(5)
                );
                occupants.Add(occupant);
            }
            App.connection.Close();
            generateOccupant();
        }
        void generateOccupant(){
            // Clear Children
            foreach (StackPanel child in gridOccupant.Children)
                child.Children.Clear();

            // Dynamic Component
            for (int i = 0; i < occupants.Count; i++) {
                Occupant occupant = occupants[i];
                // Root
                Card root = new Card {
                    Height = 200
                };
                root.SetValue(ShadowAssist.ShadowDepthProperty, ShadowDepth.Depth4);
                // Root > Grid
                Grid child = new Grid { };
                child.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(75) });
                child.ColumnDefinitions.Add(new ColumnDefinition { });
                root.Content = child;
                // Child 1
                Border child1 = new Border {
                    Child = new Image {
                        Source = occupant.Image,
                        Stretch = Stretch.UniformToFill,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    }
                };
                child1.SetValue(Grid.ColumnProperty, 0);
                child.Children.Add(child1);
                // Child 2
                StackPanel child2 = new StackPanel {
                    Margin = new Thickness(10)
                };
                child2.SetValue(Grid.ColumnProperty, 1);
                child.Children.Add(child2);
                // Child 3 (Button Kick)
                Button btnKick = new Button {
                    Content = "Kick",
                    VerticalAlignment = VerticalAlignment.Bottom,
                    Background = BrushGen.FromString("#f66"),
                    BorderBrush = BrushGen.FromString("#f00"),
                    Margin = new Thickness(10)
                };
                btnKick.SetValue(Grid.ColumnProperty, 1);
                child.Children.Add(btnKick);
                btnKick.Click += (object sender, RoutedEventArgs e) => {
                    var result = new SweetAlert() {
                        Title = "Confirmation",
                        Message = $"Are you sure you want to kick {occupant.Name}?",
                        MsgImage = SweetAlertImage.QUESTION,
                        MsgButton = SweetAlertButton.YesNo,
                        OkText = "Yes",
                        CancelText = "No"
                    }.ShowDialog();
                    if (result == SweetAlertResult.YES) {
                        // Kick
                        string sql = @"update rent set rent_status=-2 where rent_id=:id";
                        OracleCommand cmd = new OracleCommand(sql, App.connection);
                        cmd.Parameters.Add(":id", occupant.RentId);
                        App.openConnection();
                        cmd.ExecuteNonQuery();
                        App.closeConnection();
                        // Success
                        SweetAlert.Show("Success", $"{occupant.Name} successfully kicked!", msgImage: SweetAlertImage.SUCCESS);
                        // Reload
                        loadGenerateOccupant();
                        loadGenerateBlacklist1();
                        loadGenerateRoom();
                        loadGenerateUtang();
                    } else {
                        SweetAlert.Show("Cancelled", $"{occupant.Name} is safe", msgImage: SweetAlertImage.ERROR);
                    }
                };
                // StackPanel child
                foreach (var obj in new[] {
                    "Occupant :",occupant.Name,
                    "Room :",occupant.Room,
                    "Since :",occupant.Date.ToString("dd MMM yyyy"),
                    "Satisfaction :",occupant.TextRating,
                }.Select((text,idx) => new {idx,text})) {
                    TextBlock tb = new TextBlock {
                        Text = obj.text,
                        TextTrimming = TextTrimming.WordEllipsis,
                        FontWeight = obj.idx % 2 == 0 ? FontWeights.Normal : FontWeights.Thin
                    };
                    child2.Children.Add(tb);
                }
                // Add To Main StackPanel
                StackPanel spRoot = gridOccupant.Children[i % 3] as StackPanel;
                spRoot.Children.Add(root);
                spRoot.Children.Add(new Rectangle { Margin = new Thickness(0, 10, 0, 10) });
            }
        }

        void loadGenerateRequest(){
            // Clear Child
            stackRent.Children.Clear();

            string sql = $@"
                SELECT 
                    r.rent_id, r.room_id, r.rent_tanggal, 
                    a.account_nama, ro.room_nama, ro.room_lantai
                FROM rent r
                INNER JOIN mahasiswa m ON r.mahasiswa_id = m.mahasiswa_id
                INNER JOIN account a ON m.account_id = a.account_id
                INNER JOIN room ro ON r.room_id = ro.room_id
                WHERE ro.kost_id = :id and r.rent_status = 0
                order by ro.room_id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id",kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            UIElement root = null;
            string currId = "";
            while (reader.Read()){
                // Rent
                string rentId = reader.GetString(0);
                string roomId = reader.GetString(1);
                DateTime rentTgl = reader.GetDateTime(2);
                // Mhs
                string mName = reader.GetString(3);
                // Room
                string rName = reader.GetString(4);
                int rLantai = reader.GetInt32(5);

                // Dynamic Component
                bool createRoot = currId != roomId;
                if(createRoot) {
                    currId = roomId;
                    Expander expander = new Expander {
                        Foreground = BrushGen.FromString("#eee"),
                        Margin = new Thickness(0, 0, 0, 10)
                    };
                    // Header
                    expander.Header = new TextBlock {
                        Text = $"Room {rName} (Floor {rLantai})",
                        FontSize = 20
                    };
                    // Linear Gradient Brush
                    LinearGradientBrush brush = new LinearGradientBrush {
                        StartPoint = new Point(0, 0),
                        EndPoint   = new Point(1, 1)
                    };
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(System.Drawing.Color.MediumVioletRed),
                        Offset = 0f
                    });
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(System.Drawing.Color.SlateBlue),
                        Offset = 0.5f
                    });
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(System.Drawing.Color.RoyalBlue),
                        Offset = 1f
                    });
                    // Background
                    expander.Background = brush;
                    // Child
                    expander.Content = new Border {
                        Background = BrushGen.FromString("#66FFFFFF"),
                        Child = new StackPanel {
                            Margin = new Thickness(10)
                        }
                    };

                    // Add to StackPanel
                    stackRent.Children.Add(root = expander);
                }
                // Parent
                Expander parent = root as Expander;

                // Expander > Border > Stackpanel
                StackPanel rootStack = (parent.Content as Border).Child as StackPanel;

                // Root Child
                Grid grid = new Grid{
                    Margin = new Thickness(0,0,0,10),
                    Background = BrushGen.FromString("#33FFFFFF")
                };
                grid.ColumnDefinitions.Add(new ColumnDefinition{ });
                grid.ColumnDefinitions.Add(new ColumnDefinition{Width = GridLength.Auto});
                rootStack.Children.Add(grid);
                // Child 1
                TextBlock tb = new TextBlock {
                    FontSize = 18,
                    VerticalAlignment = VerticalAlignment.Center,
                    Text = $"{rentTgl.ToString("dd-MMM-yyyy h:mm tt")} {mName}"
                };
                tb.SetValue(Grid.ColumnProperty, 0);
                grid.Children.Add(tb);
                // Child 2
                Grid subGrid = new Grid { };
                subGrid.ColumnDefinitions.Add(new ColumnDefinition { });
                subGrid.ColumnDefinitions.Add(new ColumnDefinition { });
                subGrid.SetValue(Grid.ColumnProperty, 1);
                grid.Children.Add(subGrid);
                // Button Accept & Reject
                Button btnAccept = new Button {
                    Margin = new Thickness(0,0,5,0),
                    Content = "Accept",
                    Background = BrushGen.FromString("#6c6"),
                    BorderBrush = BrushGen.FromDrawingColor(System.Drawing.Color.Green)
                };
                btnAccept.Click += (object sender, RoutedEventArgs e) => {
                    if(responseRent(rentId, 1)) {
                        SweetAlert.Show("Success", "Request succesfully accepted", msgImage: SweetAlertImage.SUCCESS);
                        parent.Visibility = Visibility.Collapsed;
                    }
                };
                Button btnReject = new Button {
                    Content = "Reject",
                    Background = BrushGen.FromString("#f66"),
                    BorderBrush = BrushGen.FromDrawingColor(System.Drawing.Color.Red)
                };
                btnReject.Click += (object sender, RoutedEventArgs e) => {
                    if(responseRent(rentId, -1)) {
                        rootStack.Children.Remove(grid);
                        SweetAlert.Show("Success", "Request successfully rejected", msgImage: SweetAlertImage.SUCCESS);
                        if (rootStack.Children.Count == 0)
                            parent.Visibility = Visibility.Collapsed;
                    }
                };
                btnAccept.SetValue(Grid.ColumnProperty, 0);
                btnReject.SetValue(Grid.ColumnProperty, 1);
                subGrid.Children.Add(btnAccept);
                subGrid.Children.Add(btnReject);
                
            }
            App.connection.Close();
        }
        bool responseRent(string rid, int resp){
            var result = new SweetAlert() {
                Title = "Confirmation",
                Message = $"Are you sure you want to {(resp==1?"Accept":"Reject")} this request?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if (result != SweetAlertResult.YES) return false;

            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand()
            {
                Connection = App.connection,
                CommandText = "rentResponse",
                CommandType = CommandType.StoredProcedure
            };
            // Buat Return
            cmd.Parameters.Add(new OracleParameter(){
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            // Parameter
            cmd.Parameters.Add(new OracleParameter{
                Direction = ParameterDirection.Input,
                ParameterName = "rid",
                OracleDbType = OracleDbType.Varchar2,
                Value = rid
            });
            cmd.Parameters.Add(new OracleParameter{
                Direction = ParameterDirection.Input,
                ParameterName = "resp",
                OracleDbType = OracleDbType.Int32,
                Value = resp
            });
            App.connection.Open();
            cmd.ExecuteNonQuery();
            App.connection.Close();

            // Reload
            if(resp == 1) {
                // Refresh Saldo
                (App.Actmenu as ContainerOwnerKost).loadAccount();
                // Reload Dynamic Component
                loadGenerateOccupant();
                loadGenerateRoom();
                loadGenerateMutation();
                loadProfit();
            }
            return true;
        }
        
        void loadGenerateNews() {
            // Clear Child
            stackNews.Children.Clear();

            string sql = $@"
                select 
	                to_char(n.news_tanggal,'DD-MON-YYYY') || ' | ' ||
	                n.news_judul, 
	                (case when r.rent_id is null then 'All' else 
		                a.account_nama || ' (Room ' || ro.room_nama || ')' end),
	                n.news_description,
                    to_number(case when r.rent_id is null then 0 else 1 end)
                from news n
                left outer join rent r on n.rent_id = r.rent_id
                left outer join room ro on r.room_id = ro.room_id
                left outer join mahasiswa m on r.mahasiswa_id = m.mahasiswa_id
                left outer join account a on m.account_id = a.account_id
                where n.kost_id = :id
                order by n.news_tanggal desc
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                string
                    header = reader.GetString(0), // News Header
                    rec = reader.GetString(1),    // To Who
                    msg = reader.GetString(2);    // News Description
                // 0 All, 1 Tertuju
                int status = Convert.ToInt32(reader.GetValue(3).ToString()); 
                // Dynamic Component
                Expander root = new Expander {
                    Foreground = BrushGen.FromString("#fff"),
                    Header = new TextBlock {
                        Text = header,
                        FontSize = 20,
                        Foreground = BrushGen.FromString("#fff")
                    },
                    Margin = new Thickness(0,0,0,10)
                };
                // Background
                LinearGradientBrush grad = new LinearGradientBrush {
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1),
                };
                string[,] gradCol = {
                    {"#1488CC","#ff00cc"},
                    {"#4364F7","#911CB1"},
                    {"#0052D4","#333399"},
                };
                grad.GradientStops.Add(new GradientStop{
                    Offset = 0.0f,
                    Color = ColorGen.FromString(gradCol[0,status])
                });
                grad.GradientStops.Add(new GradientStop {
                    Offset = 0.5f,
                    Color = ColorGen.FromString(gradCol[1,status])
                });
                grad.GradientStops.Add(new GradientStop {
                    Offset = 1.0f,
                    Color = ColorGen.FromString(gradCol[2,status])
                });
                root.Background = grad;
                // Content
                Border border = new Border {
                    Background = BrushGen.FromString("#33000000")
                };
                root.Content = border;
                // Expand > Border > Grid
                Grid grid = new Grid {
                    Margin = new Thickness(10)
                };
                grid.RowDefinitions.Add(new RowDefinition{Height=GridLength.Auto});
                grid.RowDefinitions.Add(new RowDefinition{});
                grid.ColumnDefinitions.Add(new ColumnDefinition {Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition {Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition {});
                border.Child = grid;
                // Child
                string[] texts = {
                    "To"     ,":",rec,
                    "Message",":",msg
                };
                for (int i = 0; i < texts.Length; i++) {
                    TextBlock textBlock = new TextBlock {
                        Text = texts[i],
                        FontSize = 18,
                        Margin = i%3==1? new Thickness(5,0,5,0) : new Thickness(0),
                        FontWeight = i%3==2? FontWeights.Thin : FontWeights.Normal,
                        TextWrapping = TextWrapping.Wrap
                    };
                    textBlock.SetValue(Grid.RowProperty,i/3);
                    textBlock.SetValue(Grid.ColumnProperty,i%3);
                    grid.Children.Add(textBlock);
                }

                // Add to StackPanel
                stackNews.Children.Add(root);
            }
            App.connection.Close();
        }

        void loadGenerateReview() {
            string sql = @"
                select 
	                a.account_image,
	                a.account_nama,
	                rv.review_rating,
	                rv.review_description,
	                SYSDATE
                from review rv
                inner join rent r1 on rv.rent_id = r1.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                where r2.kost_id = :id  
                order by rv.review_id desc
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            reviews = new List<Review>();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                Review review = new Review(
                    BitmapConverter.GetBitmapFromBase64(reader.GetString(0)),
                    reader.GetString(1),
                    reader.GetInt32(2),
                    reader.GetString(3),
                    reader.GetDateTime(4)
                );
                reviews.Add(review);
            }
            App.connection.Close();
            // Update Jumlah Review
            for (int i = 0; i < wpRating.Children.Count; i++) {
                Button btn = wpRating.Children[i] as Button;
                int star = i == 0 ? -1 : 6 - i;
                int n = reviews.Count(rv => star == -1 || rv.Rating == star);
                btn.Content = i == 0 ? $"All ({n})" : $"{star}-Star ({n})";
            }
            // Generate Review
            generateReview();
            IdxReview = 0;
        }
        void generateReview() {
            // Clear Child
            stackReview.Children.Clear();
            foreach (Review review in reviews) {
                // Root
                Card root = new Card {
                    Padding = new Thickness(10),
                    Margin = new Thickness(5)
                };
                // Grid
                Grid grid = new Grid { };
                root.Content = grid;
                grid.ColumnDefinitions.Add(new ColumnDefinition {Width=GridLength.Auto});
                grid.ColumnDefinitions.Add(new ColumnDefinition { });
                // Image Ellipse
                Ellipse child1 = new Ellipse {
                    Height = 50,
                    Width = 50,
                    VerticalAlignment = VerticalAlignment.Top,
                    Fill = new ImageBrush {
                        ImageSource = review.Image,
                        AlignmentX = AlignmentX.Center,
                        AlignmentY = AlignmentY.Center,
                        Stretch = Stretch.UniformToFill
                    }
                };
                child1.SetValue(Grid.ColumnProperty, 0);
                grid.Children.Add(child1);
                // StackPanel
                StackPanel child2 = new StackPanel {
                    Margin = new Thickness(10, 0, 0, 0)
                };
                child2.SetValue(Grid.ColumnProperty, 1);
                grid.Children.Add(child2);
                // Stackpanel > Child
                child2.Children.Add(new TextBlock {
                    Text = review.Name,
                    FontSize = 18
                });
                child2.Children.Add(new RatingBar {
                    Value = review.Rating,
                    Foreground = BrushGen.FromString("#FFA500"),
                    IsReadOnly = true
                });
                child2.Children.Add(new TextBlock {
                    Text = review.Comment,
                    FontSize = 18,
                    TextWrapping = TextWrapping.Wrap
                });
                // Tanggal
                //child2.Children.Add(new TextBlock {
                //    Text = review.Date.ToString("dd MMM yyyy HH:mm"),
                //    FontSize = 14,
                //    Foreground = BrushGen.FromString("#808080")
                //});
                // Add to StackPanel
                stackReview.Children.Add(root);
            }
        }

        void loadGenerateMutation() {
            #region SQL load mutation
            string sql = @"
                select *
                from
                ( 
	                select 
		                rent_verified_at as col1,
		                'Down Payment '||a.account_nama||' Room '||r2.room_nama as col2, 
		                rent_nominal as col3,
		                ' ' as col4,
		                r2.kost_id as col5
	                from rent r1
	                inner join room r2 on r1.room_id = r2.room_id
	                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
	                inner join account a on m.account_id = a.account_id 
	                where rent_status in (-2,1,2)	
	                union(
		                select 
			                h.htagih_tanggal,
			                'Monthly Bill ' ||a.account_nama|| ' Room '||r2.room_nama,
		 	                d.dtagih_subtotal,
		 	                r1.room_id,
		 	                h.kost_id
		                from dtagih d
		                inner join htagih h on d.htagih_id = h.htagih_id
		                inner join rent r1 on d.rent_id = r1.rent_id
                        inner join room r2 on r1.room_id = r2.room_id
		                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
		                inner join account a on m.account_id = a.account_id 
		                union
		                select 
			                h.htagih_tanggal,
			                'Administrative Costs Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
		 	                -floor(d.dtagih_subtotal/10),
		 	                r1.room_id,
		 	                h.kost_id
		                from dtagih d
		                inner join htagih h on d.htagih_id = h.htagih_id
		                inner join rent r1 on d.rent_id = r1.rent_id
                        inner join room r2 on r1.room_id = r2.room_id
		                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
		                inner join account a on m.account_id = a.account_id 
	                )union(
		                select 
			                u.utang_updated_at,
			                'Late Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
			                u.utang_nominal+u.utang_denda,
			                r1.room_id,
			                r2.kost_id
		                from utang u
		                inner join rent r1 on u.rent_id = r1.rent_id
		                inner join room r2 on r1.room_id = r2.room_id
		                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
		                inner join account a on m.account_id = a.account_id
		                where u.utang_status = 1 
		                union
		                select 
			                u.utang_updated_at,
			                'Administrative Costs Late Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
			                -floor((u.utang_nominal+u.utang_denda)/10),
			                r1.room_id,
			                r2.kost_id
		                from utang u
		                inner join rent r1 on u.rent_id = r1.rent_id
		                inner join room r2 on r1.room_id = r2.room_id
		                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
		                inner join account a on m.account_id = a.account_id 
		                where u.utang_status = 1
	                )
                )
                where col5 = :id
                order by 1 desc,4 desc, 3 desc
            ";

            #endregion
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            mutations = new List<Mutation>();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                Mutation mutation = new Mutation(
                    reader.GetString(4),
                    reader.GetString(1),
                    Convert.ToInt32(reader.GetValue(2).ToString()),
                    reader.GetDateTime(0)
                );
                mutations.Add(mutation);
            }
            App.connection.Close();
            generateMutation();
        }
        void generateMutation() {
            // Clear CHild
            stackMutation.Children.Clear();

            // Dynamic Component
            DateTime date = new DateTime(1,1,1); // Temp Date
            Card root = null;
            TextBlock tbHeader = null;
            int acc = 0;
            foreach (Mutation mutation in mutations) {
                DateTime temp = mutation.Date;
                DateTime date1 = new DateTime(date.Year, date.Month, date.Day);
                DateTime date2 = new DateTime(temp.Year, temp.Month, temp.Day);
                if(date1 != date2) {
                    // Generate Root
                    date = date2;
                    acc = 0;
                    Card cardRoot = new Card {
                        Margin = new Thickness(5)
                    };
                    // Card > Expander
                    Expander expander = new Expander {};
                    cardRoot.Content = expander;
                    // Background
                    LinearGradientBrush brush = new LinearGradientBrush {
                        StartPoint = new Point(0,0),
                        EndPoint = new Point(1,1),
                    };
                    brush.GradientStops.Add(new GradientStop {
                        Offset = 0.0f,
                        Color = ColorGen.FromString("#C9D6FF")
                    });
                    brush.GradientStops.Add(new GradientStop {
                        Offset = 1.0f,
                        Color = ColorGen.FromString("#6DD5FA")
                    });
                    // Header
                    Grid header = new Grid {};
                    header.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(550)});
                    header.ColumnDefinitions.Add(new ColumnDefinition {});
                    TextBlock tbTanggal = new TextBlock {
                        FontSize = 18,
                        Text = date.ToString("dd MMM yyyy"),
                        TextTrimming = TextTrimming.WordEllipsis
                    };
                    tbTanggal.SetValue(Grid.ColumnProperty, 0);
                    TextBlock tbMoney = new TextBlock {
                        FontSize = 18,
                        Text = "+ " + CurrencyConverter.ToCurrency(0),
                        HorizontalAlignment = HorizontalAlignment.Right
                    };
                    tbMoney.SetValue(Grid.ColumnProperty, 1);
                    header.Children.Add(tbTanggal);
                    header.Children.Add(tbHeader = tbMoney);
                    // Content
                    Border child = new Border {
                        Background = BrushGen.FromString("#22000000"),
                        Child = new StackPanel {
                            Margin = new Thickness(10)
                        }
                    };
                    // Assign to Expander
                    expander.Background = brush;
                    expander.Header = header;
                    expander.Content = child;
                    // Add to stackpanel
                    stackMutation.Children.Add(root = cardRoot);
                }
                // Untuk update total Money
                TextBlock tbParent = tbHeader;
                // Parent: Card > Expander > Border > Stackpanel
                StackPanel spRoot = ((root.Content as Expander).Content as Border).Child as StackPanel;
                // Child (Card)
                Card newCard = new Card {
                    Padding = new Thickness(10),
                    Margin = new Thickness(5)
                };
                // Bool is Minus
                bool isPos = mutation.Nominal > 0;
                // Background
                LinearGradientBrush linear = new LinearGradientBrush {
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1)
                };
                linear.GradientStops.Add(new GradientStop {
                    Offset = 0.0f,
                    Color = ColorGen.FromString(isPos ? "#a8ff78" : "#EECDA3")
                });
                linear.GradientStops.Add(new GradientStop {
                    Offset = 1.0f,
                    Color = ColorGen.FromString(isPos ? "#78ffd6" : "#EF629F")
                });
                newCard.Background = linear;
                // Grid
                Grid grid = new Grid {};
                grid.Children.Add(new TextBlock {
                    FontSize = 14,
                    Text = mutation.Detail
                });
                grid.Children.Add(new TextBlock {
                    FontSize = 14,
                    Text = (isPos?"+ ":"- ") + CurrencyConverter.ToCurrency(Math.Abs(mutation.Nominal)),
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Foreground = BrushGen.FromString(isPos? "#088C00" : "#880202")
                });
                newCard.Content = grid;
                // Update acc
                acc += mutation.Nominal;
                tbHeader.Text = "+ " + CurrencyConverter.ToCurrency(acc);

                // Add to Stackpanel
                spRoot.Children.Add(newCard);
            }
        }

        void loadGenerateUtang() {
            string sql = @"
                select 
	                'Room '||r2.room_nama||' (Floor '||r2.room_lantai||')', 
	                a.account_image,
	                a.account_nama||' ('||a.account_username||')', 
	                h.htagih_tanggal,
                    h.htagih_tanggal + k.kost_batas_hari_telat,
	                u.utang_nominal, u.utang_denda
                from utang u
                inner join htagih h on u.htagih_id = h.htagih_id
                inner join kost k on h.kost_id = k.kost_id
                inner join rent r1 on u.rent_id = r1.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                where h.kost_id = :id and r1.rent_status = 1 and
	                u.utang_status = 0
                order by 1 desc
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            utangs = new List<Utang>();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                Utang newUtang = new Utang(
                    reader.GetString(0),
                    BitmapConverter.GetBitmapFromBase64(reader.GetString(1)),
                    reader.GetString(2),
                    reader.GetDateTime(3),
                    reader.GetDateTime(4),
                    Convert.ToInt32(reader.GetValue(5).ToString()),
                    Convert.ToInt32(reader.GetValue(6).ToString())
                );
                utangs.Add(newUtang);
            }
            App.connection.Close();
            generateUtang();
        }
        void generateUtang() {
            // Clear Child
            stackUtang.Children.Clear();
            // Dynamic Component
            foreach (Utang utang in utangs) {
                // Parent
                Expander parent = new Expander {
                    Foreground = BrushGen.FromString("#fff"),
                    Margin = new Thickness(0,0,0,10)
                };
                // Header
                Grid header = new Grid { };
                header.Children.Add(new TextBlock {
                    Text = utang.Header,
                    FontSize = 18
                });
                header.Children.Add(new TextBlock {
                    Text = utang.HeaderNominal,
                    FontSize = 18,
                    HorizontalAlignment = HorizontalAlignment.Right
                });
                parent.Header = header;
                // Background
                LinearGradientBrush linear = new LinearGradientBrush {
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1),
                };
                linear.GradientStops.Add(new GradientStop {
                    Offset = 0.0f,
                    Color = ColorGen.FromString("#c31432")
                });
                linear.GradientStops.Add(new GradientStop {
                    Offset = 1.0f,
                    Color = ColorGen.FromString("#240b36")
                });
                parent.Background = linear;
                // Content
                Grid root = new Grid {
                    Background = BrushGen.FromString("#66000000")
                };
                Grid child = new Grid {
                    Margin = new Thickness(10),
                };
                root.Children.Add(child);
                child.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(150) });
                child.ColumnDefinitions.Add(new ColumnDefinition { });
                // Child
                Border child1 = new Border {
                    Background = BrushGen.FromString("#66000000"),
                    Child = new Image {
                        Source = utang.Image,
                        Stretch = Stretch.UniformToFill,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    },
                    Height = 150,
                };
                child.Children.Add(child1);
                child1.SetValue(Grid.ColumnProperty, 0);
                StackPanel child2 = new StackPanel {
                    Margin = new Thickness(10)
                };
                child2.SetValue(Grid.ColumnProperty, 1);
                foreach (var text in new[] {
                    "Occupant :"  , utang.To,
                    "Since :", utang.Date.ToString("dd MMM yyyy HH:mm"),
                    "Deadline :", utang.Deadline.ToString("dd MMM yyyy HH:mm")
                }.Select((item,idx)=>new{item, idx})){
                    TextBlock tb = new TextBlock {
                        Text = text.item,
                        FontSize = 18,
                        FontWeight = new[]{FontWeights.Normal,FontWeights.Thin}[text.idx%2]
                    };
                    child2.Children.Add(tb);
                }
                child.Children.Add(child2);
                // Assign to Expander
                parent.Content = root;
                // Add to StackPanel
                stackUtang.Children.Add(parent);
            }
        }

        /// <summary>
        /// Occupant History
        /// </summary>
        void loadGenerateBlacklist1() {
            string sql = @"
                select
                    m.mahasiswa_id as ID,
                    a.account_image as Image,
                    a.account_username as NRP,
                    a.account_nama as Name,
                    r2.room_nama||' (Floor '||r2.room_lantai||')' as Room,
                    r1.rent_verified_at as ""Date Join"",
                    r1.rent_updated_at as ""Date Left""
                from rent r1
                inner join (
	                select mahasiswa_id, max(rent_verified_at) col2
	                from rent r1
	                inner join room r2 on r1.room_id = r2.room_id
	                where kost_id = :id
	                group by mahasiswa_id
                )re on (r1.mahasiswa_id = re.mahasiswa_id and r1.rent_verified_at = re.col2)
                inner join room r2 on r1.room_id = r2.room_id
                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                where r2.kost_id = :id and r1.rent_status in (-2,2) and
                    m.mahasiswa_id not in (
                        select mahasiswa_id from blacklist
                        where kost_id = :id
                    )
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            dtBlacklist1 = new DataTable();
            da.Fill(dtBlacklist1);
            DataView dv = dtBlacklist1.DefaultView;
            dgRent.ItemsSource = dv;
            // Hide
            // ID, Base 64 Image
            dgRent.Columns[0].Visibility = Visibility.Collapsed;
            dgRent.Columns[1].Visibility = Visibility.Collapsed;
            // Trigger
            KeyBlk1 = KeyBlk1;
        }
        /// <summary>
        /// List Blacklisted Occupant
        /// </summary>
        void loadGenerateBlacklist2() {
            string sql = @"
                select
                    m.mahasiswa_id as ID,
                    a.account_image as Image,
                    a.account_username as NRP,
                    a.account_nama as Name,
                    r2.room_nama||' (Floor '||r2.room_lantai||')' as Room,
                    r1.rent_verified_at as ""Date Join"",
                    r1.rent_updated_at as ""Date Left""
                from blacklist b
                inner join mahasiswa m on b.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                inner join rent r1 on m.mahasiswa_id = r1.mahasiswa_id
                inner join room r2 on r1.room_id = r2.room_id
                inner join (
	                select mahasiswa_id, max(rent_verified_at) col2
	                from rent r1
	                inner join room r2 on r1.room_id = r2.room_id
	                where kost_id = :id
	                group by mahasiswa_id
                )re on (r1.mahasiswa_id = re.mahasiswa_id and r1.rent_verified_at = re.col2)
                where b.kost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            dtBlacklist2 = new DataTable();
            da.Fill(dtBlacklist2);
            DataView dv = dtBlacklist2.DefaultView;
            dgBlacklist.ItemsSource = dv;
            // Hide
            // ID, Base 64 Image
            dgBlacklist.Columns[0].Visibility = Visibility.Collapsed;
            dgBlacklist.Columns[1].Visibility = Visibility.Collapsed;
            // Trigger
            KeyBlk2 = KeyBlk2;
        }
        // Load Status
        void loadProfit() {
            string sql = @"
                select
                (
	                select nvl(sum(htagih_total),0)
	                from htagih
	                where kost_id = :id
                )+
                (
	                select nvl(sum(rent_nominal),0)
	                from rent r1
	                inner join room r2 on r1.room_id = r2.room_id
	                where r2.kost_id = :id and
		                r1.rent_status IN (-2,1,2)
                )from dual
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.openConnection();
            Profit = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            App.closeConnection();
        }
        void loadStarRate() {
            string sql = @"
                select to_char(nvl(avg(review_rating),-1),'999.9')
                from review rv
                inner join rent r1 on rv.rent_id = r1.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                where r2.kost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            StarRate = float.Parse(cmd.ExecuteScalar().ToString());
            App.connection.Close();
        }
        #endregion

        #region Navigate to Different Page

        private void BtnKost_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostUpdateKost(kost));
        }

        private void BtnFaci_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostUpdateFacility(kost));
        }

        private void BtnRoom_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostUpdateRoom(kost));
        }

        private void BtnNews_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostCreateNews(kost));
        }

        private void BtnBackMyKost_Click(object sender, RoutedEventArgs e) {
            App.Actmenu.navigate(new OwnerKostMyKost());
        }

        private void BtnReload_Click(object sender, RoutedEventArgs e) {
            // Reload Kost
            string sql = @"
                select * from kost 
                where kost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            reader.Read();
            Kost newKost = new Kost(
                App.User as OwnerKost,
                reader.GetString(2),
                reader.GetString(3),
                BitmapConverter.GetBitmapFromBase64(reader.GetString(4)),
                reader.GetString(5),
                Convert.ToInt32(reader.GetValue(7).ToString()),
                Convert.ToInt32(reader.GetValue(8).ToString())
            );
            ActKost actKost = new ActKost(newKost,
                reader.GetString(0),
                reader.GetDateTime(6),
                Convert.ToInt32(reader.GetValue(9).ToString())
            );
            // Load Star
            sql = @"
                select to_char(nvl(avg(review_rating),-1),'999.9')
                from review rv
                inner join rent r1 on rv.rent_id = r1.rent_id
                inner join room r2 on r1.room_id = r2.room_id
                where r2.kost_id = :id
            ";
            cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", kost.Id);
            actKost.Rating = float.Parse(cmd.ExecuteScalar().ToString());
            App.connection.Close();
            // Navigate ke Diri Sendiri
            App.Actmenu.navigate(new OwnerKostDetailKost(actKost));
        }
        #endregion

        #region BlackList Event
        public string IdBlkMhs1 = "";
        public string IdBlkMhs2 = "";
        private void DgRent_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            int idx = dgRent.SelectedIndex;
            if (idx == -1) return;
            DataRow row = dtBlacklist1.Rows[idx];
            IdBlkMhs1 = row["Id"].ToString();
            imageBlk1.ImageSource = BitmapConverter.GetBitmapFromBase64(row["Image"].ToString());
            textNamaBlk1.Text = row["Name"].ToString();
            textNrpBlk1.Text = row["NRP"].ToString();
            textDateBlk1.Text = 
                ((DateTime)row["Date Join"]).ToString("dd/MM/yyyy")+" - "+
                ((DateTime)row["Date Left"]).ToString("dd/MM/yyyy");
            textRoomBlk1.Text = row["Room"].ToString();
            // Trigger
            FlipBlack1 = true;
            dgRent.SelectedIndex = -1;
        }

        private void TextBlk1_TextChanged(object sender, TextChangedEventArgs e) {
            string text = textBlk1.Text;
            KeyBlk1 = text;
        }

        private void BtnCloseBlk1_Click(object sender, RoutedEventArgs e) => FlipBlack1 = false;

        private void BtnBan_Click(object sender, RoutedEventArgs e) {
            if (!FlipBlack1) {
                SweetAlert.Show("Whoops", "You forgot to Select user", msgImage: SweetAlertImage.WARNING);
                return;
            }
            string mhs_name = textNamaBlk1.Text;
            var result = new SweetAlert() {
                Title = "Confirmation",
                Message = $"Are you sure you want to ban {mhs_name}?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if(result == SweetAlertResult.YES) {
                // Field
                string mhs_id = IdBlkMhs1;
                string kost_id = kost.Id;
                string message = "No Reason";
                int status = 1;
                // Query
                string sql = @"INSERT INTO blacklist VALUES(:mhs,:kost,:msg,:status)";
                OracleCommand cmd = new OracleCommand(sql, App.connection);
                cmd.Parameters.Add(":mhs",mhs_id);
                cmd.Parameters.Add(":kost",kost_id);
                cmd.Parameters.Add(":msg",message);
                cmd.Parameters.Add(":status", status);
                App.connection.Open();
                cmd.ExecuteNonQuery();
                App.connection.Close();
                // Show Success + Reload
                SweetAlert.Show("Success", $"{mhs_name} successfully banned!", msgImage: SweetAlertImage.SUCCESS);
                loadGenerateBlacklist1();
                loadGenerateBlacklist2();
                FlipBlack1 = false;
            }
        }
        
        private void DgBlacklist_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            int idx = dgBlacklist.SelectedIndex;
            if (idx == -1) return;
            DataRow row = dtBlacklist2.Rows[idx];
            IdBlkMhs2 = row["Id"].ToString();
            imageBlk2.ImageSource = BitmapConverter.GetBitmapFromBase64(row["Image"].ToString());
            textNamaBlk2.Text = row["Name"].ToString();
            textNrpBlk2.Text = row["NRP"].ToString();
            textDateBlk2.Text =
                ((DateTime)row["Date Join"]).ToString("dd/MM/yyyy") + " - " +
                ((DateTime)row["Date Left"]).ToString("dd/MM/yyyy");
            textRoomBlk2.Text = row["Room"].ToString();
            // Trigger
            FlipBlack2 = true;
            dgBlacklist.SelectedIndex = -1;
        }

        private void TextBlk2_TextChanged(object sender, TextChangedEventArgs e) {
            string text = textBlk2.Text;
            KeyBlk2 = text;
        }

        private void BtnCloseBlk2_Click(object sender, RoutedEventArgs e) => FlipBlack2 = false;

        private void BtnUnBan_Click(object sender, RoutedEventArgs e) {
            if (!FlipBlack2) {
                SweetAlert.Show("Whoops", "You forgot to Select user", msgImage: SweetAlertImage.WARNING);
                return;
            }
            string mhs_name = textNamaBlk2.Text;
            var result = new SweetAlert() {
                Title = "Confirmation",
                Message = $"Are you sure you want to unban {mhs_name}?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if (result == SweetAlertResult.YES) {
                // Field
                string mhs_id = IdBlkMhs2;
                string kost_id = kost.Id;
                // Query
                string sql = @"DELETE FROM blacklist WHERE kost_id = :kost AND mahasiswa_id = :mhs";
                OracleCommand cmd = new OracleCommand(sql, App.connection);
                cmd.Parameters.Add(":kost", kost_id);
                cmd.Parameters.Add(":mhs", mhs_id);
                App.connection.Open();
                cmd.ExecuteNonQuery();
                App.connection.Close();
                // Show Success + Reload
                SweetAlert.Show("Success", $"{mhs_name} successfully unbanned!", msgImage: SweetAlertImage.SUCCESS);
                loadGenerateBlacklist1();
                loadGenerateBlacklist2();
                FlipBlack2 = false;
            }
        }

        #endregion

        private void BtnReport_Click(object sender, RoutedEventArgs e) {
            // Compare Date
            DateTime date1 = dateStart.SelectedDate.Value;
            DateTime date2 = dateEnd.SelectedDate.Value;
            if(date2.CompareTo(date1) <= 0) {
                SweetAlert.Show("Whoops", "[Start Date > End Date] Error",msgImage: SweetAlertImage.ERROR);
                return;
            }
            // Activate Viewer
            cViewer.IsEnabled = true;
            if (rb1.IsChecked==true) {
                ReportSewaRoom report = new ReportSewaRoom();
                report.SetDatabaseLogon("proyekpcs", "proyekpcs");
                report.SetParameterValue("Id Kost", kost.Id);
                report.SetParameterValue("Tanggal Awal Sewa", date1);
                report.SetParameterValue("Tanggal Akhir Sewa", date2);
                cViewer.ViewerCore.ReportSource = report;
            } else {
                // Show Report
                ReportMoneyMutation report = new ReportMoneyMutation();
                report.SetDatabaseLogon("proyekpcs", "proyekpcs");
                // Param
                report.SetParameterValue("id_kost", kost.Id);
                report.SetParameterValue("StartDate", date1);
                report.SetParameterValue("EndDate", date2);
                cViewer.ViewerCore.ReportSource = report;
            }
        }
    }
}
