﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Oracle.DataAccess.Client;
using System.Data;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewAdmin.xaml
    /// </summary>
    public partial class AdminViewAdmin : Page
    {
        DataTable dt;
        public AdminViewAdmin()
        {
            InitializeComponent();
            load();
        }
        public void load()
        {
            OracleDataAdapter da = new OracleDataAdapter("select ad.admin_id as AdminId , acc.account_username as Username , acc.account_nama as Nama from admin ad join account acc on ad.account_id=acc.account_id", App.connection);
            dt = new DataTable();
            da.Fill(dt);
            dgadmin.ItemsSource = dt.DefaultView;
        }
        private void dgadmin_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btcreate_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new AdminAddAdmin());
        }
    }
}
