﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    /// <summary>
    /// Oy ni class buat convert integer jadi currency
    /// </summary>
    public class CurrencyConverter
    {
        private CurrencyConverter() { }

        public static string ToCurrency(int n,bool usingCurr = true){
            string curr = "IDR";
            string money = String.Format("{0:n0}", n).Replace(",",".");
            return usingCurr ? $"{curr} {money}" : money;
        }
    }
}
