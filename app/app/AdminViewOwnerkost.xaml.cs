﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewOwnerkost.xaml
    /// </summary>
    public partial class AdminViewOwnerkost : Page
    {
        DataTable dt;
        public AdminViewOwnerkost()
        {
            InitializeComponent();
            load();
        }
        public void load()
        {
            OracleDataAdapter da = new OracleDataAdapter("select ok.ownerkost_id as ID , acc.account_nama as Nama , acc.account_username as Username , ok.ownerkost_telp as Telepon , ok.ownerkost_email as Email , acc.account_saldo as Balance,(case when acc.account_status=-1 then 'Rejected' when acc.account_status=0 then 'Pending' else 'Accepted' end) as Status from ownerkost ok join account acc on ok.account_id=acc.account_id", App.connection);
            dt = new DataTable();
            da.Fill(dt);
            dgownerkost.ItemsSource = dt.DefaultView;
        }
    }
}
