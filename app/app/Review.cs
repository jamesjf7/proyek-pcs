﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    public class Review{
        private BitmapImage image;
        private string name;
        private int rating;
        private string comment;
        private DateTime date;

        public BitmapImage Image { get => image; set => image = value; }
        public string Name { get => name; set => name = value; }
        public int Rating { get => rating; set => rating = value; }
        public string Comment { get => comment; set => comment = value; }
        public DateTime Date { get => date; set => date = value; }

        public Review(BitmapImage image, string name, int rating, string comment, DateTime date) {
            Image = image;
            Name = name;
            Rating = rating;
            Comment = comment;
            Date = date;
        }
    }
}
