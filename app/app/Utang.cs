﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app{
    public class Utang {
        private string header;
        private BitmapImage image;
        private string to;
        private DateTime date;
        private DateTime deadline;
        private int nominal;
        private int denda;

        public string Header { get => header; set => header = value; }
        public BitmapImage Image { get => image; set => image = value; }
        public string To { get => to; set => to = value; }
        public DateTime Date { get => date; set => date = value; }
        public DateTime Deadline { get => deadline; set => deadline = value; }
        public int Nominal { get => nominal; set => nominal = value; }
        public int Denda { get => denda; set => denda = value; }
        public string HeaderNominal => 
            $"{CurrencyConverter.ToCurrency(nominal)} + {CurrencyConverter.ToCurrency(denda,false)}";

        public Utang(string header, BitmapImage image, string to, DateTime date, DateTime deadline, int nominal, int denda) {
            Header = header;
            Image = image;
            To = to;
            Date = date;
            Deadline = deadline;
            Nominal = nominal;
            Denda = denda;
        }
    }
}
