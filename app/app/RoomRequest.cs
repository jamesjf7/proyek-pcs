﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app {
    public class RoomRequest {
        private string idRequest;
        private string idKost;
        private string header;
        private string detail;

        public string IdRequest { get => idRequest; set => idRequest = value; }
        public string IdKost { get => idKost; set => idKost = value; }
        public string Header { get => header; set => header = value; }
        public string Detail { get => detail; set => detail = value; }

        public RoomRequest(string idRequest, string idKost, string header, string detail) {
            this.idRequest = idRequest;
            this.idKost = idKost;
            this.header = header;
            this.detail = detail;
        }
    }
}
