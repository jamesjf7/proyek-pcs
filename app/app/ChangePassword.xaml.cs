﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Page
    {
        string password,id;

        public ChangePassword()
        {
            InitializeComponent();
            id = App.User.Id;
            var koneksi = new OracleDataAdapter("select account_password   \"password\" from account  where account_id='"+id+"'", App.connection);
            var dtb = new DataTable();
            koneksi.Fill(dtb);

            password = dtb.Rows[0]["password"].ToString();
        }

        private void Prev_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to cancel change password ?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.NO)
            {
                return;
            }

            App.Actmenu.navigate(
                App.User is OwnerKost ? new OwnerKostHome() as Page:
                App.User is Admin ? new AdminHome() as Page : new MhsHome()                
            );
        }

        private void Bsp_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to change your password ?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.NO)
            {
                return;
            }

            if (top.Password == "" || tp.Password == "" || tcp.Password  == "" || tp.Password != tcp.Password)
            {
                SweetAlert.Show("Uh Oh", "Your field is not ready", msgImage: SweetAlertImage.WARNING);
                return;
            }

            if (top.Password != password)
            {
                SweetAlert.Show("Oh No !!!", "Your Current Password  Is Wrong", msgImage: SweetAlertImage.WARNING);
                return;
            }

            string query = "update account set account_password = :pass WHERE account_id =:idaccount";
            OracleCommand cmd = new OracleCommand(query, App.connection);
            cmd.Parameters.Add(":pass", tp.Password);
            cmd.Parameters.Add(":idaccount", id);
            App.connection.Open();
            cmd.ExecuteNonQuery();
            App.connection.Close();

            SweetAlert.Show("Success", "Password succesfully changed!", msgImage: SweetAlertImage.SUCCESS);
            App.Actmenu.navigate(
                App.User is OwnerKost ? new OwnerKostHome() as Page :
                App.User is Admin ? new AdminHome() as Page : new MhsHome()
            );
        }
    }
}
