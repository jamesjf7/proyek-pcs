﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for ContainerOwnerKost.xaml
    /// </summary>
    public partial class ContainerOwnerKost : Page, INavigate
    {
        public ContainerOwnerKost()
        {
            InitializeComponent();
        }

        public void navigate(Page page, object param)
        {
            mainFrame.Content = null;
            mainFrame.NavigationService.Navigate(page, param);
        }

        public void navigate(Page page)
        {
            navigate(page, null);
        }
        
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            navigate(new OwnerKostHome());
            updateDisplay();
        }

        public void loadAccount() {
            string sql = @"
                select * 
                from ownerkost o
                inner join account a on o.account_id = a.account_id
                where o.account_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql,App.connection);
            cmd.Parameters.Add(":id", App.User.Id);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            reader.Read();
            // Account
            App.User.Name  = reader["account_nama"].ToString();
            App.User.Saldo = Convert.ToInt32(reader["account_saldo"].ToString());
            App.User.Image = BitmapConverter.GetBitmapFromBase64(reader["account_image"].ToString());
            // OwnerKost
            (App.User as OwnerKost).Telp  = reader["ownerkost_telp"].ToString();
            (App.User as OwnerKost).Email = reader["ownerkost_email"].ToString();
            App.connection.Close();
            // Update Display
            updateDisplay();
        }

        void updateDisplay() {
            imageOwner.ImageSource = App.User.Image;
            textName.Text = App.User.Name;
            textEmail.Text = (App.User as OwnerKost).Email;
            textMoney.Text = CurrencyConverter.ToCurrency(App.User.Saldo);
        }

        private void ListBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int idx = listBox1.SelectedIndex;
            if (idx == -1) return;
            string menu = ((listBox1.SelectedItem as DockPanel).Children[1] as TextBlock).Text;
            listBox1.SelectedIndex = -1;
            // Navigate
            Page newPage = null;
            if(menu == "Home"){
                newPage = new OwnerKostHome();
            }else if(menu == "Create Kost"){
                newPage = new OwnerKostCreateKost1();
            }else if(menu == "My Kost"){
                newPage = new OwnerKostMyKost();
            }else if(menu == "Room Request"){
                newPage = new OwnerKostRoomRequest();
            }else if(menu == "News"){
                newPage = new OwnerKostNews();
            } else if(menu == "Change Password"){
                newPage = new ChangePassword();        
            }else if (menu == "Logout"){
                var result = new SweetAlert(){
                    Title = "Confirmation",
                    Message = "Are you sure you want to leave?",
                    MsgImage = SweetAlertImage.QUESTION,
                    MsgButton = SweetAlertButton.YesNo,
                    OkText = "Yes",
                    CancelText = "No"
                }.ShowDialog();
                if (result == SweetAlertResult.YES)
                    App.MainWindow.navigateContainer(Container.StartMenu);
                return;
            }
            navigate(newPage);
            drawerHost.IsLeftDrawerOpen = false;
        }
       
        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to Logout?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.YES)
                Application.Current.Shutdown();
        }
    }
}
