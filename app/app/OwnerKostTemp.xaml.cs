﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostTemp.xaml
    /// </summary>
    public partial class OwnerKostTemp : Page
    {
        public OwnerKostTemp()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadFacilitas();
        }

        DataTable dt = new DataTable();
        OracleDataAdapter da;
        void loadFacilitas(){
            string sql = "select * from fasilitas";
            da = new OracleDataAdapter(sql,App.connection);
            OracleCommandBuilder builder = new OracleCommandBuilder(da);
            dt = new DataTable();
            da.Fill(dt);

            dgv1.ItemsSource = dt.DefaultView;
        }

        private void Dgv1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int idx = dgv1.SelectedIndex;
            if (idx == -1) return;
            MessageBox.Show(dt.Rows[idx]["fasilitas_id"].ToString());
        }

        private void BtnCount_Click(object sender, RoutedEventArgs e)
        {
            SweetAlert.Show(dt.Rows.Count+"","");
        }

        private void BtnBuild_Click(object sender, RoutedEventArgs e)
        {
            da.Update(dt);
        }
    }
}
