﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace app
{
    /// <summary>
    /// Yow be'be'e pakee
    /// Ini class buat mbuat Color (WPF) dari System.Drawing.Color (WinForm)
    /// </summary>
    // Misal mau pake System.Drawing.Color.RoyalBlue tinggal begini
    // 1. Color myColor = ColorGen.FromDrawingColor(System.Drawing.Color.RoyalBlue);
    // Ada Juga buat Brush... sama ae seh wkwk
    // 1. Brush maBrush = BrushGen.FromDrawingColor(System.Drawing.Color.RoyalBlue);
     
    public class ColorGen
    {
        private ColorGen(){}
        public static Color FromDrawingColor(System.Drawing.Color color){
            return Color.FromRgb(color.R,color.G,color.B);
        }
        public static Color FromString(string str){
            return (Color)ColorConverter.ConvertFromString(str);
        }
    }

    /// <summary>
    /// Hi there :D
    /// </summary>
    public class BrushGen
    {
        private BrushGen(){}
        public static SolidColorBrush FromDrawingColor(System.Drawing.Color color){
            return new SolidColorBrush(ColorGen.FromDrawingColor(color));
        }
        public static SolidColorBrush FromString(string str){
            return new SolidColorBrush(ColorGen.FromString(str));
        }
    }
}
