﻿using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace app
{
    /// <summary>
    /// Interaction logic for UpdateDeleteMhs.xaml
    /// </summary>
    public partial class UpdateDeleteMhs : Page
    {
        BitmapImage img = null;
        OpenFileDialog op;
        DataTable dt,dt2;
        string id,query;
        ImageSource imaccount;
        public UpdateDeleteMhs(string accountid)
        {
            InitializeComponent();
            id = accountid;
            load();
        }
        public void load()
        {
            query = "select JURUSAN_ID,JURUSAN_NAMA from jurusan";
            OracleDataAdapter da = new OracleDataAdapter(query, App.connection);
            dt = new DataTable();
            da.Fill(dt);
            cbjurusan.DisplayMemberPath = "JURUSAN_NAMA";
            cbjurusan.SelectedValuePath = "JURUSAN_ID";
            cbjurusan.ItemsSource = dt.DefaultView;
            string query2 = $"Select acc.account_image,acc.account_nama,mhs.jurusan_id,mhs.tahun,mhs.mahasiswa_gender from account acc join mahasiswa mhs on acc.account_id=mhs.account_id where acc.account_id='{id}'";
            OracleDataAdapter da2 = new OracleDataAdapter(query2, App.connection);
            dt2 = new DataTable();
            da2.Fill(dt2);
            textName.Text = dt2.Rows[0][1].ToString();
            tbtahun.Text = dt2.Rows[0][3].ToString();
            if (dt2.Rows[0][4].ToString() == "0")
            {
                rbmale.IsChecked = true;
            }
            else
            {
                rbfemale.IsChecked = true;
            }
            cbjurusan.SelectedValue = dt2.Rows[0][2].ToString();
            imaccount = BitmapConverter.GetBitmapFromBase64(dt2.Rows[0][0].ToString());
            image.Source= imaccount;
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            query = $"delete from mahasiswa where account_id='{id}'";
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                App.connection.Close();
                SweetAlert.Show("Notification","Delete Success",SweetAlertButton.OK,SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new AdminViewMhs());
            }
            catch (Exception)
            {
                App.connection.Close();
                SweetAlert.Show("Notification", "Delete Failed", SweetAlertButton.OK, SweetAlertImage.WARNING);
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = textName.Text;
            var tempImage = imaccount;
            string title = "Uh Oh";
            string message = "";
            int gender;
            if ((bool)rbmale.IsChecked)
            {
                gender = 0;
            }
            else
            {
                gender = 1;
            }
            bool invalid = true;
            if (tempImage == null)
            {
                message = "Please Add Image first";
            }
            else if (new String[] { name }.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else invalid = false;

            // Give Message
            if (invalid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
                return;
            }
            else
            {
                string query = $"UPDATE mahasiswa set mahasiswa_gender = :gender WHERE Account_id = '{id}'";
                OracleCommand cmd = new OracleCommand(query, App.connection);
                cmd.Parameters.Add(":gender", gender);
                string query2 = $"UPDATE account set Account_nama = :Name,Account_Image = :image WHERE Account_id = '{id}'";
                OracleCommand cmd2 = new OracleCommand(query2, App.connection);
                cmd2.Parameters.Add(":NAME", name);
                cmd2.Parameters.Add(":image", BitmapConverter.GetBase64FromBitmap(imaccount as BitmapImage));
                App.connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    App.connection.Close();
                    SweetAlert.Show("Message","Update Success",SweetAlertButton.OK,SweetAlertImage.SUCCESS);
                    App.Actmenu.navigate(new AdminViewMhs());
                }
                catch (Exception)
                {
                    App.connection.Close();
                    SweetAlert.Show("Message", "Update Failed", SweetAlertButton.OK, SweetAlertImage.WARNING);
                }
            }
        }

        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }
    }
}
