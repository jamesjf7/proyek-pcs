﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
namespace app{
    public partial class MhsDetailKost : Page{
        string kostID;
        string type; // SearchKost, MyKost
        public MhsDetailKost(string type, string id){
            InitializeComponent();
            kostID = id;
            this.type = type;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e){
            // Kost : 
            using (OracleCommand cmd = new OracleCommand($"SELECT * FROM KOST WHERE KOST_ID = '{kostID}'", App.connection))
            {
                App.connection.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    imKostImage.ImageSource = BitmapConverter.GetBitmapFromBase64(reader.GetString(4));
                    textKostNama.Text = reader["kost_name"].ToString();
                    textKostAlamat.Text = reader["kost_alamat"].ToString();
                    textKostDescription.Text = reader["kost_description"].ToString();
                    textKostDendaTelat.Text = $"Denda telat : {CurrencyConverter.ToCurrency(int.Parse(reader["kost_denda_telat"].ToString()))}";
                    textKostBatasHariTelat.Text = $"Batas telat pembayaran : {reader["kost_batas_hari_telat"].ToString()} Hari";

                    string query = $"select nvl(substr(avg(rv.review_rating),1,3),'0.0') from review rv " +
                                   $"inner join rent rn on rn.rent_id = rv.rent_id " +
                                   $"inner join room r on r.room_id = rn.room_id " +
                                   $"inner join kost ko on ko.kost_id = r.kost_id " +
                                   $"where ko.kost_id = '{kostID}'";
                    OracleCommand cmds = new OracleCommand(query,App.connection);        
                    rateBar.Value = ((int)float.Parse(cmds.ExecuteScalar().ToString()));
                    textRating.Text = $"({cmds.ExecuteScalar().ToString()})";
                }
                App.connection.Close();
            }
            // Fasilitas : 
            using (OracleCommand cmd = new OracleCommand($"SELECT * FROM FASILITAS WHERE KOST_ID = '{kostID}'", App.connection))
            {
                App.connection.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                StackPanel spContainer = new StackPanel() { Orientation = Orientation.Horizontal };
                while (reader.Read())
                { 
                    StackPanel sp = new StackPanel() { Background = Brushes.White, Margin = new Thickness(5) };
                    sp.Children.Add(new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["fasilitas_image"].ToString()) }, Height = 100, Width = 100});
                    sp.Children.Add(new TextBlock() { Text = reader["fasilitas_nama"].ToString(), HorizontalAlignment = HorizontalAlignment.Center });
                    spContainer.Children.Add(sp);
                }
                listFasilitas.Content = spContainer;
                App.connection.Close();
            }
            if (type == "SearchKost"){
                // Room : 
                string query = $@"SELECT * FROM ROOM 
                                  WHERE KOST_ID = '{kostID}' 
                                  AND(ROOM_GENDER = '{(App.User as Mahasiswa).Gender}' OR ROOM_GENDER = 2) 
                                  AND ROOM_STATUS = 0";
                using (OracleCommand cmd = new OracleCommand(query, App.connection))
                {
                    App.connection.Open();
                    OracleDataReader reader = cmd.ExecuteReader();
                    List<StackPanel> listSpContainer = new List<StackPanel>();
                    for (int i = 0; i < 3; i++) listSpContainer.Add(new StackPanel() { Margin = new Thickness(5) });
                    // buat 3 grid 
                    List<UIElement> listElem = new List<UIElement>();
                    int idx = 0;
                    if (!reader.HasRows)
                    {
                        TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 5) };
                        tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                        tc.OpeningEffects.Add(new TransitionEffect(new Random().Next(2) == 0 ? TransitionEffectKind.SlideInFromRight : TransitionEffectKind.SlideInFromLeft));
                        Card card = new Card() { Margin = new Thickness(0, 5, 0, 5), Padding = new Thickness(10) , Height = 50};
                        TextBlock tb = new TextBlock() { Text = "No Room Available", FontWeight = FontWeights.SemiBold, FontSize = 16, TextAlignment = TextAlignment.Center };
                        card.Content = tb;
                        tc.Content = card;
                        Grid.SetColumn(tc, 1);
                        listRoom.Children.Add(tc);
                    }
                    while (reader.Read())
                    {
                        TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                        tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                        tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromRight));
                        Card card = new Card() { Margin = new Thickness(5), Padding = new Thickness(5) };
                        StackPanel spContainer = new StackPanel();
                        StackPanel sp1 = new StackPanel() { Margin = new Thickness(10) };
                        int    roomLantai       = int.Parse(reader["room_lantai"].ToString()),
                               roomHarga        = int.Parse(reader["room_harga"].ToString()),
                               roomStatus       = int.Parse(reader["room_status"].ToString());
                        string roomID           = reader["room_id"].ToString(),
                               roomNama         = reader["room_nama"].ToString(),
                               roomDescription  = reader["room_description"].ToString(),
                               roomGender = int.Parse(reader["room_gender"].ToString()) == 0 ? "Man Only" : int.Parse(reader["room_gender"].ToString()) == 1 ? "Woman Only" : "Both ";
                        Grid   grRoomImage      = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) }, Height = 200 };
                        // Buat dapatin jumlah org yg request
                        OracleCommand cmds = new OracleCommand($"select count(*) from rent where rent_status = 0 and rent_delete_status = 0 and room_id = '{roomID}'",App.connection);
                        int jumlahRequest = int.Parse(cmds.ExecuteScalar().ToString());
                        //listElem.Add(grRoomImage);
                        TextBlock textRoomNama = new TextBlock() { Text = roomNama, FontWeight = FontWeights.Bold, FontSize = 16 };
                        TextBlock textRoomHarga = new TextBlock() { Text = $"{CurrencyConverter.ToCurrency(roomHarga)}", FontWeight = FontWeights.SemiBold, Foreground = Brushes.Indigo , FontSize = 16};
                        TextBlock textRoomLantai = new TextBlock() { Text = $"Lantai {roomLantai}" , FontSize = 14};
                        TextBlock textRoomDescription = new TextBlock() { Text = roomDescription , FontSize = 14};
                        TextBlock textRoomGender = new TextBlock() { Text = roomGender , FontSize = 14};
                        TextBlock textjumlahRequest = new TextBlock() { Text = $"Requested by {jumlahRequest} people", FontSize = 14 };
                        OracleCommand cmds2 = new OracleCommand($"select count(*) from rent where room_id = '{roomID}' and mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' and rent_status = 0 and rent_delete_status = 0",App.connection);
                        int isRequested = int.Parse(cmds2.ExecuteScalar().ToString());
                        Button btn = new Button() { Name = roomID, Content = "Pesan" }; ;
                        if (isRequested > 0) {
                            btn = new Button() { Name = roomID, Content = "Requested" };
                            btn.IsEnabled = false;
                        }
                        else {
                            btn = new Button() { Name = roomID, Content = "Pesan" };
                            btn.Click += (source, ev) => {
                                new MhsDialog("DetailRoom", "OkCancel", (source as Button).Name).ShowDialog();
                                App.Actmenu.navigate(new MhsDetailKost(type, kostID));
                            };
                        }
                        spContainer.Children.Add(grRoomImage);
                        sp1.Children.Add(textRoomNama);
                        sp1.Children.Add(textRoomLantai);
                        sp1.Children.Add(textRoomHarga);
                        sp1.Children.Add(textRoomDescription);
                        sp1.Children.Add(textRoomGender);
                        sp1.Children.Add(textjumlahRequest);
                        spContainer.Children.Add(sp1);
                        spContainer.Children.Add(btn);
                        card.Content = spContainer;
                        tc.Content = card;
                        listSpContainer[idx % 3].Children.Add(tc);
                        idx++;
                    }
                    Grid.SetColumn(listSpContainer[0], 0);
                    Grid.SetColumn(listSpContainer[1], 1);
                    Grid.SetColumn(listSpContainer[2], 2);
                    listRoom.Children.Add(listSpContainer[0]);
                    listRoom.Children.Add(listSpContainer[1]);
                    listRoom.Children.Add(listSpContainer[2]);
                    App.connection.Close();
                }
            }
            else if (type == "MyKost"){
                using (OracleCommand cmd = new OracleCommand(
                    $"select * from rent rn " +
                    $"inner join room r on r.room_id = rn.room_id " +
                    $"inner join kost k on k.kost_id = r.kost_id " +
                    $"where k.kost_id = '{kostID}' and rn.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' and rn.rent_status = 1",
                    App.connection
                )){
                    App.connection.Open();
                    OracleDataReader reader = cmd.ExecuteReader();
                    List<StackPanel> listSpContainer = new List<StackPanel>();
                    for (int i = 0; i < 3; i++) listSpContainer.Add(new StackPanel() { Margin = new Thickness(5) });
                    // buat 3 grid 
                    List<UIElement> listElem = new List<UIElement>();
                    int idx = 0;
                    while (reader.Read())
                    {
                        TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                        tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                        tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromRight));
                        Card card = new Card() { Margin = new Thickness(5), Padding = new Thickness(5) };
                        StackPanel spContainer = new StackPanel();
                        StackPanel sp1 = new StackPanel() { Margin = new Thickness(10) };
                        int    roomLantai      = int.Parse(reader["room_lantai"].ToString()),
                               roomHarga       = int.Parse(reader["rent_nominal"].ToString()),
                               roomStatus      = int.Parse(reader["room_status"].ToString());
                        string roomID          = reader["room_id"].ToString(),
                               roomNama        = reader["room_nama"].ToString(),
                               roomDescription = reader["room_description"].ToString(),
                               rentID          = reader["rent_id"].ToString(),
                               roomGender      = int.Parse(reader["room_gender"].ToString()) == 0 ? "Boys Only" : int.Parse(reader["room_gender"].ToString()) == 2 ? "Girls Only" : "Both "; ;
                        Grid grRoomImage       = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) }, Height = 200 };
                        //listElem.Add(grRoomImage);
                        TextBlock textRoomNama = new TextBlock() { Text = roomNama, FontWeight = FontWeights.Bold, FontSize = 16 };
                        TextBlock textRoomHarga = new TextBlock() { Text = $"{CurrencyConverter.ToCurrency(roomHarga)}", FontWeight = FontWeights.SemiBold, Foreground = Brushes.Indigo, FontSize = 16 };
                        TextBlock textRoomLantai = new TextBlock() { Text = $"Lantai {roomLantai}" , FontSize = 14};
                        TextBlock textRoomDescription = new TextBlock() { Text = roomDescription , FontSize = 14 };
                        TextBlock textRoomGender = new TextBlock() { Text = roomGender, FontSize = 14 };
                        Button btnLeave = new Button() { Name = rentID, Content = "Leave Kost", Background = Brushes.Red, BorderBrush = Brushes.Red};
                        btnLeave.Click += (source, ev) => {
                            new MhsDialog("LeaveRoom", "OkCancel", (source as Button).Name).ShowDialog();
                            App.Actmenu.navigate(new MhsMyKost());
                        };
                        spContainer.Children.Add(grRoomImage);
                        sp1.Children.Add(textRoomNama);
                        sp1.Children.Add(textRoomLantai);
                        sp1.Children.Add(textRoomHarga);
                        sp1.Children.Add(textRoomDescription);
                        sp1.Children.Add(textRoomGender);
                        spContainer.Children.Add(sp1);
                        spContainer.Children.Add(btnLeave);
                        card.Content = spContainer;
                        tc.Content = card;
                        listSpContainer[idx % 3].Children.Add(tc);
                        idx++;
                    }
                    Grid.SetColumn(listSpContainer[0], 0);
                    Grid.SetColumn(listSpContainer[1], 1);
                    Grid.SetColumn(listSpContainer[2], 2);
                    listRoom.Children.Add(listSpContainer[0]);
                    listRoom.Children.Add(listSpContainer[1]);
                    listRoom.Children.Add(listSpContainer[2]);
                    App.connection.Close();
                }
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e){
            if (type == "SearchKost") {
                App.Actmenu.navigate(new MhsSearchKost());
            }else {
                App.Actmenu.navigate(new MhsMyKost());
            }
        }

        private void RateBar_MouseDoubleClick(object sender, MouseButtonEventArgs e){
            new MhsDialog("ListReview","Ok",kostID).ShowDialog();
        }
    }
}
