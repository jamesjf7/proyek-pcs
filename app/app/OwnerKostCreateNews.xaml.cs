﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostCreateNews.xaml
    /// </summary>
    public partial class OwnerKostCreateNews : Page
    {
        ActKost kost;
        public OwnerKostCreateNews(ActKost kost)
        {
            InitializeComponent();
            this.kost = kost;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var koneksi = new OracleDataAdapter($"select 'All' \"Account Name\"  , ''  \"RENT ID\" from dual union select ac.account_nama ||' | '|| r2.room_nama ,r1.RENT_ID  from rent r1 inner join room r2 on r1.room_id = r2.room_id inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id inner join account ac on m.account_id = ac.ACCOUNT_ID where r1.rent_status = 1 and r2.kost_id ='{kost.Id}'", App.connection);
            var dtb = new DataTable();
            koneksi.Fill(dtb);
            dtb.Rows[0][1] = "";
            Cr.DisplayMemberPath = "Account Name";
            Cr.SelectedValuePath = "RENT ID";
            Cr.ItemsSource = dtb.DefaultView;
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
             App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }

        private void BtnSend_Click(object sender, RoutedEventArgs e)
        {
            // Check dulu fieldnya
            if (Tbd.Text.ToString() == "" || Tbj.Text.ToString() == "") {
                SweetAlert.Show("Opppsss", "Your field is not ready", msgImage: SweetAlertImage.WARNING);
                return;
            }
            // Confirmation
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if (result == SweetAlertResult.NO) return;
            
            App.connection.Open();
            string qry = "insert into news(KOST_ID,RENT_ID,NEWS_JUDUL,NEWS_DESCRIPTION ) values (:KOST_ID , :RENT_ID , :NEWS_JUDUL ,:NEWS_DESCRIPTION )";
            OracleCommand cmd = new OracleCommand(qry, App.connection);
            cmd.Parameters.Add(":KOST_ID", kost.Id);
            cmd.Parameters.Add(":RENT_ID", Cr.SelectedValue.ToString());
            cmd.Parameters.Add(":NEWS_JUDUL", Tbj.Text.ToString());
            cmd.Parameters.Add(":NEWS_DESCRIPTION", Tbd.Text.ToString());
            cmd.ExecuteNonQuery();
            App.connection.Close();

            // Kasih Pesan + Navigate
            SweetAlert.Show("Success","News successfully created!", 
                msgImage: SweetAlertImage.SUCCESS);
            App.Actmenu.navigate(new OwnerKostDetailKost(kost));
        }
    }
}
