﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewKost.xaml
    /// </summary>
    public partial class AdminViewKost : Page
    {
        
        public AdminViewKost()
        {
            InitializeComponent();
            string query = "select * from kost k join ownerkost ok on k.ownerkost_id=ok.ownerkost_id join account acc on ok.account_id=acc.account_id";
            svkost.Content = load(query);
        }
        public StackPanel load(string query)
        {
            int idx = 0;
            string text = tbsearch.Text;
            StackPanel spContainer = new StackPanel();
            ItemsControl ic = new ItemsControl();
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                tc.OpeningEffects.Add(new TransitionEffect(new Random().Next(2) == 0 ? TransitionEffectKind.SlideInFromRight : TransitionEffectKind.SlideInFromLeft));
                StackPanel sp = new StackPanel() { Margin = new Thickness(10) };
                Card card = new Card() { Margin = new Thickness(0, 5, 0, 5) };
                Grid g = new Grid() { Height = 200 };
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                string kostID = reader.GetString(0),
                            kostNama = reader.GetString(2),
                            kostAlamat = reader.GetString(3),
                            kostDescription = reader.GetString(5);
                ImageSource imKostImage = BitmapConverter.GetBitmapFromBase64(reader.GetString(4));
                TextBlock textKostID = new TextBlock() { Text = kostID, FontSize = 16, FontWeight = FontWeights.DemiBold }; // kostID
                TextBlock textKostNama = new TextBlock() { Text = kostNama, FontSize = 16, FontWeight = FontWeights.Bold }; // kostNama 
                TextBlock textKostAlamat = new TextBlock() { Text = kostAlamat, FontSize = 16, FontWeight = FontWeights.SemiBold }; // kostAlamat
                TextBlock textKostDescription = new TextBlock() { Text = kostDescription, FontSize = 16 }; // kostDescription
                Grid grKostImage = new Grid() { Background = new ImageBrush() { ImageSource = imKostImage } }; // kostImage
                Button btn = new Button() { Name = kostID, Content = "Detail", HorizontalAlignment = HorizontalAlignment.Center };
                btn.Click += (source, ev) => {
                    App.Actmenu.navigate(new AdminViewDetailKost(btn.Name));
                };
                sp.Children.Add(textKostID);
                sp.Children.Add(textKostNama);
                sp.Children.Add(textKostAlamat);
                sp.Children.Add(textKostDescription);
                Grid.SetColumn(grKostImage, 0);
                Grid.SetColumn(sp, 1);
                Grid.SetColumn(btn, 2);
                g.Children.Add(grKostImage);
                g.Children.Add(sp);
                g.Children.Add(btn);
                card.Content = g;
                tc.Content = card;
                ic.Items.Add(tc);
                idx++;
            }
            App.connection.Close();
            spContainer.Children.Add(ic);
            return spContainer;
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            if (tbsearch.Text == "")
            {
                string query = "select * from kost k join ownerkost ok on k.ownerkost_id=ok.ownerkost_id join account acc on ok.account_id=acc.account_id";
                svkost.Content = load(query);
            }
            else
            {
                string query = $"SELECT * FROM KOST WHERE UPPER(KOST_ALAMAT) LIKE '%{tbsearch.Text.ToUpper()}%' OR UPPER(KOST_ID) LIKE '%{tbsearch.Text.ToUpper()}%' OR UPPER(KOST_NAME) LIKE '%{tbsearch.Text.ToUpper()}%'";
                svkost.Content = load(query);
            }
        }
    }
}
