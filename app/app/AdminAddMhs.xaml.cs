﻿using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminAddMhs.xaml
    /// </summary>
    public partial class AdminAddMhs : Page
    {
        DataTable dt;
        public AdminAddMhs()
        {
            InitializeComponent();
            load();
        }
        BitmapImage img = null;
        OpenFileDialog op;
        
        public void load()
        {
            string query = "select JURUSAN_ID,JURUSAN_NAMA from jurusan";
            OracleDataAdapter da = new OracleDataAdapter(query, App.connection);
            dt = new DataTable();
            da.Fill(dt);
            cbjurusan.DisplayMemberPath = "JURUSAN_NAMA";
            cbjurusan.SelectedValuePath = "JURUSAN_ID";
            cbjurusan.ItemsSource = dt.DefaultView;
        }
        
        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            string name = textName.Text;
            var tempImage = img;
            string tahun = tbtahun.Text;
            string jurusan = cbjurusan.SelectedValue.ToString();
            string title = "Uh Oh";
            string message = "";
            int gender;
            if ((bool)rbmale.IsChecked)
            {
                gender = 0;
            }
            else
            {
                gender = 1;
            }
            bool invalid = true;
            if (tempImage == null)
            {
                message = "Please Add Image first";
            }
            else if (new String[] { name, tahun}.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else if (!tahun.ToCharArray().All(c => Char.IsDigit(c)))
            {
                message = "Tahun can only contain number";
            }
            else if (tahun.Length < 4)
            {
                message = "Tahun must be 4 digit number";
            }
            else invalid = false;

            // Give Message
            if (invalid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
                return;
            }

            OracleCommand cmd = new OracleCommand()
            {
                Connection = App.connection,
                CommandText = "generateMahasiswa",
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "name",
                OracleDbType = OracleDbType.Varchar2,
                Value = name
            });
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "gender",
                OracleDbType = OracleDbType.Int32,
                Value = gender
            });
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "image",
                OracleDbType = OracleDbType.Clob,
                Value = BitmapConverter.GetBase64FromFile(op.FileName)
            });
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "tahun",
                OracleDbType = OracleDbType.Varchar2,
                Value = tahun
            });
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "jurid",
                OracleDbType = OracleDbType.Varchar2,
                Value = jurusan
            });
            

            App.connection.Open();
            cmd.ExecuteNonQuery();
            string result = cmd.Parameters["ret"].Value.ToString();
            App.connection.Close();
            if (result == "0")
            {
                SweetAlert.Show("Whoops", "Username already exists", msgImage: SweetAlertImage.ERROR);
            }
            else
            {
                SweetAlert.Show("Congratulation", "You have successfully register", msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new AdminViewMhs());
            }
        }
    }
}
