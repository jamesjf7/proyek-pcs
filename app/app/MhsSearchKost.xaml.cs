﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
namespace app{
    public partial class MhsSearchKost : Page
    {
        public MhsSearchKost()
        {
            InitializeComponent();
        }
        
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            string search = tbSearch.Text.ToUpper();
            string query = $@"SELECT * FROM KOST 
                              WHERE (UPPER(KOST_ALAMAT) LIKE '%{search}%' 
                              OR UPPER(KOST_ID) LIKE '%{search}%' 
                              OR UPPER(KOST_NAME) LIKE '%{search}%') 
                              AND KOST_STATUS = 1
                              AND KOST_ID NOT IN (
                                    select kost_id from blacklist
                                    where mahasiswa_id = '{(App.User as Mahasiswa).MhsId}'
                              ) AND KOST_ID NOT IN (
                                    select kost_id from report 
                                    where report_status = 1
                                    group by kost_id
                                    having sysdate - max(report_updated_at) < 3
                              )";
            svKost.Content = (App.Actmenu as ContainerMhs).Generate_List(query,"SearchKost");   
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string query = $@"SELECT * FROM KOST 
                             WHERE KOST_STATUS = 1 
                             and KOST_ID not in (
                                select kost_id from blacklist
                                where mahasiswa_id = '{(App.User as Mahasiswa).MhsId}'
                             ) and KOST_ID NOT IN (
                                    select kost_id from report 
                                    where report_status = 1
                                    group by kost_id
                                    having sysdate - max(report_updated_at) < 3
                             )";
            svKost.Content = (App.Actmenu as ContainerMhs).Generate_List(query, "SearchKost");
        }
    }
}
