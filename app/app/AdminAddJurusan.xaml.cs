﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminAddJurusan.xaml
    /// </summary>
    public partial class AdminAddJurusan : Page
    {
        public AdminAddJurusan()
        {
            InitializeComponent();
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            string name = textName.Text;
            string kode = tbtahun.Text;
            string title = "Uh Oh";
            string message = "";
            bool invalid = true;
            if (new String[] { name, kode}.Any(t => t == ""))
            {
                message = "Please Fill all field";
            }
            else if (!kode.ToCharArray().All(c => Char.IsDigit(c)))
            {
                message = "Kode can only contain number";
            }
            else invalid = false;

            // Give Message
            if (invalid)
            {
                SweetAlert.Show(title, message, msgImage: SweetAlertImage.WARNING);
                return;
            }

            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand()
            {
                Connection = App.connection,
                CommandText = "generateJurusan",
                CommandType = CommandType.StoredProcedure
            };
            // Buat Return
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            // Parameter Varchar2
            List<KeyValuePair<string, string>> listParam = new List<KeyValuePair<string, string>>();
            listParam.Add(new KeyValuePair<string, string>("name", name));
            listParam.Add(new KeyValuePair<string, string>("kode", kode));
            foreach (var pair in listParam)
            {
                cmd.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = pair.Key,
                    OracleDbType = OracleDbType.Varchar2,
                    Value = pair.Value
                });
            }
            App.connection.Open();
            string result = "0";
            try {
                cmd.ExecuteNonQuery();
                result = cmd.Parameters["ret"].Value.ToString();
            }
            catch (Exception ex) {}
            App.connection.Close();
            if (result == "0")
            {
                SweetAlert.Show("Whoops", "Nama Jurusan/Kode Jurusan already exists", msgImage: SweetAlertImage.ERROR);
            }
            else
            {
                SweetAlert.Show("Congratulation", "You have successfully register jurusan", msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new AdminViewListJurusan());
            }
        }
    }
}
