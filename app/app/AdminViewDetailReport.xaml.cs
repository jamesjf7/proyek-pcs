﻿using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for AdminViewDetailReport.xaml
    /// </summary>
    public partial class AdminViewDetailReport : Page
    {
        string kostid;
        public AdminViewDetailReport(string kode)
        {
            kostid = kode;
            InitializeComponent();
            load();
        }
        public void load()
        {
            listreport.Children.Clear();
            using (OracleCommand cmd = new OracleCommand($"SELECT r.report_id,a.account_nama,r.report_description,a.account_image FROM REPORT r join mahasiswa m on r.mahasiswa_id=m.mahasiswa_id join account a on m.account_id=a.account_id WHERE KOST_ID = '{kostid}' and report_status=0", App.connection))
            {
                App.connection.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                List<StackPanel> listSpContainer = new List<StackPanel>();
                for (int i = 0; i < 3; i++) listSpContainer.Add(new StackPanel() { Margin = new Thickness(5) });
                // buat 3 grid 
                List<UIElement> listElem = new List<UIElement>();
                int idx = 0;
                while (reader.Read())
                {
                    TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                    tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                    tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromRight));
                    StackPanel sp = new StackPanel();
                    Card card = new Card() { Margin = new Thickness(5), Padding = new Thickness(5) };
                    string reportID = reader[0].ToString(),
                           akunNama = reader[1].ToString(),
                           reportDescription = reader[2].ToString();
                    Grid grakunImage = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader[3].ToString()) }, Height = 200 };
                    //listElem.Add(grRoomImage);
                    TextBlock textAkunNama = new TextBlock() { Text = akunNama, FontWeight = FontWeights.Bold };
                    TextBlock textRoomDescription = new TextBlock() { Text = reportDescription };
                    Button btnaccept = new Button() { Name = reportID, Content = "Accept" };
                    btnaccept.Click += (source, ev) =>
                    {
                        string query2 = $"update report set report_status=1 where report_id='{btnaccept.Name}'";
                        OracleCommand cmd2 = new OracleCommand(query2, App.connection);
                        App.connection.Open();
                        cmd2.ExecuteNonQuery();
                        App.connection.Close();
                        App.Actmenu.navigate(new AdminConfirmationReportMhs());
                    };
                    Button btnreject = new Button() { Name = reportID, Content = "Reject" };
                    btnreject.Click += (source, ev) =>
                    {
                        string query2 = $@"
                            update report 
                            set report_status=-1, REPORT_UPDATED_AT=SYSDATE
                            where report_id='{btnaccept.Name}'
                        ";
                        OracleCommand cmd2 = new OracleCommand(query2, App.connection);
                        App.connection.Open();
                        cmd2.ExecuteNonQuery();
                        App.connection.Close();
                        App.Actmenu.navigate(new AdminConfirmationReportMhs());
                    };
                    sp.Children.Add(grakunImage);
                    sp.Children.Add(textAkunNama);
                    sp.Children.Add(textRoomDescription);
                    sp.Children.Add(btnaccept);
                    sp.Children.Add(btnreject);
                    card.Content = sp;
                    tc.Content = card;
                    listSpContainer[idx % 3].Children.Add(tc);
                    idx++;
                }
                Grid.SetColumn(listSpContainer[0], 0);
                Grid.SetColumn(listSpContainer[1], 1);
                Grid.SetColumn(listSpContainer[2], 2);
                listreport.Children.Add(listSpContainer[0]);
                listreport.Children.Add(listSpContainer[1]);
                listreport.Children.Add(listSpContainer[2]);
                App.connection.Close();
            }
            }
    }
}
