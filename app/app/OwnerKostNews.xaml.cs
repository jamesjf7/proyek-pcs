﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app {
    /// <summary>
    /// Interaction logic for OwnerKostNews.xaml
    /// </summary>
    public partial class OwnerKostNews : Page {
        public OwnerKostNews() {
            InitializeComponent();
        }

        List<News> listNews;
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            listNews = new List<News>();
            loadKost();
            loadNews();
            cbKost.SelectedIndex = 0;
        }

        void loadKost() {
            // Clear Combobox
            cbKost.Items.Clear();
            // Setting
            cbKost.DisplayMemberPath = "name";
            cbKost.SelectedValuePath = "id";

            string sql = @"
                select kost_id, kost_name from kost k
                where k.ownerkost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", (App.User as OwnerKost).OwnerId);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            // Add Item to Combobox
            cbKost.Items.Add(new {
                id = "",
                name = "All"
            });
            while (reader.Read()) {
                cbKost.Items.Add(new {
                    id = reader.GetString(0),
                    name = reader.GetString(1)
                });
            }
            App.connection.Close();
        }

        void loadNews() {
            string sql = $@"
                select 
	                to_char(n.news_tanggal,'DD-MON-YYYY') || ' | ' ||
	                n.news_judul || ' | ' || k.kost_name, 
	                (case when r.rent_id is null then 'All' else 
		                a.account_nama || ' (Room ' || ro.room_nama || ')' end),
	                n.news_description,
                    k.kost_id,
                    to_number(case when r.rent_id is null then 0 else 1 end)
                from news n
                left outer join rent r on n.rent_id = r.rent_id
                left outer join room ro on r.room_id = ro.room_id
                left outer join mahasiswa m on r.mahasiswa_id = m.mahasiswa_id
                left outer join account a on m.account_id = a.account_id
                left outer join kost k on n.kost_id = k.kost_id
                where k.ownerkost_id = :id
                order by n.news_id DESC
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", (App.User as OwnerKost).OwnerId);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                string
                    header = reader.GetString(0), // News Header
                    rec    = reader.GetString(1), // To Who
                    msg    = reader.GetString(2), // News Description
                    idKost = reader.GetString(3); // IdKost
                // 0 All, 1 Tertuju
                bool toAll = reader.GetValue(4).ToString() == "0";
                // Add to List
                News tempNews = new News(idKost,header,rec,msg,toAll);
                listNews.Add(tempNews);
            }
            App.connection.Close();
        }

        void generateNewsComponent(string where = "") {
            // Hide Card
            cardNews.Visibility = Visibility.Collapsed;
            // Filter News
            List<News> tempNews = listNews.Where(nw => where=="" || nw.IdKost == where).ToList();
            // Clear Child
            stackNews.Children.Clear();
            foreach (News news in tempNews) {
                // Dynamic Component
                Expander root = new Expander {
                    Foreground = BrushGen.FromString("#fff"),
                    Header = new TextBlock {
                        Text = news.Title,
                        FontSize = 20,
                        Foreground = BrushGen.FromString("#fff")
                    },
                    Margin = new Thickness(0, 0, 0, 10)
                };
                // Background
                LinearGradientBrush grad = new LinearGradientBrush {
                    StartPoint = new Point(0, 0),
                    EndPoint = new Point(1, 1),
                };
                string[,] gradCol = {
                        {"#1488CC","#ff00cc"},
                        {"#4364F7","#911CB1"},
                        {"#0052D4","#333399"},
                    };
                grad.GradientStops.Add(new GradientStop {
                    Offset = 0.0f,
                    Color = ColorGen.FromString(gradCol[0, news.IsPublic ? 0 : 1])
                });
                grad.GradientStops.Add(new GradientStop {
                    Offset = 0.5f,
                    Color = ColorGen.FromString(gradCol[1, news.IsPublic ? 0 : 1])
                });
                grad.GradientStops.Add(new GradientStop {
                    Offset = 1.0f,
                    Color = ColorGen.FromString(gradCol[2, news.IsPublic ? 0 : 1])
                });
                root.Background = grad;
                // Content
                Border border = new Border {
                    Background = BrushGen.FromString("#33000000")
                };
                root.Content = border;
                // Expand > Border > Grid
                Grid grid = new Grid {
                    Margin = new Thickness(10)
                };
                grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                grid.RowDefinitions.Add(new RowDefinition { });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition { });
                border.Child = grid;
                // Child
                string[] texts = {
                        "To"     ,":",news.Receiver,
                        "Message",":",news.Message
                    };
                for (int i = 0; i < texts.Length; i++) {
                    TextBlock textBlock = new TextBlock {
                        Text = texts[i],
                        FontSize = 18,
                        Margin = i % 3 == 1 ? new Thickness(5, 0, 5, 0) : new Thickness(0),
                        FontWeight = i % 3 == 2 ? FontWeights.Thin : FontWeights.Normal,
                    };
                    textBlock.SetValue(Grid.RowProperty, i / 3);
                    textBlock.SetValue(Grid.ColumnProperty, i % 3);
                    grid.Children.Add(textBlock);
                }

                // Add to StackPanel
                stackNews.Children.Add(root);
            }
            // Show Card
            cardNews.Visibility = Visibility.Visible;
        }

        private void CbKost_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            int idx = cbKost.SelectedIndex;
            if (idx == -1) return;
            generateNewsComponent(cbKost.SelectedValue.ToString());
        }
    }
}
