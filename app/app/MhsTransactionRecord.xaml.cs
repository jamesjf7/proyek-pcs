﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using Oracle.DataAccess.Client;
namespace app
{
    public partial class MhsTransactionRecord : Page
    {
        public MhsTransactionRecord()
        {
            InitializeComponent();
        }

        private void BtnTopUp_Click(object sender, RoutedEventArgs e)
        {
            new MhsDialog("TopUp","OkCancel").ShowDialog();
            App.Actmenu.navigate(new MhsTransactionRecord());
        }

        private void BtnTopUpHistory_Click(object sender, RoutedEventArgs e)
        {
            listHistory.Content = ((ContainerMhs)App.Actmenu).Generate_List($"SELECT * FROM TOPUP WHERE ACCOUNT_ID = '{App.User.Id}' ORDER BY 3 DESC","TopUp");
        }

        private void BtnPayHistory_Click(object sender, RoutedEventArgs e)
        {
            // Tabel rent, dtagih, htagih
            string query = $"select * from (" +
                           $"select rn.rent_id, rn.mahasiswa_id, rn.room_id, rn.rent_tanggal as \"TANGGAL\", rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, '' as \"HT.HTAGIH_ID\", -1 as \"HT.HTAGIH_CHARGE\", -1 as \"HT.HTAGIH_TOTAL\", '' as \"DT.HTAGIH_ID\", '' as \"DT.RENT_ID\", 0 as \"DT.DTAGIH_SUBTOTAL\", 0 as \"mode\" from rent rn " +
                           $"union " +
                           $"select rn.rent_id, rn.mahasiswa_id, rn.room_id, ht.htagih_tanggal, rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, ht.htagih_id, ht.htagih_charge, ht.htagih_total, dt.htagih_id,dt.rent_id,dt.dtagih_subtotal,1 from dtagih dt inner join rent rn on rn.rent_id = dt.rent_id inner join htagih ht on ht.htagih_id = dt.htagih_id " +
                           $"union " +
                           $"select rn.rent_id, rn.mahasiswa_id, rn.room_id, u.utang_updated_at, rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, ht.htagih_id, ht.htagih_charge, ht.htagih_total,  u.htagih_id, u.rent_id, u.utang_nominal+u.utang_denda,2 from utang u inner join rent rn on rn.rent_id = u.rent_id inner join htagih ht on ht.htagih_id = u.htagih_id " +
                           $") a where a.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' order by a.\"TANGGAL\" DESC";
            listHistory.Content = ((ContainerMhs)App.Actmenu).Generate_List(query, "PayHistory");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            tb_saldo.Text = $"{CurrencyConverter.ToCurrency(App.User.Saldo)}";
        }

        private void BtnPayUtang_Click(object sender, RoutedEventArgs e)
        {
            string query = $"select * from utang u " +
                           $"inner join rent rn on rn.rent_id = u.rent_id " +
                           $"inner join room r on r.room_id = rn.room_id " +
                           $"inner join htagih ht on ht.htagih_id = u.htagih_id " +
                           $"where rn.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' and u.utang_status = 0";
            listHistory.Content = ((ContainerMhs)App.Actmenu).Generate_List(query, "PayUtang");
        }

        private void BtnReport_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new MhsTransactionReport());
        }
    }
}
