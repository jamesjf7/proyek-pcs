﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostModifyRoom.xaml
    /// </summary>
    public partial class OwnerKostModifyRoom : Page
    {
        ActKost kost;
        ActRoomKost room;
        BitmapImage img;
        public OwnerKostModifyRoom(ActKost kost,ActRoomKost room)
        {
            InitializeComponent();
            this.kost = kost;
            this.room = room;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            // Dipslay
            imgRoom.Source = img = room.Image;
            textFloor.Text = room.Lantai.ToString();
            textName.Text = room.Name;
            textPrice.Text = room.Harga.ToString();
            textNextPrice.Text = room.HargaNext.ToString();
            textDescription.Text = room.Description;
            new[] { radio1, radio2, radio3 }[room.Gender].IsChecked = true;
            // Combobox
            cbStatus.Items.Clear();
            cbStatus.Items.Add("Unavailable");
            cbStatus.Items.Add("Available");
            // Jika Occupied...
            if(room.Occupant != " ") { // Jika occupied
                cbStatus.Items.Add($"{room.DispStatus} | {room.Occupant}");
                cbStatus.IsEnabled = false;
            }
            cbStatus.SelectedIndex = room.Status + 1;
        }

        private void BtnImage_Click(object sender, RoutedEventArgs e) {
            var op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                        "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                        "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == DialogResult.OK) {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                btnImage.Content = newImg;
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e) {
            App.Actmenu.navigate(new OwnerKostUpdateRoom(kost,room.Lantai));
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e) {
            string name = textName.Text;
            string price = textNextPrice.Text;
            string desc = textDescription.Text;
            int gender = getGender();
            int status = cbStatus.SelectedIndex - 1;

            // Check Error
            bool err = true;
            string msg = "Room succesfully Updated!";
            if (new[] { name, price, desc }.Any(f => f == "") || img == null || gender == -1 || status == -2) {
                msg = "Your field are empty!";
            } else if (!kost.isValidRoomName(name,room)) {
                msg = "Room name already exist in your kost!";
            } else if (!price.ToCharArray().All(c => char.IsDigit(c))) {
                msg = "Invalid price!";
            } else if (Convert.ToInt32(price) < 100000) {
                msg = "Minimum price is IDR 100.000!";
            } else {
                err = false;
            }

            if (err) { // Invalid
                SweetAlert.Show("Whoops", msg, msgImage: SweetAlertImage.WARNING);
            } else { // Valid
                var result = new SweetAlert() {
                    Title = "Confirmation",
                    Message = $"Are you sure you want to Update Room {room.Name}?",
                    MsgImage = SweetAlertImage.QUESTION,
                    MsgButton = SweetAlertButton.YesNo,
                    OkText = "Yes",
                    CancelText = "No"
                }.ShowDialog();
                if (result == SweetAlertResult.YES) {
                    // Jika Gender Berubah
                    // Confirm Semua beda gender diReject
                    if (room.Gender!=gender ) {
                        // Jika Ada isinya maka abort
                        if (room.Occupant != " ") {
                            SweetAlert.Show("UH OH",
                                $"Gender Restriction cant be changed in Occupied room",
                            msgImage: SweetAlertImage.ERROR);
                            return;
                        }
                        // Jika Gender baru Ada Restrictionnya (bkn Any)
                        else if(gender != 2) {
                            result = new SweetAlert() {
                                Title = "Confirmation",
                                Message = $"Are you sure to change Gender Restriction?\n[Request with different gender will automatically rejected]",
                                MsgImage = SweetAlertImage.QUESTION,
                                MsgButton = SweetAlertButton.YesNo,
                                OkText = "Yes",
                                CancelText = "No"
                            }.ShowDialog();
                            if (result == SweetAlertResult.NO) {
                                SweetAlert.Show("Cancelled",
                                    $"Gender Restriction remain unchanged :]",
                                msgImage: SweetAlertImage.ERROR);
                                return;
                            }
                        }
                    }
                    // Confirm Semua request diReject krn status Unavailable
                    if (room.Status != status && status == -1) {
                        result = new SweetAlert() {
                            Title = "Confirmation",
                            Message = $"Are you sure to make this room Unavailable?\n[This room will decline all request]",
                            MsgImage = SweetAlertImage.QUESTION,
                            MsgButton = SweetAlertButton.YesNo,
                            OkText = "Yes",
                            CancelText = "No"
                        }.ShowDialog();
                        if (result == SweetAlertResult.NO) {
                            SweetAlert.Show("Cancelled",
                                $"Room still available for anyone :]",
                            msgImage: SweetAlertImage.ERROR);
                            return;
                        }
                    }
                    // Update Room
                    OracleCommand cmd = new OracleCommand {
                        Connection = App.connection,
                        CommandText = "UpdateRoom",
                        CommandType = CommandType.StoredProcedure,
                    };
                    #region Parameter
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "id",
                        OracleDbType = OracleDbType.Varchar2,
                        Value = room.Id
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "image",
                        OracleDbType = OracleDbType.Clob,
                        Value = BitmapConverter.GetBase64FromBitmap(img)
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "nama",
                        OracleDbType = OracleDbType.Varchar2,
                        Value = name
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "harga",
                        OracleDbType = OracleDbType.Int32,
                        Value = Convert.ToInt32(price)
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "detail",
                        OracleDbType = OracleDbType.Varchar2,
                        Value = desc
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "gender",
                        OracleDbType = OracleDbType.Int32,
                        Value = gender
                    });
                    cmd.Parameters.Add(new OracleParameter {
                        ParameterName = "status",
                        OracleDbType = OracleDbType.Int32,
                        Value = status
                    });
                    #endregion
                    App.openConnection();
                    cmd.ExecuteNonQuery();
                    App.closeConnection();
                    // Give Message and Navigate to Update Room
                    SweetAlert.Show("Success", msg, msgImage: SweetAlertImage.SUCCESS);
                    App.Actmenu.navigate(new OwnerKostUpdateRoom(kost,room.Lantai));
                }
            }
        }

        #region Function
        int getGender() {
            return (bool)radio1.IsChecked ? 0 : (bool)radio2.IsChecked ? 1 : (bool)radio3.IsChecked ? 2 : -1;
        }
        #endregion
    }
}
