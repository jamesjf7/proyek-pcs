﻿
create or replace function generateAdmin(
  name IN varchar2,
  username IN varchar2,
  password IN varchar2,
  image IN clob
)return number
is
  id varchar2(5);
  total number;
begin
  select count(*) into total from account where account_username = username;
  if total > 0 then
    return 0;
  end if;
  id := autoGenerateuser();
  insert into account values('test', name, username, password, image, 0, 1, 0);
  insert into admin values('',id);
  return 1;
end;
/
show err;

create or replace function autoGenerateAdmin
return varchar2
is
  kode varchar2(5);
  acc number;
begin
  select nvl(max(to_number(substr(Admin_ID,3))),0) into acc from admin;
  acc := acc + 1;
  return 'AD' || lpad(acc,3,'0');
end;
/
show err;

create or replace trigger tInsertAdmin
before insert on admin
FOR EACH ROW
DECLARE
  newId varchar2(5);
BEGIN
  newId := autoGenerateAdmin();
  :new.admin_ID := newId;
END;
/
show err;

