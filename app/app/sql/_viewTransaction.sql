-- OWNERKOST 
drop view kosttrans;
create or replace view kosttrans
as
select *
from
( 
    select 
        rent_verified_at as payment_date,
        'Down Payment '||a.account_nama||' Room '||r2.room_nama as detail, 
        rent_nominal as nominal,
        ' ' as room_id,
        r2.kost_id as kost_id
    from rent r1
    inner join room r2 on r1.room_id = r2.room_id
    inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
    inner join account a on m.account_id = a.account_id 
    where rent_status in (-2,1,2)	
    union(
        select 
            h.htagih_tanggal,
            'Monthly Bill ' ||a.account_nama|| ' Room '||r2.room_nama,
            d.dtagih_subtotal,
            r1.room_id,
            h.kost_id
        from dtagih d
        inner join htagih h on d.htagih_id = h.htagih_id
        inner join rent r1 on d.rent_id = r1.rent_id
        inner join room r2 on r1.room_id = r2.room_id
        inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
        inner join account a on m.account_id = a.account_id 
        union
        select 
            h.htagih_tanggal,
            'Administrative Costs Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
            -floor(d.dtagih_subtotal/10),
            r1.room_id,
            h.kost_id
        from dtagih d
        inner join htagih h on d.htagih_id = h.htagih_id
        inner join rent r1 on d.rent_id = r1.rent_id
        inner join room r2 on r1.room_id = r2.room_id
        inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
        inner join account a on m.account_id = a.account_id 
    )union(
        select 
            u.utang_updated_at,
            'Late Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
            u.utang_nominal+u.utang_denda,
            r1.room_id,
            r2.kost_id
        from utang u
        inner join rent r1 on u.rent_id = r1.rent_id
        inner join room r2 on r1.room_id = r2.room_id
        inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
        inner join account a on m.account_id = a.account_id
        where u.utang_status = 1 
        union
        select 
            u.utang_updated_at,
            'Administrative Costs Late Monthly Bill '||a.account_nama|| ' Room '||r2.room_nama,
            -floor((u.utang_nominal+u.utang_denda)/10),
            r1.room_id,
            r2.kost_id
        from utang u
        inner join rent r1 on u.rent_id = r1.rent_id
        inner join room r2 on r1.room_id = r2.room_id
        inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
        inner join account a on m.account_id = a.account_id 
        where u.utang_status = 1
    )
);


-- MAHASISWA
create or replace view transactionrecord
as
select * from (
	select rn.rent_id, rn.mahasiswa_id, rn.room_id, rn.rent_tanggal as "TANGGAL", rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, '-1' as "HT.HTAGIH_ID", -1 as "HT.HTAGIH_CHARGE", -1 as "HT.HTAGIH_TOTAL", '' as "DT.HTAGIH_ID", '' as "DT.RENT_ID", 0 as "DT.DTAGIH_SUBTOTAL", 0 as "mode" 
	from rent rn 
union 
	select rn.rent_id, rn.mahasiswa_id, rn.room_id, ht.htagih_tanggal, rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, ht.htagih_id, ht.htagih_charge, ht.htagih_total, dt.htagih_id,dt.rent_id,dt.dtagih_subtotal,1 
	from dtagih dt 
	inner join rent rn on rn.rent_id = dt.rent_id 
	inner join htagih ht on ht.htagih_id = dt.htagih_id
union
	select rn.rent_id, rn.mahasiswa_id, rn.room_id, u.utang_updated_at, rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, ht.htagih_id, ht.htagih_charge, ht.htagih_total,  u.htagih_id, u.rent_id, u.utang_nominal+u.utang_denda,2 from 
	utang u 
	inner join rent rn on rn.rent_id = u.rent_id 
	inner join htagih ht on ht.htagih_id = u.htagih_id
	where u.utang_status = 1
) a order by a."TANGGAL" DESC;