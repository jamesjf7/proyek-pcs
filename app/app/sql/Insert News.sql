create or replace function autoGenerateNews
return varchar2
is 
	nnews number;
begin 
	select nvl(max(to_number(substr(news_id,3))),0) into nnews from news;
	nnews := nnews + 1;
	return 'NW' || lpad(nnews,3,'0'); 
end;
/
show err;

create or replace trigger tInsertNews
before insert on news
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	:new.news_tanggal := SYSDATE;
	newId := autoGenerateNews();
	:new.news_id:= newId;
	:new.news_status:= 1;
END;
/

