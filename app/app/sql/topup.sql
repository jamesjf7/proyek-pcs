	create or replace trigger tInsertTopUp
	before insert on topup
	FOR EACH ROW
	DECLARE
	BEGIN
		select 'TU'||lpad(count(*)+1,3,'0') into :new.TOPUP_ID from TOPUP;
		update ACCOUNT set ACCOUNT_SALDO = ACCOUNT_SALDO + :new.TOPUP_NOMINAL where ACCOUNT_ID = :new.ACCOUNT_ID;
	END;
	/
	show err;

-- create or replace procedure TopUpMahasiswa(
	-- mhsId in varchar2,
	-- saldo in number
-- )
-- is
-- begin
	-- insert into TOPUP values('',mhsId,sysdate,saldo);
	-- update ACCOUNT set ACCOUNT_SALDO = ACCOUNT_SALDO + saldo where ACCOUNT_ID = mhsId;
-- end;
-- /
-- show err;
