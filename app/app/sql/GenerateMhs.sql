﻿
create or replace function autoGenerateNrp(
  tahun in number,
  kodejur in number
)
return varchar2
is
  kode varchar2(3);
  acc number;
begin
  select nvl(max(to_number(substr(account_username,6))),0) into acc from account where account_type=1 and substr(account_username,4,2)=kodejur;
  acc := acc + 1;
  kode := substr(tahun,1,1)||substr(tahun,3,2);
  return kode || kodejur || lpad(acc,4,'0');
end;
/
show err;

create or replace function generateMahasiswa(
  name IN varchar2,
  gender IN number,
  image IN clob,
  tahun in number,
  jurid in varchar2
)return number
is
  id varchar2(5);
  total number;
  username varchar2(255);
  password varchar2(255);
  temp varchar2(5);
begin
  select JURUSAN_KODE into temp from jurusan where JURUSAN_ID=jurid;
  username := autogenerateNrp(tahun,temp);
  password := username;
  select count(*) into total from account where account_username = username;
  if total > 0 then
    return 0;
  end if;
  id := autoGenerateuser();
  insert into account values('sdc', name, username, password, image, 0, 1, 1);
  insert into mahasiswa values('',id,gender,tahun,jurid);
  return 1;
end;
/
show err;

create or replace function autoGenerateMahasiswa
return varchar2
is
  kode varchar2(5);
  acc number;
begin
  select nvl(max(to_number(substr(Mahasiswa_id,3))),0) into acc from mahasiswa;
  acc := acc + 1;
  return 'MH' || lpad(acc,3,'0');
end;
/
show err;

create or replace trigger tInsertMahasiswa
before insert on mahasiswa
FOR EACH ROW
DECLARE
  newId varchar2(5);
BEGIN
  newId := autoGenerateMahasiswa();
  :new.mahasiswa_ID := newId;
END;
/
show err;

