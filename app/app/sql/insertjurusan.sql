﻿
create or replace function autoGenerateJurusan
return varchar2
is
  kode varchar2(5);
  acc number;
begin
  select nvl(max(to_number(substr(JURUSAN_ID,3))),0) into acc from jurusan;
  acc := acc + 1;
  return 'KJ' || lpad(acc,3,'0');
end;
/
show err;

create or replace trigger tInsertJurusan
before insert on jurusan
FOR EACH ROW
DECLARE
  newId varchar2(5);
BEGIN
  newId := autoGenerateJurusan();
  :new.JURUSAN_ID := newId;
END;
/
show err;


create or replace function generateJurusan(
  name IN varchar2,
  kode number
)return number
is
  id varchar2(5);
  total number;
begin
  select count(*) into total from jurusan where JURUSAN_NAMA = name or JURUSAN_KODE=kode;
  if total > 0 then
    return 0;
  end if;
  insert into jurusan values('', lpad(kode,2,0) ,name);
  return 1;
end;
/
show err;
