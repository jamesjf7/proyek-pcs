set serveroutput on;
create or replace function rentResponse(
	rid IN varchar2,
	resp IN number
)return number
is
	roomid varchar2(10);
begin
	-- dapatkan roomid
	select room_id into roomid
	from rent where rent_id = rid;

	-- response
	update rent 
	set rent_status = resp
	where rent_id = rid;
	
	if resp = 1 then
		-- tolak sisa
		update rent 
		set rent_status = -1
		where room_id = roomid AND 
			rent_id != rid AND 
			rent_status = 0;
	end if;
	
	return 1;
end;
/

-- declare
	-- n number;
	-- stat number;
-- begin 
	-- stat := -1;
	-- n := rentResponse('RN001',stat);
-- end;
-- /

create or replace trigger tRentResponse
before update on rent
for each row
declare
	accid VARCHAR2(10);
begin
	:new.rent_updated_at := SYSDATE;
	if :old.rent_status = :new.rent_status then
		dbms_output.put_line('Status unChanged');
	-- Accept
	elsif :new.rent_status = 1 then
		-- buat kamar jadi occupied
		update room
		set room_status = 1
		where room_id = :old.room_id;
		-- Tambah Saldo OwnerKost
		update account
		set account_saldo = account_saldo + :old.rent_nominal
		where account_id = (
			select account_id
			from OwnerKost ok
			inner join kost k on ok.OwnerKost_id = k.OwnerKost_id
			inner join room r on k.kost_id  = r.kost_id
			where r.room_id = :old.room_id
		);
		-- Verify At
		:new.rent_verified_at := SYSDATE;
	-- Reject
	elsif :new.rent_status = -1 then
		-- Dapetin Account ID
		select account_id into accid
		from mahasiswa 
		where mahasiswa_id = :old.mahasiswa_id;
		-- kembaliin saldo
		update account
		set account_saldo = account_saldo + :old.rent_nominal
		where account_id = accid;
	-- Kick
	elsif :new.rent_status = -2 then
		-- Update Room
		-- Diubah jadi available
		update room
		set room_status = 0
		where room_id = :old.room_id and room_status <> -2;
	-- Keluar Sendiri
	elsif :new.rent_status = 2 then
	  	-- Ubah Room Jadi Available
		update room
		set room_status = 0
		where room_id = :old.room_id and room_status <> -2;
	end if;
end;
/
