create or replace trigger tInsertReport
before insert on report
FOR EACH ROW
DECLARE
BEGIN
	select 'RP'||nvl(lpad(max(to_number(substr(REPORT_ID,3)))+1,'3',0),'001') into :new.REPORT_ID from REPORT;
	:new.REPORT_CREATED_AT := SYSDATE;
	:new.REPORT_UPDATED_AT := SYSDATE;
END;
/
show err;
