create or replace function generateOwnerKost(
	name IN varchar2,
	username IN varchar2,
	password IN varchar2,
	email IN varchar2,
	telp IN varchar2,
	image IN clob
)return number
is
	id varchar2(5);
	total number;
begin
	select count(*) into total from account where account_username = username;
	if total > 0 then
		return 0;
	end if;
	id := autoGenerateuser();
	insert into account values('sdc', name, username, password, image, 0, 0, 2);
	insert into ownerkost values('',id,telp,email);
	return 1;
end;
/
show err;

create or replace function autoGenerateOwnerKost
return varchar2
is
	kode varchar2(5);
	acc number;
begin
	select nvl(max(to_number(substr(OWNERKOST_ID,3))),0) into acc from ownerkost;
	acc := acc + 1;
	return 'OK' || lpad(acc,3,'0');
end;
/
show err;

create or replace trigger tInsertOwnerKost
before insert on ownerkost
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	newId := autoGenerateOwnerKost();
	:new.OWNERKOST_ID := newId;
END;
/
show err;

-- Contoh Untuk manggil Functionnya
-- DECLARE
-- 	n number;
-- begin
-- 	n := generateOwnerKost(
-- 		'Dino','user','pass','asdasd',
-- 		'0821431', 'davindx88@gmail.com'
-- 	);	
-- end;
-- /

-- select account_id from account;

-- insert into account values('sdc', 'name', 'asdasdasd', 
-- 	'password', 'image', 0, 0, 2);

-- set serveroutput on;
-- declare
-- 	n varchar2(5);
-- begin
-- 	n := autoGenerateuser();
-- 	dbms_output.put_line(n);
-- end;
-- /
-- cl scr
