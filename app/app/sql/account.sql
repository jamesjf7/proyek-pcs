create or replace function autoGenerateUser
return varchar2
is
	kode varchar2(5);
	acc number;
begin
	select nvl(max(to_number(substr(account_id,3))),0) into acc from account;
	acc := acc + 1;
	return 'AC' || lpad(acc,3,'0');
end;
/
show err;

create or replace trigger tInsertUser
before insert on account
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	newId := autoGenerateUser();
	:new.account_id := newId;
END;
/

-- cl scr
