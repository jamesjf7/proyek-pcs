create or replace trigger tInsertRent
before insert on rent
FOR EACH ROW
DECLARE
	accountID varchar2(5);
	accountSaldo number;
	roomHarga number;
	err exception;
BEGIN
	select 'RN'||nvl(lpad(max(to_number(substr(RENT_ID,3)))+1,'3',0),'001') into :new.RENT_ID from RENT;
	select sysdate into :new.RENT_TANGGAL from DUAL;
	select room_harga into :new.RENT_NOMINAL from ROOM where ROOM_ID = :new.ROOM_ID;
	select sysdate into :new.RENT_UPDATED_AT from DUAL;
	
	select room_harga into roomHarga from room r where room_id = :new.ROOM_ID;
	select a.account_id into accountID from account a inner join mahasiswa m on m.account_id = a.account_id where m.mahasiswa_id = :new.mahasiswa_id;
	select account_saldo into accountSaldo from account where account_id = accountID;
	
	if accountSaldo - roomHarga >= 0 then
		update account set account_saldo = account_saldo - roomHarga where account_id = accountID;
	else	
		raise err;
	end if;
EXCEPTION 
	when err then
		RAISE_APPLICATION_ERROR('-20001','Saldo Tidak Cukup');
END;
/
show err;

-- 0 ada, 1 delete
create or replace trigger tUpdateRent
after update on rent
FOR EACH ROW
DECLARE
	accountID varchar2(5);
	mahasiswaID varchar2(9);
	rentNominal number;
BEGIN
	if :new.rent_delete_status = 1 then
		mahasiswaID := :old.mahasiswa_id;
		rentNominal := :old.rent_nominal;
		select account_id into accountID from mahasiswa where mahasiswa_id = mahasiswaID;
		update account set account_saldo = account_saldo + rentNominal where account_id = accountID;
	end if;
END;
/
SHOW ERR;

-- apakah mahasiswa sedang mengkost
-- create or replace function isMhsRent(
	-- mhsID IN varchar2
-- )return number
-- is
	-- total number;
-- begin
	-- select count(*) into total from RENT where MAHASISWA_ID = mhsID AND RENT_STATUS = 1;
	-- if total > 0 then
		-- return 1;
	-- end if;
	-- return 0;
-- end;
-- /
-- show err;


