create or replace function autoGenerateRoom
return varchar2
is 
	kr number;
begin 
	select nvl(max(to_number(substr(ROOM_ID,3))),0) into kr from room;
	kr := kr + 1;
	return 'RO' || lpad(kr,3,'0'); 
end;
/
show err;

create or replace trigger tInsertRoom
before insert on room 
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	newId := autoGenerateRoom();
	:new.room_id := newId;
	:new.room_harga_next := :new.room_harga;
END;
/

set serveroutput on;
create or replace procedure DeleteRoom(
  id in varchar2
)
is
begin
	-- Kick Mhs
	update rent set rent_status = -2
	where rent_status = 1 and room_id = id;
	-- Reject Request
	update rent set rent_status = -1
	where rent_status = 0 and room_id = id;
	-- Soft Delete
	update room set room_status = -2
	where room_id = id;
end;
/
show err;

create or replace procedure UpdateRoom(
	id in varchar2,
	image in CLOB,
	nama in varchar2,
	harga in number,
	detail in varchar2,
	gender in number,
	status in number
)
is
begin
	-- Update Room
	update room
	set 
		room_image = image,
		room_nama = nama,
		room_harga_next = harga,
		room_description = detail,
		room_gender = gender,
		room_status = status
	where room_id = id;
end;
/
show err;

create or replace trigger tUpdateRoom
before update on room
for each row
DECLARE
	rentid varchar2(100);
	title varchar2(100);
	msg varchar2(255);
	noccu number;
begin
	-- Jika harga naik atau turun
	if :old.room_harga_next <> :new.room_harga_next then
		-- Beri news untuk Occupant
		select count(*) into noccu from rent
		where room_id = :old.room_id and rent_status = 1;
		-- If there's Occupant...
		if noccu <> 0 then
			select nvl(rent_id,' ') into rentid from rent 
			where room_id = :old.room_id and rent_status = 1;
			title := 'Price Changed (Room '||:old.room_nama||')';
			msg := 
				'Rent price will changed to IDR '||
				trim(replace(to_char(:new.room_harga_next, '999,999,999'),',','.'))||
				'. This changes will apply in next monthly bill';
			insert into news values ('Id',:old.kost_id,rentid,title,'',msg,1);
		end if;
	end if;
	-- Jika Gender berubah
	if :old.room_gender <> :new.room_gender then
		-- Reject rent
		update rent set rent_status = -1
		where rent_status = 0 and
			rent_delete_status = 0 and
			room_id = :old.room_id and 
			:new.room_gender <> 2 and
			mahasiswa_id IN (
				select mahasiswa_id from mahasiswa
				where mahasiswa_gender <> :new.room_gender
			);
	end if;
	-- Jika Status berubah
	if :old.room_status <> :new.room_status then
		-- UnAvailable
		if :new.room_status = -1 then
			-- Reject
			update rent set rent_status = -1
			where room_id = :old.room_id and
				rent_status = 0 and
				rent_delete_status = 0;
		end if;
	end if;
end; 
/
