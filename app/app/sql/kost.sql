-- FUNCTION UNTUK AUTO GEN KOST 
create or replace function autoGenerateKost
return varchar2
is 
	kt number(5);
	tes varchar2(5);
begin 
	select nvl(max(to_number(substr(KOST_ID ,3))),0) into kt from kost;
	kt := kt + 1;
	tes:= 'KO' || lpad(kt,3,'0');
	return tes; 
end;
/
show err;




-- TRIGGERED UNTUK MEMBUAT INSERT
create or replace trigger tInsertKost
before insert on kost
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	newId := autoGenerateKost();
	:new.KOST_ID := newId;
END;
/



  
-- Untuk melakukan 	insert

create or replace function generateKost(
  OWNERKOST_ID IN  VARCHAR2,
  KOST_NAME in  VARCHAR2,
  KOST_ALAMAT in  VARCHAR2,
  KOST_DESCRIPTION in  VARCHAR2,
  KOST_DENDA_TELAT in  NUMBER,
  KOST_BATAS_HARI_TELAT in  NUMBER,
  KOST_STATUS in  NUMBER,
  KOST_IMAGE in  CLOB,
  KOST_CREATED_AT in  DATE
)return varchar2
is
  id varchar2(5);
begin
  id:= autoGenerateKost();
  insert into kost values('a', OWNERKOST_ID, KOST_NAME, KOST_ALAMAT, KOST_IMAGE, KOST_DESCRIPTION, KOST_CREATED_AT,KOST_DENDA_TELAT,KOST_BATAS_HARI_TELAT,1);
  return id;
end;
/
show err;

-- DECLARE
-- id varchar2(5);
-- BEGIN
-- 	id:=generateKost('OK001' , 'ASD','ASD' , 'ASD',1,1,1,1,'ASD',TO_DATE('10/01/2020','DD/MM/YYYY'));	
-- END;
-- /
-- insert into kost values ('KO001', 'OK001' , 'ASD','ASD' , 'ASD', 'ASD', TO_DATE('10/01/2020','DD/MM/YYYY'), 1,1,1,1);


-- TARGET :
-- - NEWS
-- - FASILITAS
-- - KELUAR
-- - PAY HISTORY v
-- - Cancel Balik duit v
-- - 

-- select * from (
-- select rn.rent_id, rn.mahasiswa_id, rn.room_id, rn.rent_tanggal as "TANGGAL", rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, '' as "HT.HTAGIH_ID", -1 as "HT.HTAGIH_CHARGE", -1 as "HT.HTAGIH_TOTAL", '' as "DT.HTAGIH_ID", '' as "DT.RENT_ID", -1 as "DT.DTAGIH_SUBTOTAL" from rent rn
-- union
-- select rn.rent_id, rn.mahasiswa_id, rn.room_id, ht.htagih_tanggal, rn.rent_nominal, rn.rent_status, rn.rent_delete_status, rn.rent_updated_at, ht.htagih_id, ht.htagih_charge, ht.htagih_total, dt.htagih_id,dt.rent_id,dt.dtagih_subtotal from dtagih dt inner join rent rn on rn.rent_id = dt.rent_id inner join htagih ht on ht.htagih_id = dt.htagih_id
-- ) a order by a."TANGGAL" DESC;


select ne.news_id, ne.news_tanggal, ne.news_judul, ne.news_judul from news ne
left join rent re on re.rent_id = ne.rent_id
inner join kost ko on ko.kost_id = ne.kost_id
WHERE re.mahasiswa_id = 'MH001' and ne.news_status = 1
union 
select ne.news_id, ne.news_tanggal, ne.news_judul, ne.news_judul from news ne
left join rent re on re.rent_id = ne.rent_id
inner join kost ko on ko.kost_id = ne.kost_id
WHERE news_status = 1 and ne.rent_id is null and ne.news_tanggal >
(
	select max(re.rent_tanggal) from kost ko 
	inner join room ro on ro.kost_id = ko.kost_id
	inner join rent re on re.room_id = ro.room_id
	where re.rent_status != 0 and re.rent_status != -1 and re.mahasiswa_id = 'MH001'
)



