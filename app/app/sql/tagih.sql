create or replace procedure tagih
is
	lastDate date;
	diffTagih number;
	id varchar2(5); -- ID Tagih
	tot number; -- Grand Total Htagih
	chg number; -- Charge (Keuntungan Admin)
	nom number; -- Total yg harus dibayar (+Denda)
begin
    for kost in (
        select * from kost
		where kost_status = 1
    ) loop
		-- Dapetin Tanggal Terakhir
		select
			nvl(
				(
					select max(htagih_tanggal) from htagih
					where kost_id = kost.kost_id
				),kost.kost_created_at
			)
		into lastDate from dual;
		-- Hitung Selisih hari..
		diffTagih := SYSDATE-lastDate;
		dbms_output.put_line('Day Difference: ' || diffTagih);
		-- Jika 30 Hari maka.. (TAGIH)
		if diffTagih >= 30 or true then
			-- Hitung Pendapatan... (yang bisa bayar)
			-- select nvl(sum(r2.room_harga),0)
			-- into tot from rent r1
			-- inner join room r2 on r1.room_id = r2.room_id
			-- inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
			-- inner join account a on m.account_id = a.account_id
			-- where r1.rent_status = 1 and r2.room_status = 1 and 
			-- 	r2.kost_id = kost.kost_id and 
			-- 	a.account_saldo >= r2.room_harga;
			-- Keuntungan Admin (10%)
			
			-- Init CHg,tot
			chg := 0;
			tot := 0;
			-- Insert Header
			insert into htagih values('',kost.kost_id,SYSDATE,chg,tot)
			RETURNING htagih_id INTO id;
			-- Generate ID + Update Uang OwnerKost
			dbms_output.put_line('Htagih ID: ' || id);
			-- Insert Detail
			for rent in (
				select *
				from rent r1
				inner join room r2 on r1.room_id = r2.room_id
				inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
				inner join account a on m.account_id = a.account_id
				where r2.kost_id = kost.kost_id and
					r1.rent_status = 1
			)loop
				-- Jika Bisa bayar.. maka bayar
				if rent.account_saldo >= rent.room_harga then
					-- Tambah total
					tot := tot + rent.room_harga;
					-- Insert DetailTagih
					insert into dtagih values(id,rent.rent_id,rent.room_harga);
					-- Saldo berkurang di Trigger
					dbms_output.put_line(rent.account_nama || ' Success bayar!');
				else
					-- Insert Utang
					insert into utang 
					values(id,rent.rent_id,rent.room_harga,
						kost.kost_denda_telat,0,SYSDATE);
					dbms_output.put_line(rent.account_nama || ' Hutang!');
				end if;
			end loop;

			-- Count Total
			chg := floor(tot/10);
			tot := tot - chg;
			-- Update HTAGIH
			update htagih
			set 
				HTAGIH_CHARGE = chg, 
				HTAGIH_TOTAL = tot
			where htagih_id = id;
			-- Update Saldo OwnerKost
			update account
			set account_saldo = account_saldo + tot
			where account_id = (
				select ok.account_id from kost k
				inner join ownerkost ok on k.ownerkost_id = ok.ownerkost_id
				where k.kost_id = kost.kost_id
			);

			-- Jika sudah nagih maka harga room akan update
			update room
			set room_harga = room_harga_next
			where kost_id = kost.kost_id;
			-- End Of Tagih
		end if;
    end loop;
end;
/

create or replace function autoGenerateHTagih
return varchar2
is
	kode varchar2(5);
	acc number;
begin
	select nvl(max(to_number(substr(htagih_id,3))),0) into acc from htagih;
	acc := acc + 1;
	return 'HT' || lpad(acc,3,'0');
end;
/

create or replace trigger tInsertHtagih
before insert on htagih
for each row
declare
begin
	:new.htagih_id := autoGenerateHTagih();
end;
/

create or replace trigger tInsertDtagih
before insert on dtagih
for each row
declare
begin
	--Kurangi Saldo
	update account 
	set account_saldo = account_saldo - :new.dtagih_subtotal
	where account_id = (
		select m.account_id  
		from rent r
		inner join mahasiswa m on r.mahasiswa_id = m.mahasiswa_id
		where r.rent_id = :new.rent_id
	);
end;
/

create or replace trigger tUpdateUtang
before update on utang
for each row
declare
	saldo number;
	tot number;
	chg number;
	err EXCEPTION;	
begin
	select a.account_saldo into saldo
	from rent r
	inner join mahasiswa m on r.mahasiswa_id = m.mahasiswa_id
	inner join account a on m.account_id = a.account_id
	where r.rent_id = :old.rent_id;
	-- Jika tidak bisa bayar...
	if saldo < :old.utang_nominal + :old.utang_denda then
		raise err;
	end if;
	-- Jika bayar...
	if :new.utang_status = 1 then
		update account 
		set account_saldo = account_saldo - (
			:old.utang_nominal + :old.utang_denda
		)
		where account_id = (
			select m.account_id  
			from rent r
			inner join mahasiswa m on r.mahasiswa_id = m.mahasiswa_id
			where r.rent_id = :old.rent_id
		);
		-- Calculate Charge
		tot := :old.utang_nominal + :old.utang_denda;
		chg := floor(tot/10);
		tot := tot - chg;
		-- Update Htagih
		update htagih
		set htagih_charge = htagih_charge + chg,
			htagih_total = htagih_total + tot
		where htagih_id = :old.htagih_id;
		-- Update Saldo OwnerKost
		update account
		set account_saldo = account_saldo + tot
		where account_id = (
			select ok.account_id
			from rent r1
			inner join room r2 on r1.room_id = r2.room_id
			inner join kost k on r2.kost_id = k.kost_id
			inner join ownerkost ok on k.ownerkost_id = ok.ownerkost_id
			where r1.rent_id = :old.rent_id
		);
	end if;
	:new.utang_updated_at := SYSDATE;
exception
	when err then
		RAISE_APPLICATION_ERROR('-20001','Saldo Tidak Cukup');
end;
/

-- Coba bayar Utang (Update Status)
-- update account set account_saldo = account_saldo + 1000000;
-- select account_saldo from account;
-- select * from utang;
-- select * from htagih;
-- update utang set utang_status = 1 where htagih_id = 'HT001';
-- select account_saldo from account;
-- select * from utang;
-- select * from htagih;

