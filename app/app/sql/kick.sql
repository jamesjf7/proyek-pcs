set serveroutput on;
create or replace procedure kick
is
begin
	-- Update Rent Status jadi -2(Kicked)
	-- Dilihat dari table utang yang utangnya masih belum dibayar
	-- posisinya masih nyewa dan sudah lewat dari batas yg diberikan
	update rent
	set rent_status = -2
	where rent_id IN (
		select u.rent_id from utang u
		inner join rent r1 on u.rent_id = r1.rent_id
		inner join htagih h on u.htagih_id = h.htagih_id
		inner join kost k on h.kost_id = k.kost_id
		where utang_status = 0 and rent_status = 1 and
			(SYSDATE - h.htagih_tanggal) > k.kost_batas_hari_telat
	);
	-- Untuk ubah status room akan dilakukan diTrigger
end;
/
show err;
