create or replace function autoGenerateFasilitas
return varchar2
is 
	ft number;
begin 
	select nvl(max(to_number(substr(FASILITAS_ID,3))),0) into ft from fasilitas;
	ft := ft + 1;
	return 'FA' || lpad(ft,3,'0'); 
end;
/
show err;

create or replace trigger tInsertFasilitas
before insert on fasilitas
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	newId := autoGenerateFasilitas();
	:new.FASILITAS_ID:= newId;
END;
/

create or replace trigger tUpdateFasilitas
before update on fasilitas
FOR EACH ROW
DECLARE
	newId varchar2(5);
BEGIN
	:new.fasilitas_id := :old.fasilitas_id;
END;
/
