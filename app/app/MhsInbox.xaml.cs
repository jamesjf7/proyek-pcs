﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
namespace app
{
    /// <summary>
    /// Interaction logic for MhsInbox.xaml
    /// </summary>
    public partial class MhsInbox : Page
    {
        public MhsInbox()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            string query = $"select * from ( " +
                           $"select ko.kost_id, ko.kost_name, ne.news_id, ne.news_tanggal, ne.news_judul, ne.news_description from news ne " +
                           $"inner join rent re on re.rent_id = ne.rent_id " +
                           $"inner join kost ko on ko.kost_id = ne.kost_id " +
                           $"WHERE re.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' and ne.news_status = 1 " +
                           $"union " +
                           $@"
                            select ko.kost_id, ko.kost_name, ne.news_id, ne.news_tanggal, ne.news_judul, ne.news_description
                            from news ne
                            inner join (
                                select n.news_id
                                from rent r1
                                inner join room r2 on r1.room_id = r2.room_id
                                inner join news n on r2.kost_id = n.kost_id
                                where n.rent_id is null and r1.rent_status IN (-2,1,2) 
                                and (
                                    -- Jika Status Keluar
                                    r1.rent_status = 1 or (
                                        r1.rent_verified_at <= n.news_tanggal and
                                        n.news_tanggal <= r1.rent_updated_at 
                                    )
                                )
                                and (
                                    -- Jika Status Masih Inap
                                    r1.rent_status <> 1 or (
                                        r1.rent_verified_at <= n.news_tanggal
                                    )
                                )
                                group by n.news_id
                            )a on ne.news_id = a.news_id
                            inner join kost ko on ne.kost_id = ko.kost_id
                            where ne.kost_id in (
                                select kost_id from rent r1
                                inner join room r2 on r1.room_id = r2.room_id
                                where r1.mahasiswa_id='{(App.User as Mahasiswa).MhsId}'
                            )
                            ) a order by a.news_tanggal desc
                           ";
                        //    Union All News
                        //    $"select ko.kost_id, ko.kost_name, ne.news_id, ne.news_tanggal, ne.news_judul, ne.news_description from news ne " +
                        //    $"left join rent re on re.rent_id = ne.rent_id " +
                        //    $"inner join kost ko on ko.kost_id = ne.kost_id " +
                        //    $"WHERE news_status = 1 and ne.rent_id is null and ne.news_tanggal > " +
                        //    $"( " +
                        //    $"select max(re.rent_tanggal) from kost ko " +
                        //    $"inner join room ro on ro.kost_id = ko.kost_id " +
                        //    $"inner join rent re on re.room_id = ro.room_id " +
                        //    $"where re.rent_status != 0 and re.rent_status != -1 and re.mahasiswa_id = '{(App.User as Mahasiswa).MhsId}' " +
                        //    $")) " +
                        //    $"a order by a.news_tanggal desc";
            listNews.Content = (App.Actmenu as ContainerMhs).Generate_List(query,"Inbox");
        }
    }
}
