﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace app
{
    class Admin:Account
    {
        private string adminId;

        public Admin(string id, string name, int saldo, BitmapImage bimp, string adminId) : base(id, name, saldo, bimp)
        {
            AdminId = adminId;
        }

        public string AdminId { get => adminId; set => adminId = value; }
    }
}
