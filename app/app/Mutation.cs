﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    public class Mutation {
        private string idKost;
        private string detail;
        private int nominal;
        private DateTime date;

        public string IdKost { get => idKost; set => idKost = value; }
        public string Detail { get => detail; set => detail = value; }
        public int Nominal { get => nominal; set => nominal = value; }
        public DateTime Date { get => date; set => date = value; }

        public Mutation(string idKost, string detail, int nominal, DateTime date) {
            IdKost = idKost;
            Detail = detail;
            Nominal = nominal;
            Date = date;
        }
    }
}
