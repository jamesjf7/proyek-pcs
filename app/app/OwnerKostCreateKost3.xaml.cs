﻿using MaterialDesignThemes.Wpf.Transitions;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for OwnerKostCreateKost3.xaml
    /// </summary>
    public partial class OwnerKostCreateKost3 : Page
    {
        Kost kost;
        private int _floor;
        public int Floor { get => _floor;
            set{
                // Cek Lantai -1
                if (value < 0) return;
                // Cek Lantai Kosong
                bool change = false;
                int maxFloor = kost.Floors;
                int currFloor = Floor;
                if(value > maxFloor && kost.isFloorEmpty(currFloor)){
                    SweetAlert.Show("Whoops", "This floor is lonely", msgImage: SweetAlertImage.WARNING);
                }else{
                    change = true;
                }
                // Jika Floor Berubah...
                if (change){
                    _floor = value;
                    txtFloor.Text = value + "";
                    btnMinus.IsEnabled = value > 0;
                    displayRoom();
                }
            }
        }

        public OwnerKostCreateKost3(Kost kost, int flr=0)
        {
            InitializeComponent();
            this.kost = kost;
            Floor = flr;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e){
            //Floor = 0;
        }

        List<RoomKost> listRoom = new List<RoomKost>();
        void displayRoom(){
            // Clear Wrap Pane
            wrapPanel.Children.Clear();
            // Update ListRoom
            listRoom = kost.ListRoom
            .Where(room => room.Lantai == Floor)
            .OrderBy(room => room.Name)
            .ToList();
            foreach (var room in listRoom){
                createRoom(room);
            }
        }

        void createRoom(RoomKost room){
            // Main Root
            TransitioningContent root = new TransitioningContent
            {
                OpeningEffect = new TransitionEffect(
                    TransitionEffectKind.ExpandIn
                )
            };

            // Root1
            Grid root1 = new Grid
            {
                Width = 200,
                Height = 250,
                Background = new SolidColorBrush(Color.FromRgb(
                    (System.Drawing.Color.White).R,
                    (System.Drawing.Color.White).G,
                    (System.Drawing.Color.White).B
                )),
                Margin = new Thickness(10)
            };
            root1.RowDefinitions.Add(new RowDefinition());
            root1.RowDefinitions.Add(new RowDefinition{Height=GridLength.Auto});
            root.Content = root1;

            // Child1
            Border child1 = new Border
            {
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#333"))
            };
            child1.SetValue(Grid.RowProperty, 0);
            root1.Children.Add(child1);

            // Child2
            Border child2 = new Border
            {
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(new[]{"#3D619B","#DE63B3","#5F9EA0"}[room.Gender]))
            };
            child2.SetValue(Grid.RowProperty, 1);
            root1.Children.Add(child2);

            // Child 1.1
            Image image = new Image{
                Source = room.Image,
                Stretch = Stretch.UniformToFill,
                HorizontalAlignment = HorizontalAlignment.Center
            };
            child1.Child = image;

            // Child 2.1
            StackPanel stackPane = new StackPanel
            {
                VerticalAlignment = VerticalAlignment.Top,
                Margin = new Thickness(10)
            };
            child2.Child = stackPane;

            // Child 2.1.n
            string[] arrText = new[]{
                $"Name: {room.Name}",
                $"Price: {CurrencyConverter.ToCurrency(room.Harga)}",
                $"Gender: {new[]{"Man","Woman","Both"}[room.Gender]}",
            };
            foreach (var text in arrText){
                TextBlock textBlock = new TextBlock{Text=text};
                stackPane.Children.Add(textBlock);
            }

            Grid grid = new Grid { Margin = new Thickness(0, 10, 0, 0) };
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            stackPane.Children.Add(grid);

            // Button Edit & Delete
            Button btnEdit = new Button
            {
                Content = "Edit",
                Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#212529")),
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFC107")),
                Margin = new Thickness(0, 0, 5, 0)
            };
            btnEdit.SetValue(Grid.ColumnProperty, 0);
            btnEdit.Click += btnEditClick;

            Button btnDelete = new Button
            {
                Content = "Delete",
                Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFE1")),
                Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#DC3545")),
                Margin = new Thickness(5, 0, 0, 0)
            };
            btnDelete.SetValue(Grid.ColumnProperty, 1);
            btnDelete.Click += btnDeleteClick;

            grid.Children.Add(btnEdit);
            grid.Children.Add(btnDelete);

            // Add to Parent
            wrapPanel.Children.Add(root);
        }

        RoomKost getRoomKostFromControl(Button btn){
            DependencyObject parent;
            parent = VisualTreeHelper.GetParent(btn); // grid
            parent = VisualTreeHelper.GetParent(parent); // stackpane
            parent = VisualTreeHelper.GetParent(parent); // border
            parent = VisualTreeHelper.GetParent(parent); // grid
            // Bawaan
            parent = VisualTreeHelper.GetParent(parent); // ContentPresenter
            parent = VisualTreeHelper.GetParent(parent); // Border
            parent = VisualTreeHelper.GetParent(parent); // TransitioningContent
            int idx = wrapPanel.Children.IndexOf((UIElement)parent);
            return listRoom[idx];
        }

        void btnEditClick(object sender, RoutedEventArgs e) => App.Actmenu.navigate(new OwnerKostCreateKost4(kost, getRoomKostFromControl(sender as Button)));

        void btnDeleteClick(object sender, RoutedEventArgs e){
            RoomKost room = getRoomKostFromControl(sender as Button);
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to delete this Room?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if(result == SweetAlertResult.YES){
                kost.ListRoom.Remove(room);
                Floor = Floor;
            }
        }
        
        private void BtnAddRoom_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostCreateKost4(kost,Floor));
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e) => Floor--;
        private void BtnPlus_Click(object sender, RoutedEventArgs e) => Floor++;

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new OwnerKostCreateKost2(kost));
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if (kost.ListRoom.Count <= 0){
                SweetAlert.Show("Whoops", "Please add room first", msgImage: SweetAlertImage.WARNING);
                return;
            }
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to Create Kost And All Room ?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.YES)
            {
                string Ownerkost_ID        = kost.Owner.OwnerId;
                string Kost_Name           = kost.Name ;
                string Kost_Alamat         = kost.Alamat;     
                string Kost_Description    = kost.Description;
                var    Kost_Image          = BitmapConverter.GetBase64FromBitmap(kost.Image);
                var    Kost_Created_At     = DateTime.Now;//iki belakangan
                int    Kost_Lantai         = Floor;
                int    Kost_Denda_Telat    = kost.Denda;
                int    Batas_Hari_Telat    = kost.DueDay;
                int    Kost_Status         = 0;

                OracleTransaction trans;
                App.connection.Open();

                trans = App.connection.BeginTransaction();

                try
                {
                    OracleCommand cmd = new OracleCommand()
                    {
                        Connection = App.connection,
                        CommandText = "generateKost",
                        CommandType = CommandType.StoredProcedure
                    };
                    // Buat Return
                    cmd.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.ReturnValue,
                        ParameterName = "id",
                        OracleDbType = OracleDbType.Varchar2,
                        Size = 5
                    });

                    List<KeyValuePair<string, string>> listParam = new List<KeyValuePair<string, string>>();
                    listParam.Add(new KeyValuePair<string, string>("OWNERKOST_ID", Ownerkost_ID     ));
                    listParam.Add(new KeyValuePair<string, string>("KOST_NAME", Kost_Name        ));
                    listParam.Add(new KeyValuePair<string, string>("KOST_ALAMAT", Kost_Alamat      ));
                    listParam.Add(new KeyValuePair<string, string>("KOST_DESCRIPTION", Kost_Description));

                    foreach (var pair in listParam)
                    {
                        cmd.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = pair.Key,
                            OracleDbType = OracleDbType.Varchar2,
                            Value = pair.Value
                        });
                    }

                    List<KeyValuePair<string, int>> listParam2 = new List<KeyValuePair<string, int>>();
                    listParam2.Add(new KeyValuePair<string, int>("KOST_DENDA_TELAT",  Kost_Denda_Telat ));
                    listParam2.Add(new KeyValuePair<string, int>("KOST_BATAS_HARI_TELAT",  Batas_Hari_Telat ));
                    listParam2.Add(new KeyValuePair<string, int>("KOST_STATUS",  Kost_Status      ));

                    foreach (var pair in listParam2)
                    {
                        cmd.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = pair.Key,
                            OracleDbType = OracleDbType.Int32,
                            Value = pair.Value
                        });
                    }

                    cmd.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "KOST_IMAGE",
                        OracleDbType = OracleDbType.Clob,
                        Value = Kost_Image
                    });

                    cmd.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "KOST_CREATED_AT",
                        OracleDbType = OracleDbType.Date,
                        Value = Kost_Created_At
                    });
                    
                    cmd.ExecuteNonQuery();
                
                    string id = cmd.Parameters["id"].Value.ToString();
                 
                   
                    //ini add fasilitas
                    OracleDataAdapter da;
                    OracleCommandBuilder builder;
                    string sql = "select * from fasilitas where KOST_ID ='" + id + "'";
                    da = new OracleDataAdapter(sql, App.connection);
                    builder = new OracleCommandBuilder(da);
                    var dt = new DataTable();
                    da.Fill(dt);


                    foreach (var item in kost.ListFasilitas)
                    {
                        int a = 1;
                        var row = dt.NewRow();
                        row[0] = "";
                        row[1] = id;
                        row[2] = item.Name;
                        row[3] = BitmapConverter.GetBase64FromBitmap(item.Image);
                        dt.Rows.Add(row);
                      
                    }
                    da.Update(dt);
                     
                    
                    //ini add room
                    sql = "select * from room where KOST_ID ='" + id + "'";
                    da = new OracleDataAdapter(sql, App.connection);
                    builder = new OracleCommandBuilder(da);
                     dt = new DataTable();
                    da.Fill(dt);
                   

                    foreach (var item in kost.ListRoom)
                    {
                        int a = 1;
                        var row = dt.NewRow();
                        row[0] = "";
                        row[1] = id;
                        row[2] = item.Name;
                        row[3] = item.Lantai;
                        row[4] = BitmapConverter.GetBase64FromBitmap(item.Image);
                        row[5] = item.Harga;
                        row[6] = item.Harga;
                        row[7] = item.Description;
                        row[8] = item.Gender;
                        row[9] = 0;
                        dt.Rows.Add(row);
                       
                    }
                    da.Update(dt);
             
                    trans.Commit();
                    App.connection.Close();

                    SweetAlert.Show("Congratulations", "Kost Successfuly Registered", msgImage: SweetAlertImage.SUCCESS);
                    App.Actmenu.navigate(new OwnerKostHome());
                 

                }
                catch (Exception ex)
                {
                    SweetAlert.Show("Whoops", "Something went wrong...", msgImage: SweetAlertImage.ERROR);;
                    trans.Rollback();
                    App.connection.Close();

                }
            }
        }
    }
}
