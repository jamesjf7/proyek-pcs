﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string user = textUser.Text.ToLower();
            string pass = textPass.Password;
            if(user == "" || pass == ""){
                SweetAlert.Show("Whoops", "Your field is not ready", msgImage: SweetAlertImage.WARNING);
                return;
            }
            var fields = new string[] { "ACCOUNT_ID", "ACCOUNT_NAMA", "ACCOUNT_IMAGE", "ACCOUNT_SALDO", "ACCOUNT_TYPE", "ACCOUNT_STATUS" };
            OracleCommand cmd = new OracleCommand(
                $"select {String.Join(", ",fields)} from account where account_username=:username and account_password=:password",
                App.connection
            );
            cmd.Parameters.Add(":username",user);
            cmd.Parameters.Add(":password",pass);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            bool valid = false;
            string id, name=id="";
            BitmapImage bimp = null;
            int saldo, type, status=saldo=type=0;
            if (reader.HasRows){
                reader.Read();
                id = reader.GetString(0);
                name = reader.GetString(1);
                string base64 = reader.GetString(2);
                bimp = BitmapConverter.GetBitmapFromBase64(base64);
                saldo = int.Parse(reader.GetValue(3).ToString());
                type = reader.GetInt32(4);
                status = reader.GetInt32(5);
                valid = true;
            }
            App.connection.Close();
            string title = "Oh Snap";
            if (!valid){
                SweetAlert.Show(title, "Username / Password wrong!", msgImage: SweetAlertImage.ERROR);
            }else if(status == 0){
                SweetAlert.Show(title, "Account is not activated yet!", msgImage: SweetAlertImage.ERROR);
            }else if(type == 0){
                // Admin Code...
                cmd= cmd = new OracleCommand(
                    $"select admin_id from admin where account_id=:id",
                    App.connection
                );
                cmd.Parameters.Add(":id", id);
                App.connection.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                string adminid = reader.GetString(0);
                App.connection.Close();
                Admin admin = new Admin(id, name, saldo, bimp,adminid);
                App.MainWindow.navigateContainer(Container.Admin, admin);
            }
            else if (type == 1){
                // Mhs Code...
                cmd = new OracleCommand(
                    $@"select m.mahasiswa_id, m.mahasiswa_gender, m.tahun, j.jurusan_nama, a.account_username from mahasiswa m
                       inner join jurusan j on j.jurusan_id = m.jurusan_id 
                       inner join account a on a.account_id = m.account_id 
                       where m.account_id=:id",
                    App.connection
                );
                cmd.Parameters.Add(":id", id);
                App.connection.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                string mhsid = reader.GetString(0);
                int mhsgender = reader.GetInt32(1);
                int tahun = int.Parse(reader.GetValue(2).ToString());
                string jurusanNama = reader["jurusan_nama"].ToString();
                string username = reader["account_username"].ToString();
                App.connection.Close();
                Mahasiswa mhs = new Mahasiswa(id, name, saldo, bimp, mhsid, mhsgender, jurusanNama, tahun, username);
                App.MainWindow.navigateContainer(Container.Mahasiswa, mhs);
            }
            else if(type == 2){
                // OwnerKost Code...
                cmd = new OracleCommand(
                    $"select OWNERKOST_ID, OWNERKOST_TELP, OWNERKOST_EMAIL from ownerkost where account_id=:id",
                    App.connection
                );
                cmd.Parameters.Add(":id", id);
                App.connection.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                string ownid    = reader.GetString(0);
                string owntelp  = reader.GetString(1);
                string ownemail = reader.GetString(2);
                App.connection.Close();
                OwnerKost owner = new OwnerKost(id, name, saldo, bimp, ownid, owntelp,  ownemail);
                App.MainWindow.navigateContainer(Container.OwnerKost, owner);
            }
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new Register());
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            App.Actmenu.navigate(new AdminAddAdmin(false));
        }
    }
}
