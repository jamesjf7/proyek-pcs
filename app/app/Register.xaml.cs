﻿using Microsoft.Win32;
using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Page
    {
        public Register()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e) => App.Actmenu.navigate(new Login());

        BitmapImage img = null;
        OpenFileDialog op;
        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bmp = new BitmapImage(new Uri(op.FileName));
                btnImage.Content = null;
                Image newImg = new Image();
                newImg.Name = "imgPhoto";
                newImg.Stretch = Stretch.UniformToFill;
                newImg.Source = img = bmp;
                newImg.HorizontalAlignment = HorizontalAlignment.Center;
                btnImage.Content = newImg;
            }
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            // All Field
            string name = textName.Text;
            string telp = textTelp.Text;
            string user = textUser.Text.ToLower();
            string pass1 = textPass1.Password;
            string pass2 = textPass2.Password;
            string email = textEmail.Text;
            var tempImage = img;
            // Error Title
            string title = "Uh Oh";
            string message = "";
            bool invalid = true;
            if (tempImage == null) {
                message = "Please Add Image first";
            } else if (new String[] { name, telp, user, pass1, pass2, email }.Any(t => t == "")) {
                message = "Please Fill all field";
            } else if (!telp.ToCharArray().All(c => Char.IsDigit(c))){
                message = "Invalid phone number";
            } else if (!user.ToCharArray().All(c => Char.IsLetterOrDigit(c))) {
                message = "Username can only contain letter and number";
            } else if (pass1 != pass2) {
                message = "Confirm Password must be same with Password";
            } else if (!isValidEmail(email)){
                message = "Please enter valid email";
            }else invalid = false;

            // Give Message
            if (invalid){
                SweetAlert.Show(title,message,msgImage: SweetAlertImage.WARNING);
                return;
            }
            
            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand() {
                Connection = App.connection,
                CommandText = "generateOwnerKost",
                CommandType = CommandType.StoredProcedure
            };
            // Buat Return
            cmd.Parameters.Add(new OracleParameter() {
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            // Parameter Varchar2
            List<KeyValuePair<string, string>> listParam = new List<KeyValuePair<string, string>>();
            listParam.Add(new KeyValuePair<string, string>("name", name));
            listParam.Add(new KeyValuePair<string, string>("username", user));
            listParam.Add(new KeyValuePair<string, string>("password", pass1));
            listParam.Add(new KeyValuePair<string, string>("email", email));
            listParam.Add(new KeyValuePair<string, string>("telp", telp));
            foreach (var pair in listParam) {
                cmd.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = pair.Key,
                    OracleDbType = OracleDbType.Varchar2,
                    Value = pair.Value
                });
            }
            // Parameter Clob
            cmd.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "image",
                OracleDbType = OracleDbType.Clob,
                Value = BitmapConverter.GetBase64FromBitmap(tempImage)
            });
            App.connection.Open();
            cmd.ExecuteNonQuery();
            string result = cmd.Parameters["ret"].Value.ToString();
            App.connection.Close();
            if (result == "0"){
                SweetAlert.Show("Whoops", "Username already exists", msgImage: SweetAlertImage.ERROR);
            }else{
                SweetAlert.Show("Congratulation", "You have successfully register", msgImage: SweetAlertImage.SUCCESS);
                App.Actmenu.navigate(new Login());
            }
        }

        private bool isValidEmail(string email){
            var reg = new Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Match match = reg.Match(email);
            return match.Success;
        }
    }
}
