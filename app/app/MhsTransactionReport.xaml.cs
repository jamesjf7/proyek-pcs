﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app
{
    /// <summary>
    /// Interaction logic for MhsTransactionReport.xaml
    /// </summary>
    public partial class MhsTransactionReport : Page
    {
        public MhsTransactionReport()
        {
            InitializeComponent();
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            viewer.IsEnabled = true;
            ReportTransaction report = new ReportTransaction();
            report.SetDatabaseLogon("proyekpcs", "proyekpcs");
            report.SetParameterValue("MAHASISWA_ID", (App.User as Mahasiswa).MhsId);
            viewer.ViewerCore.ReportSource = report;
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            App.Actmenu.navigate(new MhsTransactionRecord());
        }
    }
}
