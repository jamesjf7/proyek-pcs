﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using Oracle.DataAccess.Client;
using System.Data;
namespace app{
    public partial class ContainerMhs : Page, INavigate{
        public ContainerMhs() => InitializeComponent();

        public void navigate(Page page, object param)
        {
            mainFrame.Content = null;
            mainFrame.NavigationService.Navigate(page, param);
        }

        public void navigate(Page page)
        {
            navigate(page, null);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // card 1
            navigate(new MhsHome());
            imageMhs.ImageSource = App.User.Image;
            textName.Text = App.User.Name;
            textMhsID.Text = ((Mahasiswa)App.User).MhsId;
            // card 2 
        }

        private void Navigation_SelectionChanged(object sender, SelectionChangedEventArgs e){
            #region Navigasi
            if (navigation.SelectedIndex == -1) return;
            string menu = (((DockPanel)navigation.SelectedItem).Children[1] as TextBlock).Text;
            if (menu == "Home") {
                navigate(new MhsHome());
            }
            else if (menu == "My Kost")
            {
                navigate(new MhsMyKost());
            }
            else if (menu == "Search Kost")
            {
                navigate(new MhsSearchKost());
            }
            else if (menu == "My Request")
            {
                navigate(new MhsMyRequest());
            }
            else if (menu == "Money Mutation")
            {
                navigate(new MhsTransactionRecord());
            }
            else if (menu == "Inbox")
            {
                navigate(new MhsInbox());
            }
            else if (menu == "History")
            {
                navigate(new MhsHistory());
            }
            else if(menu == "Change Password") {
                navigate(new ChangePassword());
            }
            else if (menu == "Logout")
            {
                SweetAlertResult res = SweetAlert.Show("Logout", "Are you sure ? ", msgButton: SweetAlertButton.YesNo, msgImage: SweetAlertImage.QUESTION);
                if (res.ToString() == "OK")
                    App.MainWindow.navigateContainer(Container.StartMenu);
            }
            drawerHost.IsLeftDrawerOpen = false;
            #endregion
        }

        public StackPanel Generate_List(string query, string type) {
            #region catatan
            // NOTE :
            // Kost          : kostID, ksotNama, kostImage, kostAlamat, kostDescription, kostLantai,
            // Fasilitas     : fasilitasID, kostID, fasilitasNama, fasilitasImage, fasilitasStatus
            // Room          : roomID, kostID, roomNama, roomLantai, roomImage, roomHarga, roomDescription, roomGender, roomStatus
            // Dtagih        : htagihID, rentID, dtagihSubtotal
            // TopUp         : topUpID, accID, tanggal, nominal

            // History(rent) : rentID, mhsID, roomID, rentTanggal, rentStatus, rentUpdatedAt
            // News(inbox)   : newsID, kostID, rentID, newsJudul, newsDescription, newsStatus
            // MyKost(rent)  : rentID, mhsID, roomID, rentTanggal, rentStatus, rentUpdatedAt
            #endregion
            StackPanel spContainer = new StackPanel();
            ItemsControl ic = new ItemsControl();
            OracleCommand cmd = new OracleCommand(query, App.connection);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            int idx = 0;
            // Jika hasil query tidak punya row
            if (!reader.HasRows) {
                TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 5) };
                tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                tc.OpeningEffects.Add(new TransitionEffect(new Random().Next(2) == 0 ? TransitionEffectKind.SlideInFromRight : TransitionEffectKind.SlideInFromLeft));
                Card card = new Card() { Margin = new Thickness(0, 5, 0, 5), Padding = new Thickness(10) };
                TextBlock tb = new TextBlock() { Text = "No Result", FontWeight = FontWeights.SemiBold, FontSize = 16, TextAlignment = TextAlignment.Center };
                card.Content = tb;
                tc.Content = card;
                ic.Items.Add(tc);
            }
            while (reader.Read()) {
                #region add multiple transition stackpanel n card
                TransitioningContent tc = new TransitioningContent() { OpeningEffectsOffset = new TimeSpan(0, 0, 0, 0, 20 * idx * 5) };
                tc.OpeningEffects.Add(new TransitionEffect(TransitionEffectKind.SlideInFromBottom));
                tc.OpeningEffects.Add(new TransitionEffect(new Random().Next(2) == 0 ? TransitionEffectKind.SlideInFromRight : TransitionEffectKind.SlideInFromLeft));
                StackPanel sp = new StackPanel() { Margin = new Thickness(10) };
                Card card = new Card() { Margin = new Thickness(20, 5, 20, 5) };
                #endregion
                #region My Kost
                if (type == "MyKost")
                {
                    List<UIElement> listElem = new List<UIElement>();
                    Grid g = new Grid() { Height = 200 };
                    DockPanel dp = new DockPanel() { HorizontalAlignment = HorizontalAlignment.Right, VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(10) };
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    string  kostID = reader["kost_id"].ToString(),
                            kostNama = reader["kost_name"].ToString(),
                            kostAlamat = reader["kost_alamat"].ToString(),
                            kostDescription = reader["kost_description"].ToString();
                    listElem.Add(new TextBlock() { Text = kostID, FontSize = 18, FontWeight = FontWeights.DemiBold, FontFamily = new FontFamily("helvetica"), Margin = new Thickness(0, 5, 0, 5) });
                    listElem.Add(new TextBlock() { Text = kostNama, FontSize = 18, FontWeight = FontWeights.SemiBold, FontFamily = new FontFamily("helvetica") });
                    DockPanel dpanel1 = new DockPanel();
                    dpanel1.Children.Add(new PackIcon() { Kind = PackIconKind.MapMarker });
                    dpanel1.Children.Add(new TextBlock() { Text = kostAlamat, FontSize = 18, FontWeight = FontWeights.Normal, FontFamily = new FontFamily("helvetica") });
                    listElem.Add(dpanel1);
                    DockPanel dpanel2 = new DockPanel();
                    string query2 = $"select nvl(substr(avg(rv.review_rating),1,3),'0.0') from review rv " +
                                    $"inner join rent rn on rn.rent_id = rv.rent_id " +
                                    $"inner join room r on r.room_id = rn.room_id " +
                                    $"inner join kost ko on ko.kost_id = r.kost_id " +
                                    $"where ko.kost_id = '{kostID}'";
                    OracleCommand cmds = new OracleCommand(query2, App.connection);
                    dpanel2.Children.Add(new RatingBar() { Value = ((int)float.Parse(cmds.ExecuteScalar().ToString())), Foreground = Brushes.Gold, IsReadOnly = true });
                    dpanel2.Children.Add(new TextBlock() { Text = $"({cmds.ExecuteScalar().ToString()})", FontSize = 18, FontFamily = new FontFamily("helvetica") });
                    listElem.Add(dpanel2);
                    listElem.Add(new TextBlock() { Text = kostDescription, FontSize = 14, FontFamily = new FontFamily("helvetica"), Margin = new Thickness(0, 5, 0, 5) });
                    Grid grKostImage = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["kost_image"].ToString()) } }; // kostImage
                    Button btnDetail = new Button() { Name = reader["kost_id"].ToString(), Content = "Detail", HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(5) };
                    Button btnReport = new Button() { Name = reader["kost_id"].ToString(), Content = "Report", HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(5), Background = Brushes.Red };
                    btnReport.Click += (source, ev) => {
                        new MhsDialog("Report", "OkCancel", (source as Button).Name).ShowDialog();
                    };
                    btnDetail.Click += (source, ev) => {
                        navigate(new MhsDetailKost("MyKost", (source as Button).Name));
                    };
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    dp.Children.Add(btnDetail);
                    dp.Children.Add(btnReport);
                    Grid.SetColumn(grKostImage, 0);
                    Grid.SetColumn(sp, 1);
                    Grid.SetColumn(dp, 2);
                    g.Children.Add(grKostImage);
                    g.Children.Add(sp);
                    g.Children.Add(dp);
                    card.Content = g;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region Search Kost
                else if (type == "SearchKost"){
                    List<UIElement> listElem = new List<UIElement>();
                    Grid g = new Grid() { Height = 200 };
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    string kostID = reader["kost_id"].ToString(),
                            kostNama = reader["kost_name"].ToString(),
                            kostAlamat = reader["kost_alamat"].ToString(),
                            kostDescription = reader["kost_description"].ToString();
                    listElem.Add(new TextBlock() { Text = kostID, FontSize = 18, FontWeight = FontWeights.DemiBold, FontFamily = new FontFamily("helvetica"), Margin = new Thickness(0, 5, 0, 5) });
                    listElem.Add(new TextBlock() { Text = kostNama, FontSize = 18, FontWeight = FontWeights.SemiBold, FontFamily = new FontFamily("helvetica") });
                    DockPanel dpanel1 = new DockPanel();
                    dpanel1.Children.Add(new PackIcon() { Kind = PackIconKind.MapMarker });
                    dpanel1.Children.Add(new TextBlock() { Text = kostAlamat, FontSize = 18, FontWeight = FontWeights.Normal, FontFamily = new FontFamily("helvetica") });
                    listElem.Add(dpanel1);
                    DockPanel dpanel2 = new DockPanel();
                    string query2 = $"select nvl(substr(avg(rv.review_rating),1,3),'0.0') from review rv " +
                                   $"inner join rent rn on rn.rent_id = rv.rent_id " +
                                   $"inner join room r on r.room_id = rn.room_id " +
                                   $"inner join kost ko on ko.kost_id = r.kost_id " +
                    $"where ko.kost_id = '{kostID}'";
                    OracleCommand cmds = new OracleCommand(query2, App.connection);
                    dpanel2.Children.Add(new RatingBar() { Value = ((int)float.Parse(cmds.ExecuteScalar().ToString())), Foreground = Brushes.Gold, IsReadOnly = true });
                    dpanel2.Children.Add(new TextBlock() { Text = $"({cmds.ExecuteScalar().ToString()})", FontSize = 18, FontFamily = new FontFamily("helvetica") });
                    listElem.Add(dpanel2);
                    listElem.Add(new TextBlock() { Text = kostDescription, FontSize = 14, FontFamily = new FontFamily("helvetica"), Margin = new Thickness(0, 5, 0, 5) });
                    Grid grKostImage = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["kost_image"].ToString()) } }; // kostImage
                    Button btn = new Button() { Name = kostID, Content = "Detail", HorizontalAlignment = HorizontalAlignment.Right, VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(10) };
                    btn.Click += (source, ev) => {
                        navigate(new MhsDetailKost("SearchKost", (source as Button).Name));
                    };
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    Grid.SetColumn(grKostImage, 0);
                    Grid.SetColumn(sp, 1);
                    Grid.SetColumn(btn, 2);
                    g.Children.Add(grKostImage);
                    g.Children.Add(sp);
                    g.Children.Add(btn);
                    card.Content = g;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region My Request
                else if (type == "MyRequest") // My Request
                {
                    List<UIElement> listElem = new List<UIElement>();
                    Grid g = new Grid() { Height = 250 };
                    DockPanel dpanel1 = new DockPanel() { Margin = new Thickness(20) };
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.5, GridUnitType.Star) });
                    //g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    string tanggal = ((DateTime)reader["rent_tanggal"]).ToString("dddd, dd MMM yyyy HH:mm"),
                           rentID = reader["rent_id"].ToString(),
                           roomNama = reader["room_nama"].ToString();
                    int rentStatus = int.Parse(reader["rent_status"].ToString()),
                        rentDeleteStatus = int.Parse(reader["rent_delete_status"].ToString()),
                        rentNominal = int.Parse(reader["rent_nominal"].ToString());
                    Grid gImage = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) } };
                    listElem.Add(new TextBlock() { Text = rentID, FontSize = 20, FontWeight = FontWeights.Bold, Padding = new Thickness(5) });
                    listElem.Add(new TextBlock() { Text = tanggal, FontSize = 20, FontWeight = FontWeights.SemiBold, Padding = new Thickness(5) });
                    DockPanel dpanel2 = new DockPanel() { Margin = new Thickness(5) };
                    dpanel2.Children.Add(new PackIcon() { Kind = PackIconKind.Bed, Width = 20, Height = 20 });
                    dpanel2.Children.Add(new TextBlock() { Text = $" : {roomNama}", FontSize = 20, FontWeight = FontWeights.Normal, FontFamily = new FontFamily("helvetica") });
                    DockPanel dpanel3 = new DockPanel() { Margin = new Thickness(5) };
                    dpanel3.Children.Add(new PackIcon() { Kind = PackIconKind.Money, Width = 20, Height = 20 });
                    dpanel3.Children.Add(new TextBlock() { Text = $" : {CurrencyConverter.ToCurrency(rentNominal)}", FontSize = 20, FontWeight = FontWeights.Normal, FontFamily = new FontFamily("helvetica") });
                    listElem.Add(dpanel2);
                    listElem.Add(dpanel3);
                    // status
                    if (rentDeleteStatus == 1) {
                        g.Background = Background = Brushes.LightGray;
                        listElem.Add(new TextBlock() { Text = "Status : Cancelled", FontFamily = new FontFamily("helvetica"), FontSize = 20, Padding = new Thickness(5) });
                    }
                    else if (rentStatus == -1)
                    { // Rent Status (reject)
                        g.Background = Brushes.Red;
                        listElem.Add(new TextBlock() { Text = "Status : Rejected", FontFamily = new FontFamily("helvetica"), FontSize = 20, Padding = new Thickness(5) });
                    }
                    else if (rentStatus == 0)
                    { // Rent Status (pending)
                        g.Background = Brushes.LightYellow;
                        listElem.Add(new TextBlock() { Text = "Status : Pending", FontFamily = new FontFamily("helvetica"), FontSize = 20, Padding = new Thickness(5) });
                        Button btnCancelRequest = new Button() { Name = reader["rent_id"].ToString(), Content = "Cancel", Background = Brushes.Red, Foreground = Brushes.White, BorderBrush = Brushes.Red, Margin = new Thickness(10) };
                        btnCancelRequest.Click += btnCancelRequst_Click;
                        listElem.Add(btnCancelRequest);
                    }
                    foreach (UIElement tb in listElem) sp.Children.Add(tb);
                    Grid.SetColumn(gImage, 0);
                    Grid.SetColumn(sp, 1);
                    //Grid.SetColumn(dpanel1, 2);
                    g.Children.Add(gImage);
                    g.Children.Add(sp);
                    //g.Children.Add(dpanel1);
                    card.Content = g;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region Top Up (Money Mutation)
                else if (type == "TopUp")
                {
                    List<UIElement> listElem = new List<UIElement>();
                    string topupID = reader.GetString(0),
                            tanggal = reader.GetDateTime(2).ToString("dd/MM/yyyy HH:mm:ss"),
                            saldo = $"{CurrencyConverter.ToCurrency(int.Parse(reader["topup_nominal"].ToString()))}";
                    listElem.Add(new TextBlock() { Text = topupID, FontSize = 16, FontWeight = FontWeights.Bold });
                    listElem.Add(new TextBlock() { Text = tanggal, FontSize = 16, FontWeight = FontWeights.Bold });
                    listElem.Add(new TextBlock() { Text = saldo, FontSize = 16, FontWeight = FontWeights.Bold });
                    foreach (UIElement i in listElem) sp.Children.Add(i);
                    card.Content = sp;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region Pay History (Money Mutation)
                else if (type == "PayHistory") {
                    List<UIElement> listElem = new List<UIElement>();
                    string tanggal = reader["tanggal"].ToString(),
                           rentID = reader["rent_id"].ToString();
                    int    nominal = int.Parse(reader["DT.DTAGIH_SUBTOTAL"].ToString()),
                           rentNominal = int.Parse(reader["rent_nominal"].ToString()),
                           mode = int.Parse(reader["mode"].ToString());
                    
                    listElem.Add(new TextBlock() { Text = tanggal, FontSize = 16, FontWeight = FontWeights.Bold }); // Rent ID
                    listElem.Add(new TextBlock() { Text = rentID, FontSize = 16, FontWeight = FontWeights.Bold }); // Rent Tanggal
                    if (mode == 0) {
                        listElem.Add(new TextBlock() { Text = CurrencyConverter.ToCurrency(rentNominal), FontSize = 16, FontWeight = FontWeights.Bold }); // Room ID
                    }
                    else if (mode == 1) {
                        listElem.Add(new TextBlock() { Text = CurrencyConverter.ToCurrency(nominal), FontSize = 16, FontWeight = FontWeights.Bold }); // Room ID
                    }
                    else {
                        listElem.Add(new TextBlock() { Text = CurrencyConverter.ToCurrency(nominal), FontSize = 16, FontWeight = FontWeights.Bold }); // Room ID
                    }
                    foreach (UIElement tb in listElem) sp.Children.Add(tb);
                    card.Content = sp;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region PayUtang
                else if (type == "PayUtang") {
                    List<UIElement> listElem = new List<UIElement>();
                    Grid g = new Grid();
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.5, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    string htagihID = reader["htagih_id"].ToString(),
                           htagihTanggal = reader["htagih_tanggal"].ToString(),
                           rentID = reader["rent_id"].ToString();
                    int utangTotal = int.Parse(reader["utang_nominal"].ToString())+int.Parse(reader["utang_denda"].ToString());
                    listElem.Add(new TextBlock() { Text = $"{htagihTanggal} | {htagihID}", FontSize = 16, FontWeight = FontWeights.Bold });
                    listElem.Add(new TextBlock() { Text = rentID, FontSize = 16, FontWeight = FontWeights.Bold });
                    listElem.Add(new TextBlock() { Text = $"{CurrencyConverter.ToCurrency(utangTotal)}", FontSize = 16, FontWeight = FontWeights.Bold });
                    Button btnPay = new Button() { Name = rentID, Content = "Pay", VerticalAlignment = VerticalAlignment.Bottom, HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(10) };
                    btnPay.Click += (source, ev) =>{
                        new MhsDialog("PayUtang","OkCancel",(source as Button).Name).ShowDialog();
                    };
                    foreach (UIElement tb in listElem) sp.Children.Add(tb);
                    Grid.SetColumn(sp, 0);
                    Grid.SetColumn(btnPay, 1);
                    g.Children.Add(sp);
                    g.Children.Add(btnPay);
                    card.Content = g;
                    tc.Content = card;
                    ic.Items.Add(tc);
                }
                #endregion
                #region Inbox
                else if (type == "Inbox")
                {
                    List<UIElement> listElem = new List<UIElement>();
                    string  kostNama          = reader["kost_name"].ToString(), // harusnya kost name
                            newsTanggal     = ((DateTime) reader["news_tanggal"]).ToString("dddd, dd MMMM yyyy hh:MM"),
                            newsJudul       = reader["news_judul"].ToString(), 
                            newsDescription = reader["news_description"].ToString();
                    listElem.Add(new TextBlock() { Text = $"From : {kostNama}", FontSize = 16, FontWeight = FontWeights.SemiBold });
                    listElem.Add(new TextBlock() { Text = newsDescription, FontSize = 16, FontWeight = FontWeights.Thin , Margin = new Thickness(0,5,0,5)}); // News Judul
                    foreach (UIElement tb in listElem) sp.Children.Add(tb);
                    Expander expander = new Expander() { Padding = new Thickness(10,0,10,0), Margin = new Thickness(10)};
                    expander.Header = new TextBlock() { Text = $"{newsTanggal} | {newsJudul}", FontSize = 16, FontWeight = FontWeights.SemiBold };
                    expander.Content = sp;
                    tc.Content = expander;
                    ic.Items.Add(tc);
                }
                #endregion
                #region History
                else if (type == "History"){
                    List<UIElement> listElem = new List<UIElement>();
                    Grid g = new Grid() { Height = 200 };
                    Expander expander = new Expander() { Margin = new Thickness(10)};
                    DockPanel dp = new DockPanel() { Margin = new Thickness(20) };
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1.5, GridUnitType.Star) });
                    g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    string tanggal    = ((DateTime)reader["rent_tanggal"]).ToString("dddd, dd MMMM yyyy HH:mm"),
                           rentID     = reader["rent_id"].ToString(),
                           roomNama   = reader["room_nama"].ToString(),
                           kostName   = reader["kost_name"].ToString(), // harusnya room name
                           kostAlamat = reader["kost_alamat"].ToString();
                    int rentStatus = int.Parse(reader["rent_status"].ToString());
                    Grid gImageRoom = new Grid() { Background = new ImageBrush() { ImageSource = BitmapConverter.GetBitmapFromBase64(reader["room_image"].ToString()) } };
                    listElem.Add(new TextBlock() { Text = kostName, FontSize = 16, FontWeight = FontWeights.SemiBold }); 
                    listElem.Add(new TextBlock() { Text = kostAlamat, FontSize = 16, FontWeight = FontWeights.Thin }); 
                    listElem.Add(new TextBlock() { Text = roomNama, FontSize = 16, FontWeight = FontWeights.Thin }); 
                    // status
                    TextBlock textRoomStatus = new TextBlock();
                    if (rentStatus == 2) // Rent Status (pending)
                    {
                        textRoomStatus = new TextBlock() { Text = "Out Dewe", HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Padding = new Thickness(10), Margin = new Thickness(10), FontSize = 16, FontWeight = FontWeights.Bold, Background = Brushes.DarkGray, Foreground = Brushes.DimGray };
                        dp.Children.Add(textRoomStatus);
                    }
                    else if (rentStatus == -2) // Rent Status (accepted)
                    {
                        textRoomStatus = new TextBlock() { Text = "Kicked", HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center, Padding = new Thickness(10), Margin = new Thickness(10), FontSize = 16, FontWeight = FontWeights.Bold, Background = Brushes.Red, Foreground = Brushes.White };
                        dp.Children.Add(textRoomStatus);
                    }
                    Button btnReport = new Button() { Name = reader["kost_id"].ToString(), Content = "Report", HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(5), Background = Brushes.Red };
                    btnReport.Click += (source, ev) => {
                        new MhsDialog("Report", "OkCancel", (source as Button).Name).ShowDialog();
                    };
                    dp.Children.Add(btnReport);
                    foreach (UIElement tb in listElem) sp.Children.Add(tb);
                    Grid.SetColumn(gImageRoom, 0);
                    Grid.SetColumn(sp, 1);
                    Grid.SetColumn(dp, 2);
                    g.Children.Add(gImageRoom);
                    g.Children.Add(sp);
                    g.Children.Add(dp);
                    expander.Header = new TextBlock() { Text = $"{tanggal} | {rentID}", FontSize = 16, FontWeight = FontWeights.Bold }; 
                    expander.Content = g;
                    tc.Content = expander;
                    ic.Items.Add(tc);
                }
                #endregion
                idx++;
            }
            App.connection.Close();
            spContainer.Children.Add(ic);
            return spContainer;
        }
 
        public void btnCancelRequst_Click(object sender, EventArgs e)
        {
            string id = (sender as Button).Name;
            App.connection.Open();
            // rentdeletestatus 0 = ada , 1 = delete
            OracleCommand cmd = new OracleCommand($"update rent set rent_delete_status = 1 where rent_id = '{id}'",App.connection);
            cmd.ExecuteNonQuery();
            cmd = new OracleCommand($"select account_saldo from account where account_id = '{App.User.Id}'", App.connection);
            App.User.Saldo = int.Parse(cmd.ExecuteScalar().ToString());
            App.connection.Close();
            SweetAlert.Show("Notification","Successfully Canceled",msgButton: SweetAlertButton.OK, msgImage: SweetAlertImage.SUCCESS);
            navigate(new MhsMyRequest());
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            var result = new SweetAlert()
            {
                Title = "Confirmation",
                Message = "Are you sure you want to leave?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();

            if (result == SweetAlertResult.YES)
                Application.Current.Shutdown();
        }
    }
}
