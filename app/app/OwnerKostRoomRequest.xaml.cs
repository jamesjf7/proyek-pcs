﻿using Oracle.DataAccess.Client;
using SweetAlertSharp;
using SweetAlertSharp.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace app {
    /// <summary>
    /// Interaction logic for OwnerKostRoomRequest.xaml
    /// </summary>
    public partial class OwnerKostRoomRequest : Page {
        public OwnerKostRoomRequest() {
            InitializeComponent();
        }

        List<RoomRequest> roomRequests;
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            roomRequests = new List<RoomRequest>();
            loadKost();
            loadRequests();
            cbKost.SelectedIndex = 0;
        }

        void loadKost() {
            // Clear Combobox
            cbKost.Items.Clear();
            // Setting
            cbKost.DisplayMemberPath = "name";
            cbKost.SelectedValuePath = "id";

            string sql = @"
                select kost_id, kost_name from kost k
                where k.ownerkost_id = :id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", (App.User as OwnerKost).OwnerId);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            // Add Item to Combobox
            cbKost.Items.Add(new {
                id = "",
                name = "All"
            });
            while (reader.Read()) {
                cbKost.Items.Add(new {
                    id = reader.GetString(0),
                    name = reader.GetString(1)
                });
            }
            App.connection.Close();
        }

        void loadRequests() {
            string sql = @"
                select
	                r1.rent_id,
                    k.kost_id,
	                'Room ' || r2.room_nama || ' (Floor ' || r2.room_lantai || 
	                ') | ' || k.kost_name,
	                to_char(r1.rent_tanggal,'HH24:MI:SS DD MON YYYY') || 
	                ' ' || a.account_nama
                from rent r1
                inner join room r2 on r1.room_id = r2.room_id
                inner join kost k on r2.kost_id = k.kost_id
                inner join mahasiswa m on r1.mahasiswa_id = m.mahasiswa_id
                inner join account a on m.account_id = a.account_id
                where k.ownerkost_id = :id and r1.rent_status = 0
                order by k.kost_id, r2.room_id
            ";
            OracleCommand cmd = new OracleCommand(sql, App.connection);
            cmd.Parameters.Add(":id", (App.User as OwnerKost).OwnerId);
            App.connection.Open();
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read()) {
                string
                    idRent = reader.GetString(0), // Rent ID
                    idKost = reader.GetString(1), // Kost ID
                    header = reader.GetString(2), // Header
                    detail = reader.GetString(3); // Detail Request
                // Add to List
                roomRequests.Add(new RoomRequest(idRent,idKost,header,detail));
            }
            App.connection.Close();
        }

        void generateRequestComponent(string where = "") {
            // Hide Card
            cardRequest.Visibility = Visibility.Collapsed;
            // Clear Child
            stackRequest.Children.Clear();
            // Filter list
            List<RoomRequest> tempList = roomRequests
                .Where(req => where == "" || where == req.IdKost)
                .ToList();
            // Dynamic Component...
            string room = "";
            Expander root = null;
            foreach (RoomRequest roomRequest in tempList) {
                // If Coom Change Crete New Expander
                if(room != roomRequest.Header) {
                    room = roomRequest.Header;
                    Expander expander = new Expander {
                        Foreground = BrushGen.FromString("#eee"),
                        Margin = new Thickness(0, 0, 0, 10)
                    };
                    // Header
                    expander.Header = new TextBlock {
                        Text = room,
                        FontSize = 20
                    };
                    // Linear Gradient Brush
                    LinearGradientBrush brush = new LinearGradientBrush {
                        StartPoint = new Point(0, 0),
                        EndPoint = new Point(1, 1)
                    };
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(
                            System.Drawing.Color.MediumVioletRed
                        ),
                        Offset = 0f
                    });
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(
                            System.Drawing.Color.SlateBlue
                        ),
                        Offset = 0.5f
                    });
                    brush.GradientStops.Add(new GradientStop {
                        Color = ColorGen.FromDrawingColor(
                            System.Drawing.Color.RoyalBlue
                        ),
                        Offset = 1f
                    });
                    // Background
                    expander.Background = brush;
                    // Child
                    expander.Content = new Border {
                        Background = BrushGen.FromString("#66FFFFFF"),
                        Child = new StackPanel {
                            Margin = new Thickness(10)
                        }
                    };

                    // Add to StackPanel
                    stackRequest.Children.Add(root = expander);
                }
                // Parent
                Expander parent = root;

                // Expander > Border > Stackpanel
                StackPanel rootStack = (root.Content as Border).Child as StackPanel;

                // Root Child
                Grid grid = new Grid {
                    Margin = new Thickness(0, 0, 0, 10),
                    Background = BrushGen.FromString("#33FFFFFF")
                };
                grid.ColumnDefinitions.Add(new ColumnDefinition { });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                rootStack.Children.Add(grid);
                // Child 1
                TextBlock tb = new TextBlock {
                    FontSize = 18,
                    VerticalAlignment = VerticalAlignment.Center,
                    Text = roomRequest.Detail
                };
                tb.SetValue(Grid.ColumnProperty, 0);
                grid.Children.Add(tb);
                // Child 2
                Grid subGrid = new Grid { };
                subGrid.ColumnDefinitions.Add(new ColumnDefinition { });
                subGrid.ColumnDefinitions.Add(new ColumnDefinition { });
                subGrid.SetValue(Grid.ColumnProperty, 1);
                grid.Children.Add(subGrid);
                // Button Accept & Reject
                Button btnAccept = new Button {
                    Margin = new Thickness(0, 0, 5, 0),
                    Content = "Accept",
                    Background = BrushGen.FromString("#6c6"),
                    BorderBrush = BrushGen.FromDrawingColor(System.Drawing.Color.Green)
                };
                btnAccept.Click += (object sender, RoutedEventArgs e) => {
                    if (responseRent(roomRequest.IdRequest, 1)) {
                        SweetAlert.Show("Success", "Request succesfully accepted", msgImage: SweetAlertImage.SUCCESS);
                        parent.Visibility = Visibility.Collapsed;
                        // Remove RoomRequest dari List
                        roomRequests = roomRequests
                            .Where(req => req.Header != roomRequest.Header)
                            .ToList();
                    }
                };
                Button btnReject = new Button {
                    Content = "Reject",
                    Background = BrushGen.FromString("#f66"),
                    BorderBrush = BrushGen.FromDrawingColor(System.Drawing.Color.Red)
                };
                btnReject.Click += (object sender, RoutedEventArgs e) => {
                    if (responseRent(roomRequest.IdRequest, -1)) {
                        rootStack.Children.Remove(grid);
                        SweetAlert.Show("Success", "Request successfully rejected", msgImage: SweetAlertImage.SUCCESS);
                        if (rootStack.Children.Count == 0)
                            parent.Visibility = Visibility.Collapsed;
                        // Remove RoomRequest dari List
                        roomRequests.Remove(roomRequest);
                    }
                };
                btnAccept.SetValue(Grid.ColumnProperty, 0);
                btnReject.SetValue(Grid.ColumnProperty, 1);
                subGrid.Children.Add(btnAccept);
                subGrid.Children.Add(btnReject);
            }
            // Show Card
            cardRequest.Visibility = Visibility.Visible;
        }

        bool responseRent(string rid, int resp) {
            var result = new SweetAlert() {
                Title = "Confirmation",
                Message = $"Are you sure you want to {(resp == 1 ? "Accept" : "Reject")} this request?",
                MsgImage = SweetAlertImage.QUESTION,
                MsgButton = SweetAlertButton.YesNo,
                OkText = "Yes",
                CancelText = "No"
            }.ShowDialog();
            if (result != SweetAlertResult.YES) return false;

            // Panggil Function generate ownerkos
            OracleCommand cmd = new OracleCommand() {
                Connection = App.connection,
                CommandText = "rentResponse",
                CommandType = CommandType.StoredProcedure
            };
            // Buat Return
            cmd.Parameters.Add(new OracleParameter() {
                Direction = ParameterDirection.ReturnValue,
                ParameterName = "ret",
                OracleDbType = OracleDbType.Int32
            });
            // Parameter
            cmd.Parameters.Add(new OracleParameter {
                Direction = ParameterDirection.Input,
                ParameterName = "rid",
                OracleDbType = OracleDbType.Varchar2,
                Value = rid
            });
            cmd.Parameters.Add(new OracleParameter {
                Direction = ParameterDirection.Input,
                ParameterName = "resp",
                OracleDbType = OracleDbType.Int32,
                Value = resp
            });
            App.connection.Open();
            cmd.ExecuteNonQuery();
            App.connection.Close();

            // Reload
            if (resp == 1) {
                // Refresh Saldo
                (App.Actmenu as ContainerOwnerKost).loadAccount();
                // Reload Dynamic Component
                //loadGenerateRoom();
            }
            return true;
        }

        private void CbKost_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            int idx = cbKost.SelectedIndex;
            if (idx == -1) return;
            generateRequestComponent(cbKost.SelectedValue.ToString());
        }
    }
}
