﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app {
    public class News {
        private string idKost;
        private string title;
        private string receiver;
        private string message;
        private bool isPublic;

        public string IdKost { get => idKost; set => idKost = value; }
        public string Title { get => title; set => title = value; }
        public string Receiver { get => receiver; set => receiver = value; }
        public string Message { get => message; set => message = value; }
        public bool IsPublic { get => isPublic; set => isPublic = value; }

        public News(string idKost, string title, string receiver, string message, bool isPublic) {
            this.idKost = idKost;
            this.title = title;
            this.receiver = receiver;
            this.message = message;
            this.isPublic = isPublic;
        }
    }
}
