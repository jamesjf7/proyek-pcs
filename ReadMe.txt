    + ++----++
----| KOST ~ |-----
  --| ~ KAST |--
    +--+ ++--+

Kelompok:
> 218116682 - Davin Djayadi
> 218116688 - Ivan Budihardjo
> 218116689 - James Jeremy Foong
> 218116707 - Yulius

Note (Sebelum menjalankan program):
1. Pastikan ada user proyekpcs dengan password proyekpcs
2. Pastikan sudah menjalakan import proyekpcs.DMP (Filenya ada di Folder "import_first")
3. Pastikan project sudah diBuild

Step by step buat USER:
- Buka SQL PLUS
- Login dba account
- jalankan Query berikut:
	create user proyekpcs identified by proyekpcs;
	grant dba to proyekpcs;

Step by step import DMP
- Buka CMD
- Jalankan Import (Pastikan path DMP sudah benar):
	imp proyekpcs/proyekpcs file=<path>/import_first/proyekpcs.DMP full=y;
- Jika berjalan dengan lancar maka cmd akan menampilkan:
	`Import terminated successfully without warnings.`

Extra:
1. Pada form login jika user doubleclick tulisan "OR" maka user dapat membuat akun admin.

Git:
https://gitlab.com/jamesjf7/proyek-pcs

